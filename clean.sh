#!/usr/bin/env bash
find components -name "*.js" -type f -delete
find components -name "*.js.map" -type f -delete
find customize -name "*.js" -type f -delete
find customize -name "*.js.map" -type f -delete
find pages -name "*.js" -type f -delete
find pages -name "*.js.map" -type f -delete
rm travelcloud.js || true
rm rules-compute.js || true
rm order-product-compute.js || true
rm travelcloud.js.map || true
rm rules-compute.js.map || true
rm order-product-compute.js.map || true