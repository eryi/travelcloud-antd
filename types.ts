export type PriceRule = {
'id': string,
'name': string,
'type': string,
'description': string,
'is_public': string,
'adjustment_name': string,
'adjustment_sequence': string,
'adjustment_price_type': string,
'adjustment_fixed': string,
'adjustment_percentage': string,
'adjustment_percentage_price_attribute': string,
'adjustment_percentage_min': string,
'adjustment_percentage_min_enabled': string,
'adjustment_percentage_max': string,
'adjustment_percentage_max_enabled': string,
'condition_from_datetime': string,
'condition_to_datetime': string,
'condition_checkout_code': string,
'condition_checkout_code_hash': string,
'condition_product_source': string,
'condition_product_type': string,
'condition_mutex_code': string,
'condition_customer_in_any_categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'condition_product_in_any_categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
}
export type PriceRules = Array<{
'id': string,
'name': string,
'type': string,
'description': string,
'is_public': string,
'adjustment_name': string,
'adjustment_sequence': string,
'adjustment_price_type': string,
'adjustment_fixed': string,
'adjustment_percentage': string,
'adjustment_percentage_price_attribute': string,
'adjustment_percentage_min': string,
'adjustment_percentage_min_enabled': string,
'adjustment_percentage_max': string,
'adjustment_percentage_max_enabled': string,
'condition_from_datetime': string,
'condition_to_datetime': string,
'condition_checkout_code': string,
'condition_checkout_code_hash': string,
'condition_product_source': string,
'condition_product_type': string,
'condition_mutex_code': string,
'condition_customer_in_any_categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'condition_product_in_any_categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
}>
export type ApiLog = {
'id': string,
'action': string,
'api': string,
'api_ref': string,
'table': string,
'table_id': string,
'has_error': string,
'error': string,
'date_added': string
}
export type ApiLogs = Array<{
'id': string,
'action': string,
'api': string,
'api_ref': string,
'table': string,
'table_id': string,
'has_error': string,
'error': string,
'date_added': string
}>
export type Document = {
'id': string,
'name': string,
'content': string,
'photo_url': string,
'photo_license_url': string,
'sort_order': string,
'attributes': object,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'photos': Array<{
'id': string,
'title': string,
'desc': string,
'url': string,
'license_url': string,
'sequence': string
}>
}
export type Documents = Array<{
'id': string,
'name': string,
'content': string,
'photo_url': string,
'photo_license_url': string,
'sort_order': string,
'attributes': object,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'photos': Array<{
'id': string,
'title': string,
'desc': string,
'url': string,
'license_url': string,
'sequence': string
}>
}>
export type EmailDomain = {
'id': string,
'domain': string
}
export type EmailDomains = Array<{
'id': string,
'domain': string
}>
export type Site = {
'id': string,
'name': string,
'url': string,
'is_default': string,
'payment_success_url': string,
'payment_failure_url': string,
'payment_pending_url': string,
'email_name': string,
'email_mailbox': string,
'email_domain_id': string,
'email_invoice_subject': string,
'email_invoice_template': string,
'email_hotel_voucher_subject': string,
'email_hotel_voucher_template': string,
'email_hotel_failed_subject': string,
'email_hotel_failed_template': string,
'email_generic_voucher_subject': string,
'email_generic_voucher_template': string,
'email_generic_failed_subject': string,
'email_generic_failed_template': string,
'email_contact_form_subject': string,
'email_contact_form_template': string,
'email_verify_subscription_subject': string,
'email_verify_subscription_template': string,
'email_domain': {
'id': string,
'domain': string
}
}
export type Sites = Array<{
'id': string,
'name': string,
'url': string,
'is_default': string,
'payment_success_url': string,
'payment_failure_url': string,
'payment_pending_url': string,
'email_name': string,
'email_mailbox': string,
'email_domain_id': string,
'email_invoice_subject': string,
'email_invoice_template': string,
'email_hotel_voucher_subject': string,
'email_hotel_voucher_template': string,
'email_hotel_failed_subject': string,
'email_hotel_failed_template': string,
'email_generic_voucher_subject': string,
'email_generic_voucher_template': string,
'email_generic_failed_subject': string,
'email_generic_failed_template': string,
'email_contact_form_subject': string,
'email_contact_form_template': string,
'email_verify_subscription_subject': string,
'email_verify_subscription_template': string,
'email_domain': {
'id': string,
'domain': string
}
}>
export type Admin = {
'id': string,
'first_name': string,
'last_name': string,
'email': string,
'create_date': string,
'deleted': string,
'roles': {
'customer-manage': boolean,
'price-rule': boolean,
'tour-product': boolean,
'generic-product': boolean,
'visa-product': boolean,
'order-find': boolean,
'order-manual-quote': boolean,
'order-payment-add': boolean,
'order-quotation-expiry': boolean,
'order-status': boolean,
'order-product-status': boolean,
'order-report': boolean,
'site': boolean,
'admin-super': boolean
},
'restrict_referral_site': string,
'notify_new_invoice': string,
'notify_new_conversation': string,
'failed_login_count': string,
'whitelist_ipv4_start': string,
'whitelist_ipv4_end': string,
'reduce_2fa': string,
'last_30_days_tokens': Array<{
'id': string,
'roles': string,
'restrict_referral_site': string,
'ip': string,
'ip_country': string,
'user_agent': string,
'issued': string,
'expiry': string,
'whitelist_ipv4_start': string,
'whitelist_ipv4_end': string
}>
}
export type Admins = Array<{
'id': string,
'first_name': string,
'last_name': string,
'email': string,
'create_date': string,
'deleted': string,
'roles': {
'customer-manage': boolean,
'price-rule': boolean,
'tour-product': boolean,
'generic-product': boolean,
'visa-product': boolean,
'order-find': boolean,
'order-manual-quote': boolean,
'order-payment-add': boolean,
'order-quotation-expiry': boolean,
'order-status': boolean,
'order-product-status': boolean,
'order-report': boolean,
'site': boolean,
'admin-super': boolean
},
'restrict_referral_site': string,
'notify_new_invoice': string,
'notify_new_conversation': string,
'failed_login_count': string,
'whitelist_ipv4_start': string,
'whitelist_ipv4_end': string,
'reduce_2fa': string,
'last_30_days_tokens': Array<{
'id': string,
'roles': string,
'restrict_referral_site': string,
'ip': string,
'ip_country': string,
'user_agent': string,
'issued': string,
'expiry': string,
'whitelist_ipv4_start': string,
'whitelist_ipv4_end': string
}>
}>
export type Category = {
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}
export type Categories = Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
export type Conversation = {
'id': string,
'referral_site': string,
'status': string,
'subject': string,
'created': string,
'last_updated': string,
'messages': Array<{
'id': string,
'admin_id': string,
'verified': string,
'customer_name': string,
'customer_email': string,
'html': string,
'unstructured': object,
'unstructured_shape': string,
'date': string,
'admin': {
'id': string,
'first_name': string,
'last_name': string,
'email': string,
'create_date': string,
'deleted': string,
'roles': {
'customer-manage': boolean,
'price-rule': boolean,
'tour-product': boolean,
'generic-product': boolean,
'visa-product': boolean,
'order-find': boolean,
'order-manual-quote': boolean,
'order-payment-add': boolean,
'order-quotation-expiry': boolean,
'order-status': boolean,
'order-product-status': boolean,
'order-report': boolean,
'site': boolean,
'admin-super': boolean
},
'restrict_referral_site': string,
'notify_new_invoice': string,
'notify_new_conversation': string,
'failed_login_count': string,
'whitelist_ipv4_start': string,
'whitelist_ipv4_end': string,
'reduce_2fa': string
}
}>
}
export type Conversations = Array<{
'id': string,
'referral_site': string,
'status': string,
'subject': string,
'created': string,
'last_updated': string,
'messages': Array<{
'id': string,
'admin_id': string,
'verified': string,
'customer_name': string,
'customer_email': string,
'html': string,
'unstructured': object,
'unstructured_shape': string,
'date': string,
'admin': {
'id': string,
'first_name': string,
'last_name': string,
'email': string,
'create_date': string,
'deleted': string,
'roles': {
'customer-manage': boolean,
'price-rule': boolean,
'tour-product': boolean,
'generic-product': boolean,
'visa-product': boolean,
'order-find': boolean,
'order-manual-quote': boolean,
'order-payment-add': boolean,
'order-quotation-expiry': boolean,
'order-status': boolean,
'order-product-status': boolean,
'order-report': boolean,
'site': boolean,
'admin-super': boolean
},
'restrict_referral_site': string,
'notify_new_invoice': string,
'notify_new_conversation': string,
'failed_login_count': string,
'whitelist_ipv4_start': string,
'whitelist_ipv4_end': string,
'reduce_2fa': string
}
}>
}>
export type Generic = {
'id': string,
'name': string,
'description': string,
'photo_url': string,
'photo_license_url': string,
'sort_order': string,
'banner_photo_id': string,
'options': Array<{
'id': string,
'code': string,
'name': string,
'stock': string,
'stock_type': string,
'cost': string,
'price': string,
'files': Array<{
'id': string,
'key': string,
'name': string,
'mime_type': string,
'size': string,
'crc32': string,
'notification_id': string,
'added_date': string
}>
}>,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'photos': Array<{
'id': string,
'title': string,
'desc': string,
'url': string,
'license_url': string,
'sequence': string
}>
}
export type Generics = Array<{
'id': string,
'name': string,
'description': string,
'photo_url': string,
'photo_license_url': string,
'sort_order': string,
'banner_photo_id': string,
'options': Array<{
'id': string,
'code': string,
'name': string,
'stock': string,
'stock_type': string,
'cost': string,
'price': string,
'files': Array<{
'id': string,
'key': string,
'name': string,
'mime_type': string,
'size': string,
'crc32': string,
'notification_id': string,
'added_date': string
}>
}>,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'photos': Array<{
'id': string,
'title': string,
'desc': string,
'url': string,
'license_url': string,
'sequence': string
}>
}>
export type Order = {
'id': string,
'ref': string,
'title': string,
'user_message': string,
'account_id': string,
'bill_uid': string,
'order_date': string,
'order_status': string,
'referral_site': string,
'payment_status': string,
'product_status': string,
'deposit_required': string,
'payment_required': string,
'payment_received': string,
'prefix': string,
'first_name': string,
'last_name': string,
'phone': string,
'address': string,
'login_customer_email': string,
'login_customer_category_ids': string,
'checkout_code': string,
'expiry': string,
'adult': string,
'child': string,
'infant': string,
'emergency_contact_name': string,
'emergency_contact_number': string,
'payment_regex': string,
'payment_regex_name': string,
'document_1': string,
'document_2': string,
'document_3': string,
'document_4': string,
'document_5': string,
'unstructured': object,
'customer': {
'id': string,
'name': string,
'phone': string,
'email': string,
'registered': string,
'email_subscription': string,
'email_verified': string,
'title': string,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
},
'travelers': Array<{
'id': string,
'passport': string,
'expiry': string,
'title': string,
'first_name': string,
'last_name': string,
'country': string,
'birth_date': string,
'type': string,
'flight': string,
'meal': string,
'special': string,
'doc_1_barcode': string,
'doc_2_barcode': string,
'doc_3_barcode': string,
'doc_4_barcode': string,
'doc_5_barcode': string
}>,
'payments': Array<{
'id': string,
'source': string,
'source_id': string,
'amount': string,
'description': string,
'date_added': string,
'details': string,
'status': string
}>,
'products': Array<{
'id': string,
'product_source': string,
'product_source_ref': string,
'product_source_pending_status': string,
'product_source_pending_timestamp': string,
'product_type': string,
'product_id': string,
'product_code': string,
'product_name': string,
'product_detail': object,
'product_category_ids': string,
'product_attributes': object,
'quantity': string,
'from_date': string,
'from_time': string,
'to_date': string,
'to_time': string,
'product_status': string,
'voucher': object,
'form': object,
'items': Array<{
'id': string,
'header': string,
'sub_header': string,
'name': string,
'price': string,
'deposit': string,
'price_type': string,
'quantity': string,
'is_from_price_rule': string
}>
}>
}
export type Orders = Array<{
'id': string,
'ref': string,
'title': string,
'user_message': string,
'account_id': string,
'bill_uid': string,
'order_date': string,
'order_status': string,
'referral_site': string,
'payment_status': string,
'product_status': string,
'deposit_required': string,
'payment_required': string,
'payment_received': string,
'prefix': string,
'first_name': string,
'last_name': string,
'phone': string,
'address': string,
'login_customer_email': string,
'login_customer_category_ids': string,
'checkout_code': string,
'expiry': string,
'adult': string,
'child': string,
'infant': string,
'emergency_contact_name': string,
'emergency_contact_number': string,
'payment_regex': string,
'payment_regex_name': string,
'document_1': string,
'document_2': string,
'document_3': string,
'document_4': string,
'document_5': string,
'unstructured': object,
'customer': {
'id': string,
'name': string,
'phone': string,
'email': string,
'registered': string,
'email_subscription': string,
'email_verified': string,
'title': string
},
'travelers': Array<{
'id': string,
'passport': string,
'expiry': string,
'title': string,
'first_name': string,
'last_name': string,
'country': string,
'birth_date': string,
'type': string,
'flight': string,
'meal': string,
'special': string,
'doc_1_barcode': string,
'doc_2_barcode': string,
'doc_3_barcode': string,
'doc_4_barcode': string,
'doc_5_barcode': string
}>,
'payments': Array<{
'id': string,
'source': string,
'source_id': string,
'amount': string,
'description': string,
'date_added': string,
'details': string,
'status': string
}>,
'products': Array<{
'id': string,
'product_source': string,
'product_source_ref': string,
'product_source_pending_status': string,
'product_source_pending_timestamp': string,
'product_type': string,
'product_id': string,
'product_code': string,
'product_name': string,
'product_detail': object,
'product_category_ids': string,
'product_attributes': object,
'quantity': string,
'from_date': string,
'from_time': string,
'to_date': string,
'to_time': string,
'product_status': string,
'voucher': object,
'form': object,
'items': Array<{
'id': string,
'header': string,
'sub_header': string,
'name': string,
'price': string,
'deposit': string,
'price_type': string,
'quantity': string,
'is_from_price_rule': string
}>
}>
}>
export type Page = {
'id': string,
'site_id': string,
'url': string,
'template': string,
'title': string,
'headers': string,
'error': string,
'last_updated': string,
'edm_schedules': Array<{
'id': string,
'type': string,
'last_sent': string,
'rule_settings': string,
'once_schedule': string,
'override': string,
'last_sent_user_id': string,
'last_sent_total': string,
'last_sent_success': string,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
}>
}
export type Pages = Array<{
'id': string,
'site_id': string,
'url': string,
'template': string,
'title': string,
'headers': string,
'error': string,
'last_updated': string,
'edm_schedules': Array<{
'id': string,
'type': string,
'last_sent': string,
'rule_settings': string,
'once_schedule': string,
'override': string,
'last_sent_user_id': string,
'last_sent_total': string,
'last_sent_success': string,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
}>
}>
export type Template = {
'id': string,
'type': string,
'name': string,
'site_id': string,
'subject': string,
'html': string,
'description': string,
'image': string,
'is_fragment': string
}
export type Templates = Array<{
'id': string,
'type': string,
'name': string,
'site_id': string,
'subject': string,
'html': string,
'description': string,
'image': string,
'is_fragment': string
}>
export type Tour = {
'id': string,
'name': string,
'short_desc': string,
'itinerary': string,
'country': string,
'extras': string,
'remarks': string,
'photo_url': string,
'photo_license_url': string,
'max_beds': string,
'room_config': string,
'max_adults': string,
'max_children_no_bed': string,
'max_infants': string,
'price_type': string,
'sort_order': string,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'options': Array<{
'id': string,
'product_code': string,
'name': string,
'type': string,
'min_lead_time': string,
'on_demand_advance_booking': string,
'duration': string,
'SGL': string,
'TWN': string,
'TRP': string,
'QUAD': string,
'CWB': string,
'CNB': string,
'CHT': string,
'INF': string,
'fixed_deposit': string,
'percentage_deposit': string,
'rule': string,
'rule_parsed': string,
'departures': Array<{
'id': string,
'date': string,
'slots_taken': string,
'slots_total': string
}>
}>,
'photos': Array<{
'id': string,
'title': string,
'desc': string,
'url': string,
'license_url': string,
'sequence': string
}>,
'tour_addons': Array<{
'id': string,
'name': string,
'description': string,
'default_option': string,
'image_url': string,
'license_url': string,
'create_date': string,
'options': Array<{
'id': string,
'name': string,
'currency': string,
'SGL': string,
'TWN': string,
'TRP': string,
'QUAD': string,
'CWB': string,
'CNB': string,
'CHT': string,
'INF': string
}>
}>,
'generic_addons': Array<{
'id': string,
'name': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string,
'options': Array<{
'id': string,
'name': string,
'currency': string,
'price': string
}>
}>
}
export type Tours = Array<{
'id': string,
'name': string,
'short_desc': string,
'itinerary': string,
'country': string,
'extras': string,
'remarks': string,
'photo_url': string,
'photo_license_url': string,
'max_beds': string,
'room_config': string,
'max_adults': string,
'max_children_no_bed': string,
'max_infants': string,
'price_type': string,
'sort_order': string,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'options': Array<{
'id': string,
'product_code': string,
'name': string,
'type': string,
'min_lead_time': string,
'on_demand_advance_booking': string,
'duration': string,
'SGL': string,
'TWN': string,
'TRP': string,
'QUAD': string,
'CWB': string,
'CNB': string,
'CHT': string,
'INF': string,
'fixed_deposit': string,
'percentage_deposit': string,
'rule': string,
'rule_parsed': string,
'_subset_departures': Array<{
'id': string,
'date': string,
'slots_taken': string,
'slots_total': string
}>
}>,
'photos': Array<{
'id': string,
'title': string,
'desc': string,
'url': string,
'license_url': string,
'sequence': string
}>,
'tour_addons': Array<{
'id': string,
'name': string,
'description': string,
'default_option': string,
'image_url': string,
'license_url': string,
'create_date': string,
'options': Array<{
'id': string,
'name': string,
'currency': string,
'SGL': string,
'TWN': string,
'TRP': string,
'QUAD': string,
'CWB': string,
'CNB': string,
'CHT': string,
'INF': string
}>
}>,
'generic_addons': Array<{
'id': string,
'name': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string,
'options': Array<{
'id': string,
'name': string,
'currency': string,
'price': string
}>
}>
}>
export type Customer = {
'id': string,
'name': string,
'phone': string,
'email': string,
'registered': string,
'email_subscription': string,
'email_verified': string,
'title': string,
'orders': Array<{
'id': string,
'ref': string,
'title': string,
'user_message': string,
'bill_uid': string,
'order_date': string,
'order_status': string,
'referral_site': string,
'payment_status': string,
'product_status': string,
'deposit_required': string,
'payment_required': string,
'payment_received': string,
'prefix': string,
'first_name': string,
'last_name': string,
'phone': string,
'address': string,
'login_customer_email': string,
'login_customer_category_ids': string,
'checkout_code': string,
'expiry': string,
'adult': string,
'child': string,
'infant': string,
'emergency_contact_name': string,
'emergency_contact_number': string,
'payment_regex': string,
'payment_regex_name': string,
'document_1': string,
'document_2': string,
'document_3': string,
'document_4': string,
'document_5': string,
'unstructured': object,
'customer': {
'id': string,
'name': string,
'phone': string,
'email': string,
'registered': string,
'email_subscription': string,
'email_verified': string,
'title': string
},
'travelers': Array<{
'id': string,
'passport': string,
'expiry': string,
'title': string,
'first_name': string,
'last_name': string,
'country': string,
'birth_date': string,
'type': string,
'flight': string,
'meal': string,
'special': string,
'doc_1_barcode': string,
'doc_2_barcode': string,
'doc_3_barcode': string,
'doc_4_barcode': string,
'doc_5_barcode': string
}>,
'payments': Array<{
'id': string,
'source': string,
'source_id': string,
'amount': string,
'description': string,
'date_added': string,
'details': string,
'status': string
}>,
'products': Array<{
'id': string,
'product_source': string,
'product_source_ref': string,
'product_source_pending_status': string,
'product_source_pending_timestamp': string,
'product_type': string,
'product_id': string,
'product_code': string,
'product_name': string,
'product_detail': object,
'product_category_ids': string,
'product_attributes': object,
'quantity': string,
'from_date': string,
'from_time': string,
'to_date': string,
'to_time': string,
'product_status': string,
'voucher': object,
'form': object,
'items': Array<{
'id': string,
'header': string,
'sub_header': string,
'name': string,
'price': string,
'deposit': string,
'price_type': string,
'quantity': string,
'is_from_price_rule': string
}>
}>
}>,
'additional_info': Array<{
'id': string,
'key': string,
'value': string,
'timestamp': string
}>,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string,
'customer_in_any_price_rules': Array<{
'id': string,
'name': string,
'type': string,
'description': string,
'is_public': string,
'adjustment_name': string,
'adjustment_sequence': string,
'adjustment_price_type': string,
'adjustment_fixed': string,
'adjustment_percentage': string,
'adjustment_percentage_price_attribute': string,
'adjustment_percentage_min': string,
'adjustment_percentage_min_enabled': string,
'adjustment_percentage_max': string,
'adjustment_percentage_max_enabled': string,
'condition_from_datetime': string,
'condition_to_datetime': string,
'condition_checkout_code': string,
'condition_checkout_code_hash': string,
'condition_product_source': string,
'condition_product_type': string,
'condition_mutex_code': string,
'condition_customer_in_any_categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>,
'condition_product_in_any_categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
}>
}>,
'payments': Array<{
'id': string,
'order_id': string,
'amount': string,
'description': string,
'date_added': string,
'details': string,
'status': string,
'order': {
'id': string,
'ref': string,
'title': string,
'user_message': string,
'account_id': string,
'bill_uid': string,
'order_date': string,
'order_status': string,
'referral_site': string,
'payment_status': string,
'product_status': string,
'deposit_required': string,
'payment_required': string,
'payment_received': string,
'prefix': string,
'first_name': string,
'last_name': string,
'phone': string,
'address': string,
'login_customer_email': string,
'login_customer_category_ids': string,
'checkout_code': string,
'expiry': string,
'adult': string,
'child': string,
'infant': string,
'emergency_contact_name': string,
'emergency_contact_number': string,
'payment_regex': string,
'payment_regex_name': string,
'document_1': string,
'document_2': string,
'document_3': string,
'document_4': string,
'document_5': string,
'unstructured': object
}
}>
}
export type Customers = Array<{
'id': string,
'name': string,
'phone': string,
'email': string,
'registered': string,
'email_subscription': string,
'email_verified': string,
'title': string,
'categories': Array<{
'id': string,
'category_type': string,
'name': string,
'parent': string,
'description': string,
'image_url': string,
'license_url': string,
'create_date': string
}>
}>
export type Visa = {
'id': string,
'country_code': string,
'type': string,
'exempt_countries': string,
'exempt_conditions': string,
'cost': string,
'price': string,
'requirements': Array<{
'id': string,
'type': string,
'title': string,
'desc': string
}>,
'prices': Array<{
'id': string,
'countries': string,
'cost': string,
'price': string
}>
}
export type Visas = Array<{
'id': string,
'country_code': string,
'type': string,
'exempt_countries': string,
'exempt_conditions': string,
'cost': string,
'price': string,
'requirements': Array<{
'id': string,
'type': string,
'title': string,
'desc': string
}>,
'prices': Array<{
'id': string,
'countries': string,
'cost': string,
'price': string
}>
}>
export type Config = {
'Website': {
'ABACUS_AFFILIATE_ID': string,
'WCT_AFFILIATE_ID': string,
'WEGO_AFFILIATE_ID': string,
'BOOTSTRAP_NAVBAR': string,
'CAT_NAVBAR_CLASS': string,
'COLOR_SCHEME': string,
'LOCALE_DATE_FORMAT': string,
'NAVBAR_CLASS': string,
'PYTHEAS_STYLE': string,
'SLIDESHOW_FIXED_HEIGHT': string,
'TERMS_AND_CONDITIONS_PAGE': string,
'PDF_PRINT_ENABLED': string,
'HOTEL_CITY_CODE_PROVIDER': string
},
'Company': {
'COMPANY_NAME': string,
'COMPANY_LOGO': string,
'COMPANY_ADDRESS': string,
'COMPANY_PHONE': string,
'INVOICE_FOOTER': string,
'PAYMENT_CURRENCY': string,
'SYSTEM_EMAIL': string,
'TIMEZONE': string,
'TOUR_ADMIN_EXPIRY': string,
'FLIGHT_FIXED_MARKUP': string,
'FLIGHT_PERCENTAGE_MARKUP': string,
'FLIGHT_AIRLINE_BLACKLIST': string,
'FLIGHT_ORIGIN_WHITELIST': string,
'CUSTOMER_COUNTRY_WHITELIST': string,
'FLIGHT_PACKAGE_HOTEL': string,
'FLIGHT_LEAD_TIME': string,
'HOTEL_LEAD_TIME': string
},
'Experimental': {
'SEND_INVOICE_AFTER_ORDER_FULFILLED': string
},
'eNETS Lite': {
'ENETS_URL': string,
'ENETS_MID': string
},
'eNETS': {
'ENETS_UMAPI_URL': string,
'ENETS_UMAPI_MID': string,
'ENETS_UMAPI_PUBLIC_KEY_NAME': string
},
'Wirecard': {
'WIRECARD_HPP_URL': string,
'WIRECARD_MID': string,
'WIRECARD_SECRET': string,
'WIRECARD_PSP_NAME': string
},
'Paypal': {
'PAYPAL_EMAIL': string,
'PAYPAL_HOST': string
},
'PayDollar': {
'PAYDOLLAR_URL': string,
'PAYDOLLAR_MERCHANT_ID': string,
'PAYDOLLAR_HASH_ALGO': string,
'PAYDOLLAR_SECURE_HASH_SECRET': string
},
'GTA': {
'GTA_CLIENT_ID': string,
'GTA_EMAIL': string,
'GTA_PASSWORD': string,
'GTA_REQUEST_URL': string,
'GTA_FIXED_MARKUP': string,
'GTA_PERCENTAGE_MARKUP': string
},
'WebConnect': {
'WEBCONNECT_EPR': string,
'WEBCONNECT_PASSCODE': string,
'WEBCONNECT_IPCC': string,
'WEBCONNECT_AGENCY_PHONE_LOCATION_CODE': string,
'WEBCONNECT_AGENCY_PHONE_NUMBER': string,
'WEBCONNECT_PAYMENT_RECEIVED_HOST_COMMANDS': string,
'WEBCONNECT_URL': string,
'WEBCONNECT_PAYMENT_PENDING_QUEUE': string,
'WEBCONNECT_PAYMENT_RECEIVED_QUEUE': string,
'WEBCONNECT_UNSUCCESSFUL_QUEUE': string
},
'Powersuite': {
'POWERSUITE_EPR': string,
'POWERSUITE_IPCC': string,
'POWERSUITE_PASSCODE': string,
'POWERSUITE_URL': string,
'POWERSUITE_HOTEL_PRODUCT_CODE_DEFAULT': string,
'POWERSUITE_HOTEL_SUPPLIER_CODE_DEFAULT': string,
'POWERSUITE_TOUR_PRODUCT_CODE_DEFAULT': string,
'POWERSUITE_TOUR_SUPPLIER_CODE_DEFAULT': string
},
'Cybersource': {
'CYBERSOURCE_MERCHANT_ID': string,
'CYBERSOURCE_TRANSACTION_KEY': string,
'CYBERSOURCE_WSDL_URL': string
},
'Travelport': {
'TRAVELPORT_USERNAME': string,
'TRAVELPORT_PASSWORD': string,
'TRAVELPORT_BRANCH_CODE': string,
'TRAVELPORT_AGENCY_NAME': string,
'TRAVELPORT_AGENCY_PHONE': string,
'TRAVELPORT_URL': string,
'TRAVELPORT_PAYMENT_PENDING_QUEUE': string,
'TRAVELPORT_PAYMENT_RECEIVED_QUEUE': string,
'TRAVELPORT_UNSUCCESSFUL_QUEUE': string
},
'Reddot': {
'REDDOT_URL': string,
'REDDOT_MERCHANT_ID': string,
'REDDOT_KEY': string,
'REDDOT_SECRET_KEY': string
},
'Admin Payment': {
'ADMIN_PAYMENT_LABEL_1': string,
'ADMIN_PAYMENT_CHOICES_1': string,
'ADMIN_PAYMENT_LABEL_2': string,
'ADMIN_PAYMENT_CHOICES_2': string,
'ADMIN_PAYMENT_LABEL_3': string,
'ADMIN_PAYMENT_CHOICES_3': string,
'ADMIN_PAYMENT_LABEL_4': string,
'ADMIN_PAYMENT_CHOICES_4': string,
'ADMIN_PAYMENT_LABEL_5': string,
'ADMIN_PAYMENT_CHOICES_5': string
},
'Bedsonline': {
'BEDSONLINE_USERNAME': string,
'BEDSONLINE_PASSWORD': string,
'BEDSONLINE_URL': string,
'BEDSONLINE_SHOW_OPAQUE_CONTRACT': string,
'BEDSONLINE_FIXED_MARKUP': string,
'BEDSONLINE_PERCENTAGE_MARKUP': string
},
'Amadeus': {
'AMADEUS_URL': string,
'AMADEUS_USERNAME': string,
'AMADEUS_PASSWORD': string,
'AMADEUS_PCC': string,
'AMADEUS_AGENT_SIGN': string,
'AMADEUS_RECEIVED_QUEUE': string
},
'GlobalPayments': {
'GLOBALPAYMENTS_MARCHANT_ID': string,
'GLOBALPAYMENTS_ACCESSCODE': string,
'GLOBALPAYMENTS_SECURESECRET': string,
'GLOBALPAYMENTS_REQUEST_URL': string
},
'WebCollect': {
'WEBCOLLECT_MERCHANTID': string,
'WEBCOLLECT_ENDPOINT': string
},
'WebCollect UnionPay': {
'WEBCOLLECT_CUP_MERCHANTID': string,
'WEBCOLLECT_CUP_ENDPOINT': string
},
'Acase': {
'ACASE_COMPANY_ID': string,
'ACASE_USER_ID': string,
'ACASE_PASSWORD': string,
'ACASE_URL': string,
'ACASE_FIXED_MARKUP': string,
'ACASE_PERCENTAGE_MARKUP': string
},
'Intellsoft': {
'INTELLSOFT_USERNAME': string,
'INTELLSOFT_PASSWORD': string,
'INTELLSOFT_AGENT_ID': string,
'INTELLSOFT_URL': string
},
'Hoiio': {
'HOIIO_APP_ID': string,
'HOIIO_ACCESS_TOKEN': string,
'HOIIO_SMS_REBRAND_ENABLED': string
},
'TTC': {
'TTC_ENV': string,
'TTC_API_KEY': string
},
'GlobalTix': {
'GLOBALTIX_URL': string,
'GLOBALTIX_USERNAME': string,
'GLOBALTIX_PASSWORD': string
}
}
export type FlightSearch = Array<{
'od1': {
'id': string,
'segments': Array<{
'marketing_carrier': {
'code': string,
'name': string
},
'flight_number': string,
'cabin': string,
'connected': boolean,
'operating_carrier': {
'code': string,
'name': string
},
'flights': Array<{
'departure_airport': {
'code': string,
'name': string
},
'departure_terminal': string,
'departure_datetime': string,
'arrival_airport': {
'code': string,
'name': string
},
'arrival_terminal': string,
'arrival_datetime': string,
'equipment': string
}>
}>
},
'od2': {
'id': string,
'segments': Array<{
'marketing_carrier': {
'code': string,
'name': string
},
'flight_number': string,
'cabin': string,
'connected': boolean,
'operating_carrier': {
'code': string,
'name': string
},
'flights': Array<{
'departure_airport': {
'code': string,
'name': string
},
'departure_terminal': string,
'departure_datetime': string,
'arrival_airport': {
'code': string,
'name': string
},
'arrival_terminal': string,
'arrival_datetime': string,
'equipment': string
}>
}>
},
'fares': Array<{
'id': string,
'source': string,
'cost': string,
'price': string,
'currency': string,
'plating_carrier': string,
'ptc_breakdown_map': {
[key: string]: {
'quantity': number,
'cost': string,
'price': string,
'base_fare': string,
'currency': string
}
},
'ptc_fare_detail_map': {
[key: string]: Array<{
'id': string,
'origin': {
'code': string,
'name': string
},
'destination': {
'code': string,
'name': string
},
'fare_basis': string,
'booking_code': string,
'cabin': string,
'booking_count': string,
'baggage_pieces': number,
'baggage_weight_amount': string,
'baggage_weight_unit': string,
'brand': {
'id': string,
'name': string,
'description': string,
'image': string,
'services': Array<{
'included': boolean,
'name': string,
'description': string
}>
},
'rule': Array<{
'category_number': number,
'category_description': string,
'text': string
}>
}>
}
}>
}>
export type FlightDetail = {
'od1': {
'id': string,
'segments': Array<{
'marketing_carrier': {
'code': string,
'name': string
},
'flight_number': string,
'cabin': string,
'connected': boolean,
'operating_carrier': {
'code': string,
'name': string
},
'flights': Array<{
'departure_airport': {
'code': string,
'name': string
},
'departure_terminal': string,
'departure_datetime': string,
'arrival_airport': {
'code': string,
'name': string
},
'arrival_terminal': string,
'arrival_datetime': string,
'equipment': string
}>
}>
},
'od2': {
'id': string,
'segments': Array<{
'marketing_carrier': {
'code': string,
'name': string
},
'flight_number': string,
'cabin': string,
'connected': boolean,
'operating_carrier': {
'code': string,
'name': string
},
'flights': Array<{
'departure_airport': {
'code': string,
'name': string
},
'departure_terminal': string,
'departure_datetime': string,
'arrival_airport': {
'code': string,
'name': string
},
'arrival_terminal': string,
'arrival_datetime': string,
'equipment': string
}>
}>
},
'fares': Array<{
'id': string,
'source': string,
'cost': string,
'price': string,
'currency': string,
'plating_carrier': string,
'ptc_breakdown_map': {
[key: string]: {
'quantity': number,
'cost': string,
'price': string,
'base_fare': string,
'currency': string
}
},
'ptc_fare_detail_map': {
[key: string]: Array<{
'id': string,
'origin': {
'code': string,
'name': string
},
'destination': {
'code': string,
'name': string
},
'fare_basis': string,
'booking_code': string,
'cabin': string,
'booking_count': string,
'baggage_pieces': number,
'baggage_weight_amount': string,
'baggage_weight_unit': string,
'brand': {
'id': string,
'name': string,
'description': string,
'image': string,
'services': Array<{
'included': boolean,
'name': string,
'description': string
}>
},
'rule': Array<{
'category_number': number,
'category_description': string,
'text': string
}>
}>
}
}>
}
