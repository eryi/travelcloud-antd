import React from 'react'
import { TravelCloudClient, Cart, validateFlightsParams, TcResponse, FlightsParams } from '../travelcloud'
import { FlightsParamsAdvancedForm } from '../components/flights-params-form'
import { FlightsResult } from '../components/flights-result'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import { FlightSearch, FlightDetail } from '../types'

export default class extends React.PureComponent<{cart: Cart, query}> {
  private client = new TravelCloudClient(config.tcUser);

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    // flights only contains the cheapest fares
    // flight contains all fares for all cabin class
    flights: {} as TcResponse<FlightSearch>,
    flightMap: {} as {[key: string]: TcResponse<FlightDetail>},
    flightsParam: {
      source: 'travelport',
      'od1.origin_airport.code': this.props.query['od1.origin_airport.code'],
      'od1.origin_datetime': this.props.query['od1.origin_datetime'],
      'od1.destination_airport.code': this.props.query['od1.destination_airport.code'],
      'od2.origin_airport.code': this.props.query['od2.origin_airport.code'],
      'od2.origin_datetime': this.props.query['od2.origin_datetime'],
      'od2.destination_airport.code': this.props.query['od2.destination_airport.code'],
      ptc_adt: this.props.query.ptc_adt || 1,
      ptc_cnn: this.props.query.ptc_cnn || 0,
      ptc_inf: this.props.query.ptc_inf || 0,
      cabin: this.props.query.cabin || 'Y',
    },
    type: 'return'
  }

  swap() {
    const od1Origin = this.state.flightsParam["od1.origin_airport.code"]
    const od2Origin = this.state.flightsParam["od2.origin_airport.code"]
    const newValue = Object.assign({}, this.state.flightsParam)
    newValue["od1.origin_airport.code"] = od2Origin
    newValue["od2.origin_airport.code"] = od1Origin
    this.setState({flightsParam: newValue})
  }

  async componentDidMount() {
    const nullOrEmpty = (x) => x == null || x === ""
    if (!nullOrEmpty(this.state.flightsParam["od1.destination_airport.code"]) || !nullOrEmpty(this.state.flightsParam["od2.destination_airport.code"])) {
      this.typeChange('multi')
    }

    // check if current params are valid
    const validated = validateFlightsParams(this.state.flightsParam)

    if (validated != null) {
      this.flights(validated)
    }
  }

  async paramsChange(value) {
    this.setState({flightsParam: value})
  }

  typeChange = (type: string) => {
    let result
    if (type === 'return') {
      this.setState(Object.assign(this.state.flightsParam, {
        "od1.destination_code": undefined,
        "od2.destination_code": undefined
      }))
    }

    if (type === 'one') {
      this.setState(Object.assign(this.state.flightsParam, {
        "od1.destination_code": undefined,
        "od2.destination_code": undefined,
        "od2.origin_departure": undefined
      }))
    }

    this.setState({type})
  }

  async flights(value) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: value
    })

    this.client.cancelFlights()

    this.setState({flights: {loading: true}, flightsParam: value, details: {}})

    const flights = await this.client.flights(value)

    this.setState({flights, flightMap: {}})
  }

  async flight(index, flight) {
    var flightMap: {[key: string]: TcResponse<FlightDetail>}= Object.assign({}, this.state.flightMap, {[index]: {loading: true}})
    this.setState({flightMap})

    const detail = await this.client.flight({
      source: 'travelport',
      'od1.id': flight.od1.id,
      'od2.id': flight.od2 ? flight.od2.id : null,
      ptc_adt: flight.fares[0].ptc_breakdown_map.adt.quantity,
      ptc_cnn: flight.fares[0].ptc_breakdown_map.cnn ? flight.fares[0].ptc_breakdown_map.cnn.quantity : 0,
      ptc_inf: flight.fares[0].ptc_breakdown_map.inf ? flight.fares[0].ptc_breakdown_map.inf.quantity : 0,
    })

    flightMap = Object.assign({}, this.state.flightMap, {[index]: detail})
    this.setState({flightMap})
  }

  render() {
    const myStyle = {padding: "20px 40px"}
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Flights</title>
        </Head>
          <h1>Flights</h1>
          <FlightsParamsAdvancedForm
            client={this.client}
            onTypeChange={this.typeChange}
            onChange={(value) => this.paramsChange(value)}
            onSearch={(value) => this.flights(value)}
            onSwapClick={() => this.swap()}
            value={this.state.flightsParam}
            type={this.state.type}
            defaultIataCodes={['SIN', 'BKK', 'KUL', 'HKG']}
            loading={this.state.flights.loading === true}
          />
          <div style={{marginTop: 20}}>&nbsp;</div>
          {this.state.flights.result == null || this.state.flights.result.length === 0
            ? <NoResult
                style={{paddingTop: 128}}
                response={this.state.flights}
                type="flights"
                loadingMessage="Searching for the lowest fares...">
              </NoResult>
            : <FlightsResult
                cart={this.props.cart}
                flights={this.state.flights.result}
                flightMap={this.state.flightMap}
                onFlightClick={(index, flight) => this.flight(index, flight)}
                onFareClick={(index, flight, fareId) => {
                  this.props.cart.reset().addFlight(flight, fareId)
                  Router.push('/checkout')}} />}
      </div>
    )
  }
}
