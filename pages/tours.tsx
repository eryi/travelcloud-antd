import React from 'react'
import { Row, Col, Menu, Dropdown } from 'antd'
import { TravelCloudClient } from '../travelcloud'
import { ToursResult } from '../components/tours-result'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'

export default class extends React.Component<any, any> {
  static async getInitialProps(context) {
    const client = new TravelCloudClient(config.tcUser)
    const tours = await client.tours(Object.assign({ 'categories.name': 'Tours' }, context.query))
    const query = context.query
    const categories = await client.categories({ category_type: 'tour_package' })
    return { tours, query, categories }
  }

  state = {
    loading: false,
    'categories.name/and': this.props.query['categories.name/and'] || 'Tours',
    currentSelect: 'show1',
    computedTour: 'Full Package'
  }

  componentDidUpdate(prevProps) {
    if (prevProps.tours !== this.props.tours) {
      this.setState({
        loading: false
      })
    }
  }

  menuClick({ item, key }) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: { 'categories.name': key }
    })

    // update state with filter values and loading true
    this.setState({ loading: true, "categories.name": key })
  }

  seasonChange = (e) => {
    Router.push({
      pathname: Router.pathname,
      query: { 'categories.name/and': e }
    })

    this.setState({
      "categories.name/and": e
    })
  }

  onTourClick(tour) {
    Router.push({
      pathname: '/tour',
      query: { id: tour.id }
    })
    this.setState({
      loading: true
    })
  }

  handleClick = (e) => {
    if (e.key === 'show1') {
      this.setState({
        currentSelect: e.key,
        computedTour: 'Full Package',
        filterByCategory: 'Tours'
      });


    }
    else if (e.key === 'show2') {
      this.setState({
        currentSelect: e.key,
        computedTour: 'Land Tour',
        filterByCategory: 'Tours'
      });

    }
    else if (e.key === 'show3') {
      this.setState({
        currentSelect: e.key,
        computedTour: null,
        filterByCategory: 'Day Tours',
      });
    }

  }

  render() {
    const catSelect = [
      <Menu.Item key="show1">
        <a onClick={() => {
          Router.push({
            pathname: Router.pathname,
            query: { ...this.props.query, 't': 'Tours' }
          })
        }
        }>Tours + Flights</a>
      </Menu.Item>,
      <Menu.Item key="show2">
        <a onClick={() => {
          Router.push({
            pathname: Router.pathname,
            query: { ...this.props.query, 't': 'Tours' }
          })
        }
        }>Land Tours</a>
      </Menu.Item>,
      <Menu.Item key="show3">
        <a onClick={() => {
          Router.push({
            pathname: Router.pathname,
            query: { ...this.props.query, 't': 'Day Tours' }
          })
        }
        }>Day Tours</a>
      </Menu.Item>
    ]

    const catSmall = (
      <Menu selectedKeys={[this.state.currentSelect]} onClick={this.handleClick}>
        {catSelect}
      </Menu>
    )
    return (
      <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32 }}>
        <Head>
          <title>{config.defaultTitle} | Tours</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
            <h1>Tours</h1>
            <Menu
              selectedKeys={[this.state['categories.name']]}
              onClick={(x) => this.menuClick(x)}
              mode="vertical">
              {this.props.categories.result != null
                && this.props.categories.result.map(category =>
                  <Menu.Item key={category.name}>
                    <span>{category.name}</span>
                  </Menu.Item>)}
            </Menu>
          </Col>
          <Col span={18}>

            <Menu id="category"
              selectedKeys={[this.state.currentSelect]} onClick={this.handleClick} mode="horizontal" className="visible-lg">
              {catSelect}
            </Menu>
            <Dropdown overlay={catSmall} className="hidden-lg">
              <a className="ant-dropdown-link" href="#"></a>
            </Dropdown>
            {this.state.loading || this.props.tours.result == null || this.props.tours.result.length === 0
              ? <NoResult
                style={{ paddingTop: 128 }}
                response={this.props.tours}
                loading={this.state.loading}
                type="tours" />
              : <ToursResult
                filterOptionType={this.state.computedTour}
                toursResult={this.props.tours.result}
                cart={this.props.cart}
                onTourClick={(tour) => this.onTourClick(tour)} />}
          </Col>
        </Row>
      </div>
    )
  }
}

