import React, { Fragment } from 'react'

import config from '../customize/config'

import { TravelCloudClient, FlightsParams, Cart } from '../travelcloud'
import { SearchForm } from '../components/search-form';
import { SearchAndResult } from '../components/globaltix-serach-and-details';
import AttractionModal from '../components/attraction-detail';
import TicketModal from '../components/ticket-detail';

import { Spin, Pagination } from 'antd';

export default class extends React.PureComponent<{ cart: Cart, openCart: () => void }> {
    private client = new TravelCloudClient(config.tcUser);

    static async getInitialProps(context) {
        const query = context.query
        return { query }
    }

    /**
     * State properties of serach page
     */
    state = {
        countryList: [],
        cityList: [],
        attractionList: [],
        show: false,
        ticketModal: false,
        searchFormKeys: {
            "keyword": "",
            "countryId": "",
            "tab": "ALL",
            "cityId": "",
            "product_type": "",
        },
        page: '',
        loading: true,
        startLimit: 0,
        endLimit: 10,
        attractionDetail: [],
        ticketDetail: [],
        qtyMap: {}
    }

    /**
    * Load all the data according to the state values
    */
    async loadData() {
        var countryId = this.state.searchFormKeys.countryId;
        if (this.state.searchFormKeys.countryId == '') {
            var countryId = '1';
        }
        const attractionList = await this.client.globaltix('attraction/list', { countryId: countryId, page: this.state.page, keyword: this.state.searchFormKeys.keyword, tab: this.state.searchFormKeys.tab, cityId: this.state.searchFormKeys.cityId, field: 'description' })
        const attractionresult = attractionList.result.data;
        this.setState({ attractionList: attractionresult, loading: false });
    }

    /**
     * Called after the component is mounted to retrive data
     */
    async componentDidMount() {
        // step 1
        // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#6a8ab8a6-c06d-9ee8-dd58-ddb0ea34f339
        const countryList = await this.client.globaltix('country/getAllListingCountry');
        const result = countryList.result.data;
        this.setState({ countryList: result });

        // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#b26a2600-47c2-a452-98c4-c2895941788c
        const cityList = await this.client.globaltix('city/getAllCities');
        const cityresult = cityList.result.data;
        this.setState({ cityList: cityresult });

        // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#f24b9cfb-4c15-ddf7-b06d-b555668be893
        this.loadData();
        // step 2
        // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#7964838c-8613-755f-a31b-5de91c6ca1d4
        // const ticketTypeList = await this.client.globaltix('ticketType/list', {attraction_id: 21})

        // step 3
        // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#13fdc072-2d95-a19f-b4e3-408bf0a35ff1
        // const attractionGet = await this.client.globaltix('attraction/get', {id: 21})

        // https://documenter.getpostman.com/view/3866783/globaltix-partner-api/RVnSH2gE#a5717943-9902-14e3-b153-b122d1a794af

    }

    /**
     * Serach the data according to the selected state searchFormKeys
     * @param id Selected serach key id or keyword
     * @param e selected state searchFormKey type
     */
    Searchdata(id, e) {
        let searchStates = this.state.searchFormKeys;
        searchStates[e] = id;
        if (e == 'countryId') {
            searchStates['cityId'] = "";
        }
        this.setState({ searchFormKeys: searchStates })
        this.setState({ loading: true });
        this.loadData();
    }

    /**
     * Retrieve ticket model details
     * @param id ticket id
     */
    async getTicketDetail(id) {
        const ticketTypeGet = await this.client.globaltix('ticketType/get', { id: id })
        var details = ticketTypeGet.result.data;
        this.setState({ ticketDetail: details, show: false, ticketModal: true });
    }

    /**
     * Retrieve attraction model details
     * @param details
     */
    showAttractionDetail(details) {
        this.setState({ attractionDetail: details, show: true, ticketModal: false });
    }

    /**
     * Retrive ticket id using search result props
     * @param details
     */
    showTicketDetail(details) {
        this.getTicketDetail(details.id);
    }

    /**
     * Keep track of the page number for pagination
     * @param pageNumber
     */
    onPageChange(pageNumber) {
        this.setState({ page: pageNumber });
        var total = this.state.attractionList.length; //total items in array
        var limit = 10; //per page
        var totalPages = Math.ceil(total / limit); //calculate total pages
        var offset = (pageNumber - 1) * limit;
        if (offset < 0) offset = 0;
        var endlimit = Math.ceil(offset + limit);
        this.setState({ startLimit: offset, endLimit: endlimit });
    }

    /**
     * Describes the elements on the search page
     * @return {String} HTML elements
     */
    render() {
        return (
            <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32 }}>
                <Fragment key="2">
                    <section id="content" key="3">
                        <article id="search" key="a1">
                            <SearchForm countryList={this.state.countryList} cityList={this.state.cityList} searchFormKeys={this.state.searchFormKeys} onSearch={(value, detail) => this.Searchdata(value, detail)} />
                        </article>
                        <article id="result" key="a2">
                            {this.state.loading ?
                                <div className="loader"><Spin tip="Loading..." spinning={this.state.loading} /></div> : ''
                            }
                            {!this.state.loading &&
                                <SearchAndResult
                                    offset={this.state.startLimit}
                                    limit={this.state.endLimit}
                                    data={this.state.attractionList}
                                    onAttractionClick={(details) => this.showAttractionDetail(details)}
                                    onTicketClick={(details) => this.showTicketDetail(details)}
                                    onAddToCart={(attraction, ticket) => {
                                        this.props.cart.addGlobaltix(attraction, {
                                            id: ticket.id,
                                            quantity: this.state.qtyMap[ticket.id],
                                            fromResellerId: ticket.from
                                        })
                                        this.props.openCart()
                                    }}
                                    qtyMap={this.state.qtyMap}
                                    onQtyChange={(ticketId, newQty) => {
                                        const qtyMap = { ...this.state.qtyMap }
                                        qtyMap[ticketId] = newQty;
                                        this.setState({ qtyMap });
                                    }} />
                            }
                        </article>
                    </section>
                    <AttractionModal data={this.state.attractionDetail} handleClose={() => this.setState({ show: false })} show={this.state.show} />

                    <TicketModal data={this.state.ticketDetail} handleClose={() => this.setState({ ticketModal: false })} show={this.state.ticketModal} />
                </Fragment>
                {!this.state.loading ?
                    <div className="pagi-wrapper">
                        <Pagination defaultCurrent={1} total={this.state.attractionList.length} onChange={(e) => this.onPageChange(e)} /> </div> : ''
                }
            </div>
        )
    }
}
