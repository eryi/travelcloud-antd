import React from 'react'
import { Row, Col, Carousel } from 'antd'
import { TravelCloudClient, TcResponse, Cart, extractValueFromFormState } from '../travelcloud'
import config from '../customize/config'
import {TourBookingForm, initialTourBookingForm} from '../components/tour-booking'
import Router from 'next/router'
import { Tour } from '../travelcloud-api';

export default class extends React.PureComponent<{tour: TcResponse<Tour>, cart: Cart}> {
  state = {tourBookingForm: null}
  static getDerivedStateFromProps(props, currentState) {
    if (props.tour.result != null && currentState.tourBookingForm == null) {
      return {
        tourBookingForm: initialTourBookingForm(props.query.id)
      }
    }
    return null
  }

  static async getInitialProps (context) {
    const client = new TravelCloudClient(config.tcUser)
    const tour = await client.tour(context.query)
    const query = context.query
    return { tour, query }
  }

  render() {
    const tour = this.props.tour.result

    if (tour == null) return <div>Tour not found</div>

    return (
      <div>
        <Row style={{margin: '32px 64px'}}>
          <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h1>
        </Row>
        <Row style={{backgroundColor: '#fff', margin: '32px 64px', height: '650px'}}>
          <Col span={12}>
            <Carousel autoplay>
              <div key='cover'><div style={{
                backgroundImage: `url(${tour.photo_url})`,
                height: '650px',
                width: '100%',
                backgroundSize: 'cover',
                backgroundPosition: 'center'}} /></div>
              {tour.photos.map((photo, index) =>
                <div key={index}><div style={{
                  backgroundImage: `url(${photo.url})`,
                  height: '650px',
                  width: '100%',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center'}} /></div>)}
            </Carousel>
          </Col>
          <Col span={12} style={{height: '100%'}}>
            <TourBookingForm
              cart={this.props.cart}
              value={this.state.tourBookingForm}
              tour={tour}
              onChange={(tourBookingForm) => this.setState({tourBookingForm})}
              onSubmit={(e) => {
                this.props.cart.reset().addTour(tour, this.state.tourBookingForm)
                Router.push('/checkout')
                e.preventDefault()
              }} />
          </Col>
        </Row>
        <Row gutter={64} style={{backgroundColor: '#fff', margin: '32px 64px', padding: 32}}>
          <Col span={12}>
            <h1>Highlights</h1>
            <div dangerouslySetInnerHTML={{__html: tour.short_desc}} />
            <h1>Extras</h1>
            <div dangerouslySetInnerHTML={{__html: tour.extras}} />
            <h1>Remarks</h1>
            <div dangerouslySetInnerHTML={{__html: tour.remarks}} />
          </Col>
          <Col span={12}>
            <h1>Itinerary</h1>
            <div dangerouslySetInnerHTML={{__html: tour.itinerary}} />
          </Col>
        </Row>
      </div>
    )
  }
}
