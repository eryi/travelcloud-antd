import App, { Container } from 'next/app'
import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import * as NProgress from 'nprogress/nprogress'
import { Layout, Menu, Icon, Button, Col, Drawer } from 'antd'
import '../customize/styles.less'
import Router from 'next/router'
import config from '../customize/config'
import { Cart, TravelCloudClient } from '../travelcloud'
import { Order } from '../components/order'
import { Account } from '../components/account'


const { Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;
Router.events.on('routeChangeStart', (url) => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

export default class MyApp extends App {

  rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

  state = {
    // for cart
    order: { loading: true } as any,
    customer: { loading: true },
    priceRules: { loading: true },
    token: null,
    cart: null,

    // for _app
    collapsed: true,
    siderAction: '',
    collapsedMenu: true,

    // error
    hasError: false,
    openKeys: ['/'],

    // Gitlab-link
    link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/_app.tsx',

    // Drawer
    visible: false
  }

  client: TravelCloudClient = new TravelCloudClient(config.tcUser)

  componentDidMount() {
    this.setState({
      cart: new Cart({
        client: this.client,
        onOrderChange: (order) => this.setState({ order }),
        onCustomerChange: (customer) => this.setState({ customer }),
        onPriceRulesChange: (priceRules) => this.setState({ priceRules })
      },
      )
    })
  }

  openCart = async () => {
    this.setState({
      siderAction: 'cart',
      collapsed: false
    })
  }
  openAccount = async () => {
    this.setState({
      siderAction: 'account',
      collapsed: false
    })

    await this.state.cart.refreshCustomer()
  }
  closeCart = () => {
    if (this.state.collapsed === false) {
      this.setState({
        siderAction: {},
        collapsed: true,
      })
    }
  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
    // You can also log the error to an error reporting service
    //logErrorToMyService(error, info);
  }

  /**
   * Page navigation for drawer menu
   */
  onClickSideMenu = (e) => {

    if (e.key === 'tour') {
      Router.push({
        pathname: '/tour',
        query: { 'id': 169 }
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/tour.tsx'
      })
    } if (e.key === 'hotel') {
      Router.push({
        pathname: '/hotel',
        query: {
          'hotelId': 69622,
          'cityCode': 'SIN',
          'checkInDate': '2020-03-19',
          'checkOutDate': '2020-03-22',
          'adults': 2,
          'children': 0
        }
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/hotel.tsx'
      })
    } if (e.key === 'tour-search') {
      Router.push({
        pathname: '/tour-search',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/tour-search.tsx'
      })
    } if (e.key === 'globaltix-1') {
      Router.push({
        pathname: '/globaltix-details',
        query: { 'result': 1211 }
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/globaltix-details.tsx'
      })
    } if (e.key === 'globaltix-2') {
      Router.push({
        pathname: '/globaltix-search-and-details',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/globaltix-search-and-details.tsx'
      })
    }

    if (e.key === 'checkout') {
      Router.push({
        pathname: '/checkout',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/checkout.tsx'
      })
    } if (e.key === 'price-rules') {
      Router.push({
        pathname: '/price-rules',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/price-rules.tsx'
      })
    } if (e.key === 'flights-tabbed') {
      Router.push({
        pathname: '/flights-tabbed',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/flights-tabbed.tsx'
      })
    } if (e.key === 'hotels') {
      Router.push({
        pathname: '/hotels',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/hotels.tsx'
      })
    } if (e.key === 'tours') {
      Router.push({
        pathname: '/tours',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/tours.tsx'
      })
    } if (e.key === 'generics') {
      Router.push({
        pathname: '/generics',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/generics.tsx'
      })
    } if (e.key === 'payment') {
      Router.push({
        pathname: '/payment',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/payment.tsx'
      })
    } if (e.key === 'contact') {
      Router.push({
        pathname: '/contact',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/contact.tsx'
      })
    } if (e.key === 'globaltix') {
      Router.push({
        pathname: '/globaltix-search',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/globaltix-search.tsx'
      })
    } if (e.key === 'home') {
      Router.push({
        pathname: '/',
      })
      this.setState({
        link: 'https://gitlab.com/travel-click/travelcloud-antd/blob/master/pages/_app.tsx'
      })
    }
  }

  /**
   * Set drawer open
   */
  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  /**
   * Set drawer close
   */
  drawrOnClose = () => {
    this.setState({
      visible: false,
    });
  };

  /**
   * Navigate to GitLab
   */
  fetchUrl = () => {
    window.open(this.state.link);
  }

  render() {
    const { Component, pageProps } = this.props
    const cartIsEmpty = this.state.order.result == null || this.state.order.result.products == null || this.state.order.result.products.length === 0

    return (
      <Container>
        <Layout hasSider={true}>
          <Layout className={this.state.collapsed === false && 'tc-dim-no-scroll-bars'} onClick={this.closeCart} style={{ overflow: 'hidden' }}>
            <Menu
              theme="dark"
              mode="horizontal"
              style={{ lineHeight: '64px', borderBottom: 0 }}>
              <Menu.Item key="cart" style={{ float: 'right' }} onClick={() => this.openCart()}>
                <Icon type="shopping-cart" style={{ fontSize: 16 }} /> Cart
                </Menu.Item>
              <Menu.Item key="customer" style={{ float: 'right' }} onClick={() => this.openAccount()}>
                <Icon type="user" style={{ fontSize: 16 }} /> My Account
                </Menu.Item>
              <Menu.Item key="gitlab" style={{ float: 'right' }} onClick={() => this.fetchUrl()} >
                <Icon type="github" />View in GitLab
                </Menu.Item>
              <Menu.Item key="drawer" style={{ float: 'left' }} onClick={this.showDrawer} >
                <Icon type="bars" />
              </Menu.Item>
            </Menu>
            <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px' }}>
              <Col span={18}>
                <div>
                  <Content style={{ minHeight: 1000 }}>
                    {
                      this.state.hasError
                        ? <div style={{ padding: "200px 0", textAlign: "center", fontSize: "20px" }}><Icon type="warning" /> An error has occured</div>
                        : <Component priceRules={this.state.priceRules} client={this.client} customer={this.state.customer} order={this.state.order} cart={this.state.cart} openCart={() => this.openCart()} {...pageProps} />
                    }
                  </Content>
                </div>
              </Col>
            </div>
            <Footer style={{ textAlign: 'center' }}>
              {config.companyName} ©2018
           </Footer>
          </Layout>
          <Sider
            className="cart-sider"
            collapsedWidth={0}
            collapsible
            collapsed={this.state.collapsed}
            theme="light"
            width={700}
            trigger={null}
            style={{ width: 700, overflow: 'auto', height: '100vh', position: 'fixed', right: 0, zIndex: 2, boxShadow: '-1px 0 5px 0px rgba(0, 0, 0, 0.2), -4px 0 10px 0px rgba(0, 0, 0, 0.1)' }}>
            <div style={{ padding: "32px", position: "relative", width: 700 }}>
              <Icon type="close" style={{ fontSize: 24, position: 'absolute', top: 32, right: 32, cursor: 'pointer' }} onClick={this.closeCart} />
              {this.state.siderAction === 'cart'
                && <div>
                  <div style={{ paddingBottom: 16, fontSize: 24, paddingRight: 16, lineHeight: "32px" }}>Shopping Cart</div>
                  {cartIsEmpty
                    ? <div>Your cart is empty</div>
                    : [
                      <Order order={this.state.order.result} showSection={{ products: true, remove: true }} cart={this.state.cart} key={1} />,
                      <Button
                        key={2}
                        onClick={() => {
                          this.setState({ collapsed: true })
                          Router.push('/checkout')
                        }}
                        style={{ marginTop: 16, float: 'right' }}
                        type="primary">Proceed to checkout <Icon type="right" /></Button>]
                  }</div>}
              {this.state.siderAction === 'account'
                && <Account customer={this.state.customer} cart={this.state.cart} />}
            </div>
          </Sider>
        </Layout>
        <Layout>
          <div>
            <Drawer
              title="Pages"
              placement='left'
              closable={false}
              onClose={this.drawrOnClose}
              visible={this.state.visible}
              style={{ padding: 0 }}
            >
              <Menu
                onClick={this.onClickSideMenu}
                style={{ width: '100%' }}
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                mode="inline"
              >
                <SubMenu key="/" title={<span><span>Home</span></span>} >
                  <Menu.Item key="home">Main</Menu.Item>
                </SubMenu>
                <SubMenu key="/tours" title={<span><span>Tours</span></span>}>
                  <Menu.Item key="tours">All tours</Menu.Item>
                  <Menu.Item key="tour">Selected Tour</Menu.Item>
                  <Menu.Item key="tour-search">Tour filter by category</Menu.Item>
                </SubMenu>
                <SubMenu key="/generics" title={<span><span>Tickets</span></span>}>
                  <Menu.Item key="generics">Select ticket by type</Menu.Item>
                </SubMenu>
                <SubMenu key="/hotels" title={<span><span>Hotels</span></span>}>
                  <Menu.Item key="hotels">Select available hotels</Menu.Item>
                  <Menu.Item key="hotel">Selected hotel</Menu.Item>
                </SubMenu>
                <SubMenu key="/flights" title={<span><span>Flights</span></span>}>
                  <Menu.Item key="flights-tabbed">Select flight</Menu.Item>
                </SubMenu>
                <SubMenu key="/contact" title={<span><span>Contact</span></span>}>
                  <Menu.Item key="contact">Contact info</Menu.Item>
                </SubMenu>
                <SubMenu key="/globaltix" title={<span><span>Globaltix</span></span>}>
                  <Menu.Item key="globaltix">Search</Menu.Item>
                  <Menu.Item key="globaltix-1"> Details</Menu.Item>
                  <Menu.Item key="globaltix-2">Search and Details</Menu.Item>
                </SubMenu>
                <SubMenu key="/payment" title={<span><span>Payment</span></span>}>
                  <Menu.Item key="payment">Payment</Menu.Item>
                  <Menu.Item key="checkout">Checkout</Menu.Item>
                  <Menu.Item key="price-rules">Price Rules</Menu.Item>
                </SubMenu>
              </Menu>
            </Drawer>
          </div>
        </Layout>
      </Container>
    )
  }
}

