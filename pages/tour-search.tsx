import React from 'react'
import { Row, Col } from 'antd'
import { ToursResult } from '../components/tours-result'
import { TravelCloudClient } from '../travelcloud'
import { ToursParamsForm } from '../components/tours-params-form'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'

export default class extends React.Component<any, any> {
  static async getInitialProps (context) {
    const client = new TravelCloudClient(config.tcUser)
    const tours = await client.tours(context.query)
    const query = context.query
    const categories = await client.categories({category_type: 'tour_package'})
    return { tours, query, categories }
  }

  state = {
    loading: false,
    'categories.name': this.props.query['categories.name'],
    '~boolean': this.props.query['~boolean'],
  }

  componentDidUpdate(prevProps) {
    if (prevProps.tours !== this.props.tours) {
      this.setState({
        loading: false
      })
    }
  }

  paramsChange(value) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: value
    })

    // update state with filter values and loading true
    this.setState({loading: true})
  }

  onTourClick(tour) {
    Router.push({
      pathname: '/tour',
      query: {id: tour.id}
    })
    this.setState({
      loading: true
    })
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Tours</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
            <h1>Tour filters</h1>
            <ToursParamsForm
              categories={this.props.categories}
              onChange={(value) => this.paramsChange(value)}
              value={this.props.query} />
          </Col>
          <Col span={18}>
            {this.state.loading || this.props.tours.result == null || this.props.tours.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={this.props.tours}
                  loading={this.state.loading}
                  type="tours" />
              : <ToursResult
                  cart={this.props.cart}
                  toursResult={this.props.tours.result}
                  onTourClick={(tour) => this.onTourClick(tour)} />}
          </Col>
        </Row>
      </div>
    )
  }
}