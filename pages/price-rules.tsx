import { Cart, TcResponse } from '../travelcloud'
import { Order } from '../components/order'
import { Layout, Row, Col, Spin, Card, Button, Icon } from 'antd'
import React from 'react'
import { withRouter, SingletonRouter } from 'next/router'
import { Customer, PriceRules } from '../types';

class Page extends React.PureComponent<{router: SingletonRouter, order:any, cart: Cart, customer: TcResponse<Customer>, priceRules: TcResponse<PriceRules>}, any> {
  render() {
    const cart = this.props.cart
    const order = this.props.order.result

    const customerPriceRules
      = this.props.customer.result == null
        ? null
        : this.props.customer.result.categories.reduce((acc, category) => acc.concat(category.customer_in_any_price_rules), [])


    if (this.props.order.loading) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Card loading={true} style={{border: 0}}></Card>
      </div>
    }

    if (order == null || order.products == null || order.products.length === 0) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <h1>Your cart is empty</h1>
      </div>
    }

    return (
      <Layout style={{backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Row>
          <Col span={12}>
            <h1>Public price rules</h1>
            {this.props.priceRules.loading
              ? <div>Loading...</div>
              : (this.props.priceRules.result == null || this.props.priceRules.result.length === 0
                ? <div>No public price rules</div>
                : this.props.priceRules.result.map((priceRule) =>
                  <div key={String(priceRule.id)}>
                    <h3>{priceRule.name}</h3>
                    <div dangerouslySetInnerHTML={{__html: priceRule.description}} />
                  </div>))
            }
            <h1>Customer price rules</h1>
            {this.props.customer.result == null
              ? <div>Customer not logged in</div>
              : (customerPriceRules.length === 0
                ? <div>No customer price rules</div>
                : customerPriceRules.map((priceRule) =>
                  <div key={String(priceRule.id)}>
                    <h3>{priceRule.name}</h3>
                    <div dangerouslySetInnerHTML={{__html: priceRule.description}} />
                  </div>))
            }

            {/* This is for testing purpose. You do not need to manually refresh price rules in normal usage. */}
            <Button onClick={() => {
              window['travelCloudClientCache']=null
              this.props.cart['refreshPriceRules']()
              this.props.cart.refreshCustomer()
              }}>
              Refresh price rules
            </Button>

          </Col>
          <Col span={12}>
            <h1>Order Details</h1>
            <Order order={order} showSection={{products: true}} cart={cart} />
          </Col>
        </Row>
      </Layout>
  )}
}

export default withRouter(Page)