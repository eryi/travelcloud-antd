import React from 'react'
import { Button } from 'antd'
import { TravelCloudClient } from '../travelcloud'
import { ContactForm } from '../components/contact'
import config from '../customize/config'

export default class extends React.PureComponent<any> {
  formRef
  state = {
    formState: ''
  }

  private client = new TravelCloudClient(config.tcUser);

  onSubmit = () => {
    this.formRef.props.form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        this.setState({
          formState: 'submitting'
        })

        const result = await this.client.submitContactForm({
          referral_site: 'www.tripmyfeet.com',
          subject: 'Contact form',
          customer_name: values.name,
          customer_email: values.email
        }, values)

        if (result.result == null) {
          this.setState({
            formState: 'error'
          })
        } else {
          this.setState({
            formState: 'submitted'
          })
        }
      } else {
        //console.log('error', error, values);
      }
    })
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <h1>Contact Us</h1>

        {this.state.formState === 'submitted'
        ? <div>We have received your enquiry and will get back to you as soon as possible</div>
        : <><ContactForm
            wrappedComponentRef={(formRef) => {
              this.formRef = formRef;
            }} />

          <Button
            type="primary"
            disabled={this.state.formState !== '' && this.state.formState !== 'error'}
            onClick={this.onSubmit}>
            Send message
          </Button>
        </>}

        {this.state.formState === 'error' && <div>There was a problem submitting the form. Please try again later.</div>}
      </div>
    )
  }
}