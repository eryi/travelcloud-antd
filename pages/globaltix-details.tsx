import React from 'react'
import Router from 'next/router'

import config from '../customize/config'

import { TravelCloudClient, Cart } from '../travelcloud'
import { Collapse, Icon, Row, Col, Button } from "antd";
import { IncDecInput } from "../components/inc-dec-input";
import TicketModal from '../components/ticket-detail';

export default class extends React.PureComponent<{ cart: Cart, openCart: () => void }> {
    private client = new TravelCloudClient(config.tcUser);

    /**
     * State properties of serach page
     */
    state = {
        resultId: 0,
        ticketTypeList: [],
        qtyMap: {},
        countryId: 0,
        item: {},
        title: '',
        ticketDetail: [],
        show: false,
        ticketModal: false,

        attractionImage: '',
        description: '',
        hoursOfOperation: '',
        imagePath: ''

    }

    /**
     * Called after the component is mounted to retrive data
     */
    async componentDidMount() {

        const resultId = Router.router.query.result
        const attractionGet = await this.client.globaltix('attraction/get', { id: resultId })
        const ticketTypeList = await this.client.globaltix('ticketType/list', { attraction_id: resultId })
        let item = attractionGet.result.data;
        item.ticketTypes = ticketTypeList.result.data;

        this.setState({
            ticketTypeList: ticketTypeList.result.data,
            countryId: attractionGet.result.data.country.id,
            resultId: Router.router.query.result,
            item: item,
            title: attractionGet.result.data.title,
            attractionImage: attractionGet.result.data.imagePath,
            description: attractionGet.result.data.description,
            hoursOfOperation: attractionGet.result.data.hoursOfOperation,
            imagePath: attractionGet.result.data.imagePath,
        });


    }

    onQtyChange = ((ticketId, newQty) => {
        const qtyMap = { ...this.state.qtyMap }
        qtyMap[ticketId] = newQty;;
        this.setState({ qtyMap });
    })

    onAddToCart = ((item, ticket) => {
        this.props.cart.addGlobaltix(item, {
            id: ticket.id,
            fromResellerId: ticket.from,
            quantity: this.state.qtyMap[ticket.id]
        })
        this.props.openCart()
    })

    /**
    * Retrive ticket id using search result props
    * @param details
    */
    showTicketDetail(details) {
        this.getTicketDetail(details.id);
    }

    /**
   * Retrieve ticket model details
   * @param id ticket id
   */
    async getTicketDetail(id) {
        const ticketTypeGet = await this.client.globaltix('ticketType/get', { id: id })
        var details = ticketTypeGet.result.data;
        this.setState({ ticketDetail: details, show: false, ticketModal: true });
    }

    render() {
        return (
            <div className="globalTix-container">
                <div className="tickets-details">
                    <h2>{this.state.title}</h2>
                </div>
                <div className="globalTix-container">
                    <p><img alt="No Image" width="100%" src={"https://uat-api.globaltix.com/api/image?name=" + this.state.imagePath} /></p>
                    <h4>{this.state.title}</h4>
                    <p>{this.state.description}</p>
                    <p><hr /></p>
                    <p>Operating Hours</p>
                    <p>{this.state.hoursOfOperation}</p>
                </div>
                {this.state.ticketTypeList.map(ticket => (
                    <div className="tickets-details" key={"ticket-details" + ticket.id}>
                        <Row type="flex" justify="space-between" align="middle" gutter={10} key={"ticket-row" + ticket.id}>
                            <Col lg={1} md={24} sm={24} xs={24} key={"ticket-col" + ticket.id}>
                                <Icon type="star" className="list-star1" key={"ticket-star" + ticket.id} />
                            </Col>
                            <Col lg={10} md={24} sm={24} xs={24} className="ticket-name" key={"ticket-name" + ticket.id}>
                                <p key={"catgory-name" + ticket.id}><span className="catgory-name" key={"catgory-name-span" + ticket.id}>{ticket.variation.name}</span><strong>{ticket.name}</strong><span className="ticketname-des" key={"ticketname-des" + ticket.id}>{'Usual Price ' + ticket.currency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)}</span><span className="ticket-merchant" key={"ticket-merchant" + ticket.id}>{'Merchant ' + ticket.sourceName}</span></p>
                            </Col>
                            <Col lg={8} md={24} sm={24} xs={24} className="ticket-price" key={"ticket-price" + ticket.id}>
                                <p className="rghtAlign-marginRght" key={"rghtAlign-marginRght" + ticket.id}>
                                    <span key={"ticket-currency" + ticket.id}><strong>{ticket.currency + ' '}</strong>{parseFloat(ticket.originalPrice).toFixed(2)}</span>
                                    <span className="price-des" key={"price-des" + ticket.id}>Before conversion rate {ticket.currency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)}</span>
                                </p>
                            </Col>
                            <Col lg={3} md={24} sm={24} xs={24}>
                                <IncDecInput min={0} max={9} value={this.state.qtyMap[ticket.id] || 0} onChange={(newVal) => this.onQtyChange(ticket.id, newVal)} />
                            </Col>
                            <Col lg={2} md={24} sm={24} xs={24} className="addto-cart" key={"addto-cart" + ticket.id}>
                                <Button disabled={this.state.qtyMap[ticket.id] === 0 || this.state.qtyMap[ticket.id] == null} type="primary" onClick={() => this.onAddToCart(this.state.item, ticket)}>Add to cart</Button>
                            </Col>
                        </Row>
                        <div className="ticketInfo-sec" key={"ticketInfo-sec" + ticket.id}>
                            <Icon key={"ticketInfo-icon" + ticket.id}
                                type="info-circle"
                                className="ticketInfo"
                                onClick={() => this.showTicketDetail(ticket)}
                            />
                        </div>
                    </div>
                ))}
                <TicketModal data={this.state.ticketDetail} handleClose={() => this.setState({ ticketModal: false })} show={this.state.ticketModal} />
            </div>
        );
    }

}
