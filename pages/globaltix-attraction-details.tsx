import React from 'react'
import Router from 'next/router'

import config from '../customize/config'
import { TravelCloudClient } from '../travelcloud'

export default class extends React.PureComponent {
    private client = new TravelCloudClient(config.tcUser);

    /**
    * State properties of attarction details page
    */
    state = {
        attractionImage: '',
        title: '',
        description: '',
        hoursOfOperation: '',
        imagePath: ''
    }

    /**
     * Called after the component is mounted to retrive data
     */
    async componentDidMount() {
        const resultId = Router.router.query.attractionId
        const attractionGet = await this.client.globaltix('attraction/get', { id: resultId })

        this.setState({
            attractionImage: attractionGet.result.data.imagePath,
            title: attractionGet.result.data.title,
            description: attractionGet.result.data.description,
            hoursOfOperation: attractionGet.result.data.hoursOfOperation,
            imagePath: attractionGet.result.data.imagePath
        });
    }

    render() {
        return (
            <div className="globalTix-container">
                <p><img alt="No Image" width="100%" src={"https://uat-api.globaltix.com/api/image?name=" + this.state.imagePath} /></p>
                <h4>{this.state.title}</h4>
                <p>{this.state.description}</p>
                <p><hr /></p>
                <p>Operating Hours</p>
                <p>{this.state.hoursOfOperation}</p>
            </div>
        );
    }
}

