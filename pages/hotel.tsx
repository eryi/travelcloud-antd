import React, { Fragment } from 'react'
import { Row, Col, Icon, Card, Button, Tabs, Popover, Carousel } from 'antd'
import { TravelCloudClient, formatCurrency, Cart, validateLegacyHotelParams } from '../travelcloud'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'
import CarouselArrow from '../components/carousel-arrow'
import imageExists from 'image-exists'
import { Element , Events, scroller } from 'react-scroll'

export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config.tcUser)

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    activeKey: 'overview',
    hotel: {} as any,
    loading: true,
    hotelId: this.props.query.hotelId,
    checkInDate: this.props.query.checkInDate,
    checkOutDate: this.props.query.checkOutDate,
    adults: this.props.query.adults || 2,
    children: this.props.query.children || 0,
    address: '' as string,

    // selected hotel state properties
    selectedHotel: {
      hotels: {} as any,
      loading: false,
      cityCode: this.props.query.cityCode,
      checkInDate: this.props.query.checkInDate,
      checkOutDate: this.props.query.checkOutDate,
      adults: this.props.query.adults || 2,
      children: 0
    }
  }

  async componentDidMount() {
    // update state with loading true
    this.setState({loading: true})

    // search hotel with default state
    //const hotelRes = await fetch('/static/hotel.json')
    //const hotel = await hotelRes.json()
    const hotel = await this.client.legacyHotel(this.state)

    // update state with hotel
    this.setState({loading: false, hotel})
  }

  async paramsChange(value) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: value
    })

    // update state with filter values and loading true
    this.setState({loading: true, ...value})

    // search hotel with filter values
    const hotel = await this.client.legacyHotel(value)

    // update state with hotel and loading false
    this.setState({loading: false, hotel: true})
  }

  truncateText(txt, max) {
    const truncated = 
      <div>
        <span>{txt.substr(0, max)}</span>
        <span className='truncated'>{txt.substr(max)}</span>
        <span>... more</span>
      </div>
    return truncated
  }
  handleChange = (key) => {
    this.setState({ activeKey: key });
  }
  scrollToTab() {
    scroller.scrollTo('price-list', {
      duration: 800,
      delay: 0,
      smooth: 'easeInOutQuart',
      offset: -10
    })
  }
  checkExist(exists) {
    if (exists) {
      return true
    }
    else {
      return false
    }
  }
  render() {
    const { windowWidth } = this.props
    const photoSlides = {
      arrows: true,
			prevArrow: <CarouselArrow classname='slick-arrow slick-prev' theme='filled'  iconType='left-circle' />,
			nextArrow: <CarouselArrow classname='slick-arrow slick-next' theme='filled' iconType='right-circle' />,
      dots: false,
      autoplay: false,
      centerMode: true,
      variableWidth: true
    }
    const hotel = this.state.hotel.result
    
    const params = validateLegacyHotelParams(this.props.query)
    if (params == null) return <div>
      Invalid hotel params
    </div>

    return (
      <div className="globalTix-container">
        <Head><title>{config.defaultTitle} | Hotel</title></Head>
        {this.state.loading || this.state.hotel.result == null
        ? <div className='wrap pad-y'>
            <NoResult response={this.state.hotel} loading={this.state.loading} type='hotel'>
              Cannot load selected hotel. Please&nbsp; 
              <Link href={{ pathname:'/hotel', query:{
                cityCode: this.props.query.cityCode,
                checkInDate: this.props.query.checkInDate,
                checkOutDate: this.props.query.checkOutDate,
                adults: this.props.query.adults || 2,
                children: this.props.query.children || 0
              }}}>
                <a>select a different hotel</a>
              </Link>
              &nbsp;or different dates.
            </NoResult>
          </div>
        : <div>
            <Carousel className='hotel-photos' {...photoSlides}>
              {hotel.images.map((photo,key) => 
                imageExists(photo.image,this.checkExist)===true &&
                <div key={key}>
                  <div style={{position:'relative'}}>
                    <img src={photo.image} height={300} />
                    {photo.text.length > 0 && <div style={{position:'absolute',bottom:0,left:0,right:0,color:'#fff',background:'rgba(0,0,0,.5)',padding:10,textAlign:'center'}}>{photo.text}</div>}
                  </div>
                </div>
              )}
            </Carousel>
            <div className='wrap pad-y'>
              <h2 style={{marginBottom:40}}><strong>{hotel.name}</strong></h2>
              <Element name='price-list'>
                <Tabs activeKey={this.state.activeKey} onChange={this.handleChange}>
                  <Tabs.TabPane tab='Overview' key='overview'>            
                    <Row gutter={32}>
                      <Col sm={24} lg={15}> 
                        {hotel.description !== null ? <p style={{textAlign:'justify'}}>{hotel.description}</p> : ''}
                        {hotel.contact !== null && hotel.contact.address !== null
                        ? <div>
                            <strong>Address: </strong>
                            {hotel.contact.address.streetName !=='' ? <Fragment>{hotel.contact.address.streetName}</Fragment> : null}
                            {hotel.contact.address.postalCode !=='' ? <Fragment>,{hotel.contact.address.postalCode}</Fragment> : null}
                            {hotel.contact.address.city !=='' ? <Fragment>,{hotel.contact.address.city}</Fragment> : null}
                            {hotel.contact.address.countryCode !=='' ? <Fragment>, {hotel.contact.address.countryCode}</Fragment> : null}
                          </div>
                        : null
                        }
                        {hotel.contact !== null 
                        ? <div>
                            {hotel.contact.email !=='' ? <Fragment><br /><strong>Email:</strong> <a href={'tel:'+hotel.contact.email}>{hotel.contact.email}</a><br /></Fragment> : null}
                            {hotel.contact.phone !=='' ? <Fragment><strong>Phone:</strong> <a href={'tel:'+hotel.contact.phone}>{hotel.contact.phone}</a></Fragment> : null}
                            {hotel.contact.fax !=='' ? <Fragment>&nbsp;&nbsp;<strong>Fax:</strong> {hotel.contact.fax}</Fragment> : null}
                          </div>
                        : null
                        }
                      </Col>
                      <Col sm={24} lg={9}>
                        {hotel.latitude !== null ? <iframe width='100%' height='400' src={'https://maps.google.com/maps?q='+hotel.latitude+','+hotel.longitude +'&hl=es;z=14&output=embed'}></iframe>: ''}
                      </Col>
                    </Row>
                    {hotel.facilities !==null 
                    ? <div>
                        <Button style={{margin:'40px 0'}} type='primary' onClick={()=>{this.handleChange('rooms'); this.scrollToTab()}}>Book Now <Icon type='right' /></Button>
                        <Card className='info-box'>
                          <Row gutter={32}>
                            <Col sm={12} lg={12}>
                              <h3>Facilities / Services</h3>
                              <Col sm={24} lg={12}>
                                <ul>
                                  {hotel.facilities.sort(function(a, b){
                                    if(a.text < b.text) return -1
                                    if(a.text > b.text) return 1
                                    return 0
                                  }).slice(0, 9).map((facility, index) => facility.fee == 'N' ? <li key={'n'+index}>{facility.text}</li> : null)}
                                </ul>
                              </Col>
                              <Col sm={24} lg={12}>
                                <ul>
                                  {hotel.facilities.sort(function(a, b){
                                    if(a.text < b.text) return -1
                                    if(a.text > b.text) return 1
                                    return 0
                                  }).slice(9, 17).map((facility, index) => facility.fee == 'N' ? <li key={'n2'+index}>{facility.text}</li> : null)}
                              </ul>
                            </Col>
                          </Col>
                          <Col sm={24} lg={12}>
                            <h3>Chargeables</h3>
                              <Col sm={24} lg={12}>
                                <ul>
                                  {hotel.facilities.sort(function(a, b){
                                    if(a.text < b.text) return -1
                                    if(a.text > b.text) return 1
                                    return 0
                                  }).map((facility, index) => facility.fee == 'Y' ? <li key={'y'+index}> {facility.text}</li> : null)}
                                </ul>
                              </Col>
                              <Col sm={24} lg={12}>
                                <ul>
                                  {hotel.facilities.sort(function(a, b){
                                    if(a.text < b.text) return -1
                                    if(a.text > b.text) return 1
                                    return 0
                                  }).slice(9, 17).map((facility, index) => facility.fee == 'Y' ?<li key={'y2'+index}>{facility.text}</li>: null)}
                                </ul> 
                              </Col>
                            </Col>
                          </Row>
                        </Card>
                      </div>
                    : null
                    }
                  </Tabs.TabPane>
                  
                  <Tabs.TabPane tab='Rooms' key='rooms'>
                    <div className='table-responsive'>
                      <table className='info-table'>
                      {this.state.hotel.result.rooms.map((room, index) =>
                      room.prices.map((price, index2) =>
                        <tr key={index}>
                          {index2 === 0 && <td rowSpan={room.prices.length}>
                            {room.description}
                          </td>}
                          <td>
                            {price.meal}
                          </td>
                          <td>
                            {price.occupancy.adults} adults
                                {parseInt(price.occupancy.children) > 0
                              ? price.occupancy.children + 'children'
                              : ''}
                          </td>
                          <td>
                            ${price.price}
                          </td>
                          <td>
                            <Button onClick={() => {
                              this.props.cart.reset().addLegacyHotel(
                                this.state.hotel.result, {
                                  room_id: room.id,
                                  price_code: price.code,
                                  params
                                })
                              Router.push('/checkout')
                            }}>
                              Add to cart
                                </Button>
                          </td>
                        </tr>
                      )
                    )}

                      </table>
                    </div>
                  </Tabs.TabPane>
                </Tabs>
              </Element>
            </div>
          </div>
        }
      </div>
    )  
  }
}
