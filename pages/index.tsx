import React from 'react'
import { Row, Col, Button } from 'antd';
import { TravelCloudClient, validateLegacyHotelsParams, validateFlightsParams } from '../travelcloud'
import { LegacyHotelsParamsForm } from '../components/legacy-hotels-params-form'
import { FlightsParamsForm } from '../components/flights-params-form'
import config from '../customize/config'
import Router from 'next/router'

export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config.tcUser)

  static async getInitialProps(context) {
    const client = new TravelCloudClient(config.tcUser)
    const query = context.query
    const document = await client.document({ id: 1 })
    const tours = await client.tours({ 'categories.name/and': context.query.t || 'Tours' })
    return { query, document, tours }
  }

  state = {
    hotelsParams: {
      cityCode: undefined,
      checkInDate: undefined,
      checkOutDate: undefined,
      adults: 2,
      children: 0
    },
    hotelsParamsInvalid: true,

    flightsParams: {
      source: 'tp',
      'od1.origin_airport.code': undefined,
      'od1.origin_datetime': undefined,
      'od2.origin_airport.code': undefined,
      'od2.origin_datetime': undefined,
      cabin: 'Y',
      ptc_adt: 1,
      ptc_cnn: 0,
      ptc_inf: 0,
    },
    flightsParamsInvalid: true,
  }
  render() {
    return (
      <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 42 }}>
        <div dangerouslySetInnerHTML={{ __html: this.props.document.result.content }} />
        <h2>Hotel Search</h2>
        <LegacyHotelsParamsForm
          client={this.client}
          onChange={(value) => this.setState({ hotelsParams: value, hotelsParamsInvalid: validateLegacyHotelsParams(value) == null })}
          value={this.state.hotelsParams}
          defaultCityCodes={['SIN', 'BKK', 'KUL', 'HKG']}>
          <LegacyHotelsParamsForm.CityCodeSelect style={{ width: '100%' }} placeholder="Destination" />
          <div className="search-index">
            <Row>
              <Col span={12}>
                <LegacyHotelsParamsForm.CheckInDatePicker style={{ width: '98%' }} placeholder="Check-in" />
              </Col>
              <Col span={12}>
                <LegacyHotelsParamsForm.CheckOutDatePicker style={{ width: '98%' }} placeholder="Check-out" />
              </Col>
            </Row>
            <div className="search-index">
              <LegacyHotelsParamsForm.AdultOccupancySelect style={{ width: 200 }} />
            </div>
          </div>
        </LegacyHotelsParamsForm>
        <div className="search-index">
          <Button
            type="primary"
            disabled={this.state.hotelsParamsInvalid}
            onClick={() => Router.push({
              pathname: '/hotels',
              query: this.state.hotelsParams
            })}>
            Search Hotel
        </Button>
        </div>
        <div className="header-index">
          <h2>Flight Search</h2>
        </div>
        <FlightsParamsForm
          client={this.client}
          onChange={(value) => this.setState({ flightsParams: value, flightsParamsInvalid: validateFlightsParams(value) == null })}
          value={this.state.flightsParams}
          defaultIataCodes={['SIN', 'BKK', 'KUL', 'HKG']}>
          <Row>
            <Col span={12}>
              <FlightsParamsForm.Od1OriginCodeSelect style={{ width: '98%' }} placeholder="From" />
            </Col>
            <Col span={12}>
              <FlightsParamsForm.Od2OriginCodeSelect style={{ width: '98%' }} placeholder="To" />
            </Col>
          </Row>
          <div className="search-index">
            <Row>
              <Col span={12}>
                <FlightsParamsForm.Od1OriginDepartureDatePicker style={{ width: '98%' }} placeholder="Depart" />
              </Col>
              <Col span={12}>
                <FlightsParamsForm.Od2OriginDepartureDatePicker style={{ width: '98%' }} placeholder="Return" />
              </Col>
            </Row>
          </div>
          <div className="search-index">
            <Row>
              <Row>
                <Col span={8}>
                  <FlightsParamsForm.PtcAdtSelect style={{ width: '98%' }} />
                </Col>
                <Col span={8}>
                  <FlightsParamsForm.PtcCnnSelect style={{ width: '98%' }} />
                </Col>
                <Col span={8}>
                  <FlightsParamsForm.PtcInfSelect style={{ width: '97%' }} />
                </Col>
              </Row>
            </Row>
          </div>
        </FlightsParamsForm>
        <div className="search-index">
          <Button
            type="primary"
            disabled={this.state.flightsParamsInvalid}
            onClick={() => Router.push({
              pathname: '/flights',
              query: this.state.flightsParams
            })}>
            Search Flight
        </Button>
        </div>
      </div>
    )
  }
}
