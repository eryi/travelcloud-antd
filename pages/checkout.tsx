import { CheckoutForm, initialCheckoutForm } from '../components/checkout'
import { Cart, updateFormState } from '../travelcloud'
import { Order } from '../components/order'
import { Layout, Row, Col, Card, Button, Icon } from 'antd'
import React from 'react'
import { withRouter, SingletonRouter } from 'next/router'

class Page extends React.PureComponent<{router: SingletonRouter, order:any, cart: Cart}, any> {
  formRef

  state = {
    submittingOrder: false,
    orderLoading: true,
    error: null,
    errorRef: null,
    checkoutForm: null
  }

  componentDidMount() {
    // prefetch payment page
    this.props.router.prefetch('/payment')
  }

  onSubmit (e) {
    e.preventDefault();
    this.formRef.props.form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        this.setState({
          submittingOrder: true
        })

        const result = await this.props.cart.checkout(values)

        if (result.result == null) {
          this.setState({
            error: true
          })
        } else {
          if (result.result.order_status === 'Quotation') {
            this.props.router.push('/payment?ref=' + result.result.ref)
          } else {
            this.setState({
              error: true,
              errorRef: result.result.ref
          })}
        }
      } else {
        console.log('error', error, values);
      }
    });
  };

  static getDerivedStateFromProps(props, current_state) {
    if (current_state.orderLoading === true && props.order.result != null) {
      // scroll up after order loaded
      setTimeout(() => window.scrollTo(0, 0), 100)
      return {
        orderLoading: false,
        checkoutForm: initialCheckoutForm(props.order.result, true)
      }
    }
    return null
  }

  render() {
    const cart = this.props.cart
    const order = this.props.order.result

    if (this.state.error === true) {
      return  <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <h1>We were not able to process your booking online</h1>
          <p>Please contact our friendly travel consultants to perform your booking.</p>
          {this.state.errorRef != null && <p>Order reference: {this.state.errorRef}</p>}
        </div>
    }

    if (this.props.order.loading) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Card loading={true} style={{border: 0}}></Card>
      </div>
    }

    if (order == null || order.products == null || order.products.length === 0) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <h1>Your cart is empty</h1>
      </div>
    }

    return (
      <Layout style={{backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Row>
          <Col span={12}>
            <h1>Contact Details</h1>
            <CheckoutForm
              formState={this.state.checkoutForm}
              onChange={updateFormState(this, 'checkoutForm')}
              wrappedComponentRef={(inst) => this.formRef = inst} />
            <Button size="large" type="primary" disabled={this.state.submittingOrder} onClick={(e) => this.onSubmit(e)}>Proceed to payment <Icon type={this.state.submittingOrder ? 'loading' : 'right'} /></Button>
          </Col>
          <Col span={12}>
            <h1>Order Details</h1>
            <Order order={order} showSection={{products: true}} cart={cart} />
          </Col>
        </Row>
      </Layout>
  )}
}

export default withRouter(Page)