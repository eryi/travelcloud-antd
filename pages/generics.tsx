import React from 'react'
import { Row, Col, Menu } from 'antd'
import { TravelCloudClient, Cart } from '../travelcloud'
import { GenericsResult } from '../components/generics-result'
import { NoResult } from '../components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'

export default class extends React.Component<{order, categories, generics, query, openCart, cart: Cart}, any> {
  static async getInitialProps (context) {
    const client = new TravelCloudClient(config.tcUser)
    const categories = await client.categories({category_type: 'nontour_product'})
    const generics = await client.generics(Object.assign({'categories.name': 'Attractions'}, context.query))
    const query = context.query
    return { categories, generics, query }
  }

  state = {
    loading: false,
    'categories.name': this.props.query['categories.name'] || 'Attractions'
  }

  componentDidUpdate(prevProps) {
    if (prevProps.generics !== this.props.generics) {
      this.setState({
        loading: false
      })
    }
  }

  menuClick({item, key}) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: {'categories.name': key}
    })

    // update state with filter values and loading true
    this.setState({loading: true, "categories.name": key})
  }

  onAddToCart(generic, form) {
    for (var i in form) {
      this.props.cart.addGeneric(generic, form[i])
    }
    this.props.openCart()
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', padding: 42}}>
        <Head>
          <title>{config.defaultTitle} | Generics</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
          <h1>Tickets</h1>
          <Menu
            selectedKeys={[this.state['categories.name']]}
            onClick={(x) => this.menuClick(x)}
            mode="vertical">
            {this.props.categories.result != null
              &&this.props.categories.result.map(category =>
                <Menu.Item key={category.name}>
                  <span>{category.name}</span>
                </Menu.Item>)}
          </Menu>
          </Col>
          <Col span={18}>
            {this.state.loading || this.props.generics.result == null || this.props.generics.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={this.props.generics}
                  loading={this.state.loading}
                  type="generics" />
              : <GenericsResult order={this.props.order} genericsResult={this.props.generics.result} onAddToCart={(generic, form) => this.onAddToCart(generic, form)} />}
          </Col>
        </Row>
      </div>
    )
  }
}
