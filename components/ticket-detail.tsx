import React from 'react'

import { Modal, Button } from 'antd';

/* Use ANTD Modal */
const TicketModal = ({ data, handleClose, show }) => {

  return (
    <Modal
      title="Ticket Detail"
      visible={show}
      onCancel={handleClose}
      width={800}
      footer={null}
    >
      <h3 className="redHeading" >{data.name}</h3>
      <p>{data.description}</p>
      {data.variation ? <p><strong>Type: </strong> <br />{data.variation.name}</p> : ''}
      <p className="tickets-details-terms"><strong>Terms & Condition: </strong><br />{data.termsAndConditions}</p>
      <p><strong>Product Id: </strong><br />{data.id}</p>
      <p><strong>SKU Id: </strong><br />{data.SKU}</p>
      <p><strong>Merchant: </strong><br />{data.merchantProductCode}</p>
      <p><strong>validity: </strong><br />{data.issuanceLimit}</p>
      <p><strong>Original Price: </strong><br />{data.currency + ' ' + parseFloat(data.originalPrice).toFixed(2)}</p>
      <p><strong>Settlement Price: </strong><br />{data.currency + ' ' + parseFloat(data.settlementRate).toFixed(2)}</p>
    </Modal>
  );
};
export default TicketModal;
