"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var travelcloud_1 = require("../travelcloud");
function notEmpty(x) {
    return x != null && x !== "" && x !== '0000-00-00' && x !== '0000-00-00 00:00:00';
}
exports.Order = function (_a) {
    var order = _a.order, cart = _a.cart, _b = _a.showSection, showSection = _b === void 0 ? { status: true, contact: true, products: true, remove: true, message: true, travelers: true, payments: true } : _b;
    var products = order.products || [];
    // group by name
    // same products may have different names
    var productsGrouped = travelcloud_1.objectEntries(products.reduce(function (acc, product) {
        if (acc[product.product_name] == null) {
            // clone product because we are flattening items from all products into one
            acc[product.product_name] = Object.assign({}, product);
            acc[product.product_name].items = product.items.slice(0);
        }
        else {
            acc[product.product_name].items = acc[product.product_name].items.concat(product.items);
        }
        return acc;
    }, {}));
    // properties in the outermost dive may not be rendered when
    // nextjs replaces client rendered dom with server rendered dom
    // add additional div outside to workaround this
    return react_1.default.createElement("div", null,
        react_1.default.createElement("div", { className: "tc-invoice" },
            showSection.status && react_1.default.createElement("h1", null, order.order_status),
            showSection.status && order.ref != null && react_1.default.createElement("div", null,
                react_1.default.createElement("div", null,
                    "Reference: REF-",
                    order.ref),
                react_1.default.createElement("div", null,
                    "Order Date: ",
                    order.order_date)),
            showSection.contact && order.first_name != null && order.first_name != "" && react_1.default.createElement("div", null,
                react_1.default.createElement("div", { className: "block-header" }, "Bill To"),
                react_1.default.createElement("div", null,
                    order.first_name,
                    " ",
                    order.last_name),
                order.customer != null && react_1.default.createElement("div", null, order.customer.email),
                react_1.default.createElement("div", null, order.phone)),
            showSection.products && productsGrouped.map(function (kvp, index) {
                var product = kvp[1];
                var invoiceItemsGrouped = product.items.reduce(function (acc, line) {
                    if (acc[line.sub_header] == null)
                        acc[line.sub_header] = [];
                    acc[line.sub_header].push(line);
                    return acc;
                }, {});
                // not sure how to enable Object.entries in babel
                var groups = [];
                for (var i in invoiceItemsGrouped) {
                    groups.push([i, invoiceItemsGrouped[i]]);
                }
                return react_1.default.createElement("table", { key: index, className: "invoice_table_style" },
                    react_1.default.createElement("tbody", null,
                        react_1.default.createElement("tr", { className: "item_name" },
                            react_1.default.createElement("td", { colSpan: 3, className: "col1" }, product.product_name),
                            react_1.default.createElement("td", { colSpan: 1 }, showSection.remove && cart && react_1.default.createElement("a", { className: "remove", onClick: function () { return cart.removeProductByName(product.product_name); } }, "remove"))),
                        notEmpty(product.product_source_ref) && react_1.default.createElement("tr", null,
                            react_1.default.createElement("td", { colSpan: 4, className: "col1" },
                                react_1.default.createElement("div", { className: "subtitle" },
                                    "Booking Ref: ",
                                    product.product_source_ref))),
                        notEmpty(product.from_date) && react_1.default.createElement("tr", null,
                            react_1.default.createElement("td", { colSpan: 4, className: "col1" },
                                react_1.default.createElement("div", { className: "subtitle" },
                                    "Departure date: ",
                                    product.from_date))),
                        react_1.default.createElement("tr", { className: "header" },
                            react_1.default.createElement("td", { style: { width: '60%' }, className: "col1" }, "Items"),
                            react_1.default.createElement("td", { style: { width: '10%' } }, "Units"),
                            react_1.default.createElement("td", { style: { width: '15%' } }, "Unit Price"),
                            react_1.default.createElement("td", { style: { width: '15%' } }, "Sub Total"))),
                    groups.map(function (group, index) { return react_1.default.createElement("tbody", { key: index },
                        react_1.default.createElement("tr", { className: "subheader" },
                            react_1.default.createElement("td", { className: "col1" },
                                react_1.default.createElement("div", null, group[0]))),
                        group[1].map(function (item, index) {
                            return react_1.default.createElement("tr", { key: index },
                                react_1.default.createElement("td", { className: "col1" },
                                    react_1.default.createElement("div", null, item.name)),
                                react_1.default.createElement("td", null, item.quantity),
                                react_1.default.createElement("td", null, travelcloud_1.formatCurrency(item.price)),
                                react_1.default.createElement("td", null, travelcloud_1.formatCurrency(item.quantity * item.price)));
                        })); }));
            }),
            showSection.products && [react_1.default.createElement("div", { className: "total", key: 1 },
                    react_1.default.createElement("span", null, "Total"),
                    react_1.default.createElement("span", { className: "amount" }, travelcloud_1.formatCurrency(order.payment_required))),
                react_1.default.createElement("div", { className: "tc-clearfix", key: 2 })],
            showSection.message && order.user_message != null && order.user_message.trim() !== "" && react_1.default.createElement("table", { className: "invoice_table_style" },
                react_1.default.createElement("tbody", null,
                    react_1.default.createElement("tr", { className: "item_name" },
                        react_1.default.createElement("td", { className: "col1" }, "Additional Requests")),
                    react_1.default.createElement("tr", null,
                        react_1.default.createElement("td", { className: "col1" }, order.user_message)))),
            showSection.travelers && order.travelers != null && order.travelers.length > 0 && react_1.default.createElement("table", { className: "invoice_table_style" },
                react_1.default.createElement("tbody", null,
                    react_1.default.createElement("tr", { className: "item_name" },
                        react_1.default.createElement("td", { className: "col1", colSpan: 7 }, "Travelers")),
                    react_1.default.createElement("tr", { className: "header" },
                        react_1.default.createElement("td", { className: "col1" }, "Name"),
                        react_1.default.createElement("td", null, "Type"),
                        react_1.default.createElement("td", null, "Date of birth"),
                        react_1.default.createElement("td", null, "Gender"),
                        react_1.default.createElement("td", null, "Nationality"),
                        react_1.default.createElement("td", null, "Passport"),
                        react_1.default.createElement("td", null, "Expiry")),
                    order.travelers != null && order.travelers.length > 0 && order.travelers.map(function (traveler, index) { return react_1.default.createElement("tr", { key: index },
                        react_1.default.createElement("td", { className: "col1" },
                            traveler.first_name,
                            " ",
                            traveler.last_name),
                        react_1.default.createElement("td", null, traveler.type),
                        react_1.default.createElement("td", null, traveler.birth_date),
                        react_1.default.createElement("td", null, traveler.title === 'Ms' ? 'female' : 'male'),
                        react_1.default.createElement("td", null, traveler.country),
                        react_1.default.createElement("td", null, traveler.passport),
                        react_1.default.createElement("td", null, traveler.expiry)); }))),
            showSection.payments && react_1.default.createElement("table", { className: "invoice_table_style" },
                react_1.default.createElement("tbody", null,
                    react_1.default.createElement("tr", { className: "item_name" },
                        react_1.default.createElement("td", { className: "col1", colSpan: 4 }, "Payments")),
                    react_1.default.createElement("tr", { className: "header" },
                        react_1.default.createElement("td", { className: "col1" }, "Description"),
                        react_1.default.createElement("td", null, "Date"),
                        react_1.default.createElement("td", null, "Status"),
                        react_1.default.createElement("td", null, "Amount")),
                    order.payments != null && order.payments.length > 0
                        ? order.payments.map(function (payment, index) { return react_1.default.createElement("tr", { key: index },
                            react_1.default.createElement("td", { className: "col1" }, payment.description.length === 0 ? "Added by " + payment.source : payment.description),
                            react_1.default.createElement("td", null, payment.date_added),
                            react_1.default.createElement("td", null, payment.status),
                            react_1.default.createElement("td", null, travelcloud_1.formatCurrency(payment.amount))); })
                        : react_1.default.createElement("tr", null,
                            react_1.default.createElement("td", { className: "col1", colSpan: 4 }, "No payments received"))))));
};
//# sourceMappingURL=order.js.map