import React from "react";
import { Collapse, Icon, Row, Col, Button } from "antd";
import { IncDecInput } from "./inc-dec-input";

const Panel = Collapse.Panel;

export const SearchAndResult: React.StatelessComponent<{
  offset: number;
  limit: number;
  data: any;
  qtyMap: {[key: string]: number};
  onQtyChange: (ticketId, newQty) => any;
  onAttractionClick?: (attractionDetail: any) => any;
  onTicketClick?: (ticketDetail: any) => any;
  onAddToCart?: (attraction: any, ticket: any) => any;
}> = ({ offset, limit, data, qtyMap, onQtyChange, onAttractionClick, onTicketClick, onAddToCart }) => {
  return (
    <div key={"main-container"} className="search-result">
      {data == null || data.length === 0 ? (
        <h4>There is no record found!</h4>
      ) : (
          data.map((attraction, index) => {
            if (index >= offset && index <= limit) {
              return (
                <Collapse
                  className="main-collaspe-relative"
                  key={"collaspepanel" + index}
                  accordion
                >
                  <Panel
                    header={
                      <div className="header-list1" key={"header" + index}>
                        <Icon key={"starticon" + index} type="star" className="list-star" />
                        <img
                          width="10%"
                          src={
                            "https://uat-api.globaltix.com/api/image?name=" +
                            attraction.imagePath
                          }
                          alt="No Image"
                          key={"image" + index}
                        />
                        <span key={"title" + index}>{attraction.title}</span>
                        <span key={"iconspan" + index}>
                          <Icon
                            type="info-circle"
                            className="attractionInfo"
                            onClick={() => onAttractionClick(attraction)}
                          />
                        </span>
                      </div>
                    }
                    key={"upperpanel" + index}
                  >
                    {attraction.ticketTypes.map(ticket => (
                      <div className="tickets-details" key={"ticket-details" + ticket.id}>
                        <Row type="flex" justify="space-between" align="middle" gutter={10} key={"ticket-row" + ticket.id}>
                          <Col lg={1} md={24} sm={24} xs={24} key={"ticket-col" + ticket.id}>
                            <Icon type="star" className="list-star1" key={"ticket-star" + ticket.id} />
                          </Col>
                          <Col lg={10} md={24} sm={24} xs={24} className="ticket-name" key={"ticket-name" + ticket.id}>
                            <p key={"catgory-name" + ticket.id}><span className="catgory-name" key={"catgory-name-span" + ticket.id}>{ticket.variation.name}</span><strong>{ticket.name}</strong><span className="ticketname-des" key={"ticketname-des" + ticket.id}>{'Usual Price ' + ticket.currency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)}</span><span className="ticket-merchant" key={"ticket-merchant" + ticket.id}>{'Merchant ' + ticket.sourceName}</span></p>
                          </Col>
                          <Col lg={8} md={24} sm={24} xs={24} className="ticket-price" key={"ticket-price" + ticket.id}>
                            <p className="rghtAlign-marginRght" key={"rghtAlign-marginRght" + ticket.id}>
                              <span key={"ticket-currency" + ticket.id}><strong>{ticket.currency + ' '}</strong>{parseFloat(ticket.originalPrice).toFixed(2)}</span>
                              <span className="price-des" key={"price-des" + ticket.id}>Before conversion rate {ticket.currency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)}</span>
                            </p>
                          </Col>
                          <Col lg={3} md={24} sm={24} xs={24}>
                            <IncDecInput min={0} max={9} value={qtyMap[ticket.id] || 0} onChange={(newVal) => onQtyChange(ticket.id, newVal)} />
                          </Col>
                          <Col lg={2} md={24} sm={24} xs={24} className="addto-cart" key={"addto-cart" + ticket.id}>
                            <Button disabled={qtyMap[ticket.id] === 0 || qtyMap[ticket.id] == null} type="primary" onClick={() => onAddToCart(attraction, ticket)}>Add to cart</Button>
                          </Col>
                        </Row>
                        <div className="ticketInfo-sec" key={"ticketInfo-sec" + ticket.id}>
                          <Icon key={"ticketInfo-icon" + ticket.id}
                            type="info-circle"
                            className="ticketInfo"
                            onClick={() => onTicketClick(ticket)}
                          />
                        </div>
                      </div>
                    ))}
                  </Panel>
                </Collapse>
              );
            }
          })
        )}
    </div>
  );
};
