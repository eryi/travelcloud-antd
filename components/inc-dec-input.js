"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
exports.IncDecInput = function (props) {
    var getFieldDecorator = function (_) { return function (x) { return x; }; };
    var val;
    var valProp = {};
    if (props.form == null) {
        val = props.value;
        valProp.value = val;
    }
    else {
        val = props.form.getFieldValue(props.name);
        getFieldDecorator = props.form.getFieldDecorator;
    }
    var handleInc = function () {
        var _a;
        if (props.allowAdd != null && props.allowAdd === false)
            return;
        val++;
        if (props.max != null && val > props.max)
            val = props.max;
        if (props.form != null) {
            props.form.setFieldsValue((_a = {},
                _a[props.name] = val,
                _a));
        }
        if (props.onChange != null) {
            props.onChange(val);
        }
    };
    var handleDec = function () {
        var _a;
        if (props.allowSubtract != null && props.allowSubtract === false)
            return;
        val--;
        if (props.min != null && val < props.min)
            val = props.min;
        if (props.form != null) {
            props.form.setFieldsValue((_a = {},
                _a[props.name] = val,
                _a));
        }
        if (props.onChange != null) {
            props.onChange(val);
        }
    };
    var effectiveAllowAdd = props.allowAdd == null ? val < props.max : props.allowAdd && val < props.max;
    var effectiveAllowSubtract = props.allowSubtract == null ? val > props.min : props.allowSubtract && val > props.min;
    var className = "";
    if (effectiveAllowAdd)
        className = className + ' tc-right-addon-primary';
    if (effectiveAllowSubtract)
        className = className + ' tc-left-addon-primary';
    // not sure why TypeScript freaks out if we don't cast to any
    // getFieldDecorator probably has some incorrect types
    return react_1.default.createElement("div", { className: className }, getFieldDecorator(props.name)(react_1.default.createElement(antd_1.Input, __assign({}, valProp, { className: "tc-inc-dec-input", type: "number", addonAfter: react_1.default.createElement(antd_1.Icon, { type: "plus", onClick: handleInc }), addonBefore: react_1.default.createElement(antd_1.Icon, { type: "minus", onClick: handleDec }), min: props.min, max: props.max, 
        // prevent readonly warning
        onChange: function () { } }))));
};
//# sourceMappingURL=inc-dec-input.js.map