import React from 'react'
import {formatCurrency, Cart, objectEntries} from '../travelcloud'

function notEmpty(x) {
  return x != null && x !== "" && x !== '0000-00-00' && x !== '0000-00-00 00:00:00'
}

export const Order = (
  {order, cart, showSection = {status: true, contact: true, products: true, remove: true, message: true, travelers: true, payments: true}}
  : {order, cart?: Cart,
    showSection?: {status?: boolean, contact?: boolean, products?: boolean, remove?: boolean, message?: boolean, travelers?: boolean, payments?: boolean}}) => {
  const products = order.products || []

  // group by name
  // same products may have different names
  const productsGrouped = objectEntries(products.reduce((acc, product) => {
    if (acc[product.product_name] == null) {
      // clone product because we are flattening items from all products into one
      acc[product.product_name] = Object.assign({}, product)
      acc[product.product_name].items = product.items.slice(0)
    } else {
      acc[product.product_name].items = acc[product.product_name].items.concat(product.items)
    }
    return acc
  }, {}))

  // properties in the outermost dive may not be rendered when
  // nextjs replaces client rendered dom with server rendered dom
  // add additional div outside to workaround this
  return <div><div className="tc-invoice">
    {showSection.status && <h1>{order.order_status}</h1>}
    {showSection.status && order.ref != null && <div>
      <div>Reference: REF-{order.ref}</div>
      <div>Order Date: {order.order_date}</div>
    </div>}

    {showSection.contact && order.first_name != null && order.first_name != "" && <div>
      <div className="block-header">Bill To</div>
      <div>{order.first_name} {order.last_name}</div>
      {order.customer != null && <div>{order.customer.email}</div>}
      <div>{order.phone}</div>
    </div>}

    {showSection.products && productsGrouped.map((kvp, index) => {
      const product = kvp[1]
      const invoiceItemsGrouped = product.items.reduce((acc, line) => {
        if (acc[line.sub_header] == null) acc[line.sub_header] = []
        acc[line.sub_header].push(line)
        return acc
      }, {})

      // not sure how to enable Object.entries in babel
      const groups = []
      for (var i in invoiceItemsGrouped) {
        groups.push([i, invoiceItemsGrouped[i]])
      }

      return <table key={index} className="invoice_table_style"><tbody>
        <tr className="item_name">
          <td colSpan={3} className="col1">
            {product.product_name}
          </td>
          <td colSpan={1}>
          {showSection.remove && cart && <a className="remove" onClick={() => cart.removeProductByName(product.product_name)}>remove</a>}
          </td>
        </tr>
        {notEmpty(product.product_source_ref) && <tr>
          <td colSpan={4} className="col1">
            <div className="subtitle">Booking Ref: {product.product_source_ref}</div>
          </td>
        </tr>}
        {notEmpty(product.from_date) && <tr>
          <td colSpan={4} className="col1">
            <div className="subtitle">Departure date: {product.from_date}</div>
          </td>
        </tr>}
        <tr className="header">
          <td style={{width:'60%'}} className="col1">Items</td>
          <td style={{width:'10%'}}>Units</td>
          <td style={{width:'15%'}}>Unit Price</td>
          <td style={{width:'15%'}}>Sub Total</td>
        </tr></tbody>

        {groups.map((group, index) => <tbody key={index}>
          <tr className="subheader">
            <td className="col1">
              <div>
              {group[0]}
              </div></td>
          </tr>
          {group[1].map((item, index) =>
            <tr key={index}>
              <td className="col1">
                <div>
                  {item.name}
                </div></td>
              <td>{item.quantity}</td>
              <td>{formatCurrency(item.price)}</td>
              <td>{formatCurrency(item.quantity * item.price)}</td>
            </tr>)}
        </tbody>)}
      </table>
    })}
    {showSection.products && [<div className="total" key={1}>
      <span>Total</span>
      <span className="amount">{formatCurrency(order.payment_required)}</span>
    </div>,
    <div className="tc-clearfix" key={2} />]}

    {showSection.message && order.user_message != null && order.user_message.trim() !== "" && <table className="invoice_table_style"><tbody>
      <tr className="item_name">
        <td className="col1">Additional Requests</td>
      </tr>
      <tr>
        <td className="col1">{order.user_message}</td>
      </tr>
      </tbody></table>}

    {showSection.travelers && order.travelers != null && order.travelers.length > 0 && <table className="invoice_table_style"><tbody>
      <tr className="item_name">
        <td className="col1" colSpan={7}>Travelers</td>
      </tr>
      <tr className="header">
        <td className="col1">Name</td>
        <td>Type</td>
        <td>Date of birth</td>
        <td>Gender</td>
        <td>Nationality</td>
        <td>Passport</td>
        <td>Expiry</td>
      </tr>
      {order.travelers != null && order.travelers.length > 0 && order.travelers.map((traveler, index) => <tr key={index}>
        <td className="col1">{traveler.first_name} {traveler.last_name}</td>
        <td>{traveler.type}</td>
        <td>{traveler.birth_date}</td>
        <td>{traveler.title === 'Ms' ? 'female' : 'male'}</td>
        <td>{traveler.country}</td>
        <td>{traveler.passport}</td>
        <td>{traveler.expiry}</td>
      </tr>)}
      </tbody></table>}

    {showSection.payments && <table className="invoice_table_style"><tbody>
      <tr className="item_name">
        <td className="col1" colSpan={4}>Payments</td>
      </tr>
      <tr className="header">
        <td className="col1">Description</td>
        <td>Date</td>
        <td>Status</td>
        <td>Amount</td>
      </tr>
      {order.payments != null && order.payments.length > 0
        ?  order.payments.map((payment, index) => <tr key={index}>
            <td className="col1">{payment.description.length === 0 ? "Added by " + payment.source : payment.description}</td>
            <td>{payment.date_added}</td>
            <td>{payment.status}</td>
            <td>{formatCurrency(payment.amount)}</td>
          </tr>)
        : <tr>
            <td className="col1" colSpan={4}>No payments received</td>
          </tr>}
    </tbody></table>}
  </div></div>}