"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
exports.ContactForm = antd_1.Form.create()(/** @class */ (function (_super) {
    __extends(class_1, _super);
    function class_1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    class_1.prototype.render = function () {
        var form = this.props.form;
        var getFieldDecorator = form.getFieldDecorator;
        var formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        };
        return (react_1.default.createElement(antd_1.Form, { layout: "vertical" },
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "E-mail" }), getFieldDecorator('email', {
                rules: [{
                        type: 'email', message: 'E-mail is not valid',
                    }, {
                        required: true, message: 'Please enter your E-mail!',
                    }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Phone" }), getFieldDecorator('phone', {
                rules: [{ required: true, message: 'Please enter your phone number!' }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Name" }), getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please enter your name!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Message" }), getFieldDecorator('message', {
                rules: [{ required: true, message: 'Please enter your message!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input.TextArea, { autosize: { minRows: 2, maxRows: 6 } })))));
    };
    return class_1;
}(react_1.default.Component)));
//# sourceMappingURL=contact.js.map