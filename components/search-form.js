"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
// import queryString from "query-string";
var Option = antd_1.Select.Option;
var Search = antd_1.Input.Search;
exports.SearchForm = function (_a) {
    var countryList = _a.countryList, cityList = _a.cityList, searchFormKeys = _a.searchFormKeys, onSearch = _a.onSearch;
    return (react_1.default.createElement("div", { className: "search-deals", key: "Searchdiv" },
        react_1.default.createElement(antd_1.Form, { onSubmit: function (e) { return e.preventDefault(); }, key: "SearchForm" },
            react_1.default.createElement(antd_1.Row, { gutter: 16, key: "row1" },
                react_1.default.createElement(antd_1.Col, { md: 24, sm: 24, key: "col1" },
                    react_1.default.createElement(antd_1.Form.Item, { key: "formItem1" },
                        react_1.default.createElement(Search, { defaultValue: searchFormKeys.keyword, placeholder: "Keyword", onSearch: function (value) { return onSearch(value, 'keyword'); } })))),
            react_1.default.createElement(antd_1.Row, { gutter: 16, key: "row2" },
                react_1.default.createElement(antd_1.Col, { lg: 8, md: 12, sm: 12, xs: 24, key: "col2" },
                    react_1.default.createElement(antd_1.Form.Item, { key: "formItem2" },
                        react_1.default.createElement(antd_1.Select, { key: "country", placeholder: "Country", defaultValue: searchFormKeys.countryId, onChange: function (e) { return onSearch(e, "countryId"); } },
                            react_1.default.createElement(Option, { value: "", key: "emptycountry" }, "Select Country"),
                            countryList.map(function (item, index) { return (react_1.default.createElement(Option, { value: "" + item.id, key: "country" + item.id }, item.name)); })))),
                react_1.default.createElement(antd_1.Col, { lg: 8, md: 12, sm: 12, xs: 24, key: "col3" },
                    react_1.default.createElement(antd_1.Form.Item, { key: "formItem3" },
                        react_1.default.createElement(antd_1.Select, { key: "city", placeholder: "City", defaultValue: searchFormKeys.cityId, onChange: function (e) { return onSearch(e, "cityId"); } },
                            react_1.default.createElement(Option, { value: "", key: "emptycity" }, "Select City"),
                            cityList.map(function (item, index) {
                                if (parseInt(item.countryId) == parseInt(searchFormKeys.countryId)) {
                                    return react_1.default.createElement(Option, { value: "" + item.id, key: "city" + item.id }, item.name);
                                }
                            })))),
                react_1.default.createElement(antd_1.Col, { lg: 8, md: 12, sm: 12, xs: 24, key: "col4" },
                    react_1.default.createElement(antd_1.Form.Item, { key: "formItem4" },
                        react_1.default.createElement(antd_1.Select, { key: "category", placeholder: "Category", defaultValue: "" + searchFormKeys.tab, onChange: function (e) { return onSearch(e, "tab"); } },
                            react_1.default.createElement(Option, { value: "ALL", key: "emptytab" }, "ALL"),
                            react_1.default.createElement(Option, { value: "Attractions", key: "Attractionstab" }, "Attractions"),
                            react_1.default.createElement(Option, { value: "Tours", key: "Tourstab" }, "Tours"),
                            react_1.default.createElement(Option, { value: "Lifestyle", key: "Lifestyletab" }, "Lifestyle"))))))));
};
//# sourceMappingURL=search-form.js.map