import React from 'react'
import { Spin } from 'antd'

export const NoResult: React.StatelessComponent<{children?: any, type: any, className?: string, response: any, loading?: boolean, style?: React.CSSProperties, loadingMessage?: string}> = ({response, style, className, loading, type, children, loadingMessage}) => {
  if (response.error != null) {
    return <div style={{textAlign: 'center', ...style}} className={className}>
    An error has occured. Please try again later.
  </div>
  }

  if (response.loading === true || loading === true) {
    if (loadingMessage == null) {
      loadingMessage = 'Searching...'
    }

    return <div style={{textAlign: 'center', ...style}} className={className}>
    <Spin size="large" style={{margin: 'auto'}} />
    <h1 style={{width: '100%', marginTop: 32}}>{loadingMessage}</h1>
  </div>
  }

  if (response.result == null) {
    return <div style={{textAlign: 'center', ...style}} className={className}>{children}</div>
  }

  if (response.result.length === 0) {
    return <div style={{textAlign: 'center', ...style}} className={className}>
    No {type} matching your search criteria was found.
  </div>
  }

  return null}