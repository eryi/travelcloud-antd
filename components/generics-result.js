"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var travelcloud_1 = require("../travelcloud");
var inc_dec_input_1 = require("./inc-dec-input");
var big_js_1 = __importDefault(require("big.js"));
var GenericsResult = /** @class */ (function (_super) {
    __extends(GenericsResult, _super);
    function GenericsResult() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            order: null,
            orderState: {}
        };
        return _this;
    }
    GenericsResult.getDerivedStateFromProps = function (props, currentState) {
        if (currentState != null && props.order.result != null && props.order !== currentState.order) {
            var products = props.order.result.products || [];
            var orderState = products.reduce(function (acc, product) {
                if (product.product_type !== 'nontour_product')
                    return acc;
                acc[product.product_id] = product.quantity;
                return acc;
            }, {});
            return __assign({ order: props.order, orderState: orderState }, orderState);
        }
        return null;
    };
    GenericsResult.prototype.render = function () {
        var _this = this;
        var _a = this.props, genericsResult = _a.genericsResult, onAddToCart = _a.onAddToCart, style = _a.style, className = _a.className;
        var Total = function (_a) {
            var item = _a.item;
            var total = item.options.reduce(function (acc, option) {
                var key = item.id + '/' + option.id;
                var qty = _this.state[key] || 0;
                var orderQty = _this.state.orderState[key] || 0;
                var subTotal = big_js_1.default(option.price).times(qty);
                acc.onAddToCartPayload.push({
                    generic_id: item.id,
                    option_id: option.id,
                    quantity: qty
                });
                return {
                    totalPrice: acc.totalPrice.add(subTotal),
                    totalQty: acc.totalQty + qty,
                    onAddToCartPayload: acc.onAddToCartPayload,
                    notSameAsOrder: acc.notSameAsOrder || qty !== orderQty,
                    orderIsEmpty: acc.orderIsEmpty && orderQty === 0
                };
            }, {
                totalPrice: big_js_1.default(0),
                totalQty: 0,
                onAddToCartPayload: [],
                notSameAsOrder: false,
                orderIsEmpty: true
            });
            if (total.notSameAsOrder === false)
                return null;
            return react_1.default.createElement("tr", { style: { borderTop: '2px solid #999' } },
                react_1.default.createElement("td", null,
                    "Total (",
                    total.totalQty,
                    " items)"),
                react_1.default.createElement("td", { style: { fontWeight: 'bold' } }, travelcloud_1.formatCurrency(total.totalPrice.toFixed(2))),
                react_1.default.createElement("td", null,
                    react_1.default.createElement(antd_1.Button, { style: { width: '100%', margin: "8px 0" }, type: "primary", onClick: function () {
                            onAddToCart && onAddToCart(item, total.onAddToCartPayload);
                        } }, total.orderIsEmpty
                        ? "Add to cart"
                        : "Update cart")));
        };
        return react_1.default.createElement("div", { style: style, className: className },
            react_1.default.createElement(antd_1.List, { dataSource: genericsResult, renderItem: function (item) { return (react_1.default.createElement(antd_1.List.Item, null,
                    react_1.default.createElement("div", { style: { width: '100%' } },
                        react_1.default.createElement("div", { style: {
                                backgroundImage: "url(" + item.photo_url + ")",
                                height: 400,
                                width: "40%",
                                backgroundSize: 'cover',
                                backgroundPosition: 'center',
                                float: 'left'
                            } }),
                        react_1.default.createElement("div", { style: { padding: '16px 32px', float: 'left', width: "60%" } },
                            react_1.default.createElement("h2", { className: "tc-full-width-ellipsis" }, item.name),
                            react_1.default.createElement(antd_1.Tabs, { defaultActiveKey: "1" },
                                react_1.default.createElement(antd_1.Tabs.TabPane, { tab: "Prices", key: "1" },
                                    react_1.default.createElement("table", null,
                                        react_1.default.createElement("tbody", null,
                                            item.options.map(function (option) {
                                                var key = item.id + '/' + option.id;
                                                var val = _this.state[key] || 0;
                                                return react_1.default.createElement("tr", { key: option.id },
                                                    react_1.default.createElement("td", { style: { paddingRight: 32, minWidth: 150 } },
                                                        react_1.default.createElement("div", { className: "tc-full-width-ellipsis" }, option.name)),
                                                    react_1.default.createElement("td", { style: { paddingRight: 32, fontWeight: 'bold' } }, travelcloud_1.formatCurrency(option.price)),
                                                    react_1.default.createElement("td", { style: { width: 150 } },
                                                        react_1.default.createElement(inc_dec_input_1.IncDecInput, { min: 0, max: option.stock, value: val, onChange: function (newVal) {
                                                                var _a;
                                                                return _this.setState((_a = {}, _a[key] = newVal, _a));
                                                            } })));
                                            }),
                                            react_1.default.createElement("tr", { style: { height: 16 } },
                                                react_1.default.createElement("td", { colSpan: 3 })),
                                            react_1.default.createElement(Total, { item: item })))),
                                react_1.default.createElement(antd_1.Tabs.TabPane, { tab: "Description", key: "2" },
                                    react_1.default.createElement("div", { style: { height: 260, overflowY: 'auto' }, dangerouslySetInnerHTML: { __html: item.description } }))))))); } }));
    };
    return GenericsResult;
}(react_1.default.PureComponent));
exports.GenericsResult = GenericsResult;
//# sourceMappingURL=generics-result.js.map