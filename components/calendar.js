"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
function Calendar(_a) {
    var validRange = _a.validRange, _b = _a.locale, locale = _b === void 0 ? 'en-US' : _b, style = _a.style, className = _a.className, CalendarDayGenerator = _a.CalendarDayGenerator;
    var startDate = new Date(validRange[0].getTime());
    var months = [];
    while (startDate.getFullYear() * 12 + startDate.getMonth() <= validRange[1].getFullYear() * 12 + validRange[1].getMonth()) {
        months.push(CalendarMonth({ date: startDate, locale: locale, CalendarDayGenerator: CalendarDayGenerator }));
        startDate.setMonth(startDate.getMonth() + 1);
    }
    return react_1.default.createElement("div", { className: "ant-fullcalendar ant-fullcalendar-full ant-fullcalendar-fullscreen " + (className || ''), style: __assign({ position: 'relative' }, style) },
        react_1.default.createElement("div", { className: "ant-fullcalendar-calendar-body", style: { paddingTop: 23, height: '100%' } },
            react_1.default.createElement("div", { style: { paddingRight: 12, position: 'absolute', zIndex: 1, top: 0 } },
                react_1.default.createElement("table", { className: "ant-fullcalendar-table" },
                    react_1.default.createElement("thead", null,
                        react_1.default.createElement("tr", { role: "row" },
                            react_1.default.createElement("th", { role: "columnheader", title: "Sun", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Sun")),
                            react_1.default.createElement("th", { role: "columnheader", title: "Mon", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Mon")),
                            react_1.default.createElement("th", { role: "columnheader", title: "Tue", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Tue")),
                            react_1.default.createElement("th", { role: "columnheader", title: "Wed", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Wed")),
                            react_1.default.createElement("th", { role: "columnheader", title: "Thu", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Thu")),
                            react_1.default.createElement("th", { role: "columnheader", title: "Fri", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Fri")),
                            react_1.default.createElement("th", { role: "columnheader", title: "Sat", className: "ant-fullcalendar-column-header" },
                                react_1.default.createElement("span", { className: "ant-fullcalendar-column-header-inner" }, "Sat")))))),
            react_1.default.createElement("div", { style: { flex: 1, overflowY: 'scroll', height: 'inherit' } },
                react_1.default.createElement("table", { className: "ant-fullcalendar-table", role: "grid" }, months))));
}
exports.default = Calendar;
function CalendarMonth(_a) {
    var date = _a.date, _b = _a.locale, locale = _b === void 0 ? 'en-US' : _b, CalendarDayGenerator = _a.CalendarDayGenerator;
    var thisMonth = date.getMonth();
    var startDate = new Date(date.getTime());
    startDate.setDate(1); // get the 1st of month
    startDate.setDate(1 - startDate.getDay()); // get the start of week
    var endDate = new Date(date.getTime());
    endDate.setMonth(endDate.getMonth() + 1); // get next month
    endDate.setDate(0); // get last day of previous month of next month
    endDate.setDate(endDate.getDate() + 6 - endDate.getDay()); // get end of week
    var allDates = [];
    for (; startDate <= endDate; startDate.setDate(startDate.getDate() + 1)) {
        allDates.push(new Date(startDate.getTime()));
    }
    var allWeeks = partition(allDates, 7);
    var monthHeader = date.toLocaleDateString(locale, { year: 'numeric', month: 'long' });
    return react_1.default.createElement("tbody", { className: "ant-fullcalendar-tbody", key: "" + date.getFullYear() + date.getMonth() },
        react_1.default.createElement("tr", null,
            react_1.default.createElement("td", { className: "month-divider", colSpan: 7 }, monthHeader)),
        allWeeks.map(function (week, index) {
            return react_1.default.createElement("tr", { role: "row", key: index }, week.map(function (date, index) {
                return CalendarDayGenerator(date, date.getMonth() !== thisMonth, index);
            }));
        }));
}
exports.CalendarMonth = CalendarMonth;
exports.CalendarDayDefault = function (props) {
    return react_1.default.createElement("td", { role: "gridcell", className: "ant-fullcalendar-cell" },
        react_1.default.createElement("div", { className: "ant-fullcalendar-date" },
            react_1.default.createElement("div", { className: "ant-fullcalendar-value" }, props.date.getDate()),
            react_1.default.createElement("div", { className: "ant-fullcalendar-content" })));
};
// https://stackoverflow.com/questions/11345296/partitioning-in-javascript
function partition(input, spacing) {
    var output = [];
    for (var i = 0; i < input.length; i += spacing) {
        output[output.length] = input.slice(i, i + spacing);
    }
    return output;
}
//# sourceMappingURL=calendar.js.map