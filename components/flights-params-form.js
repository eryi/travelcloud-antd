"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var moment_1 = __importDefault(require("moment"));
var antd_1 = require("antd");
var FormValueContext = react_1.default.createContext(null);
var FormRcContext = react_1.default.createContext(null);
var FormState = react_1.default.createContext(null);
var validKeys = [
    "source",
    "od1.origin_airport.code",
    "od1.origin_datetime",
    "od1.destination_airport.code",
    "od2.origin_airport.code",
    "od2.origin_datetime",
    "od2.destination_airport.code",
    "cabin",
    "ptc_adt",
    "ptc_cnn",
    "ptc_inf"
];
var FlightPtcAdtSelect = /** @class */ (function (_super) {
    __extends(FlightPtcAdtSelect, _super);
    function FlightPtcAdtSelect() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightPtcAdtSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(antd_1.Select, { onChange: function (value) { return rc.handle('ptc_adt', value); }, value: formValue.ptc_adt, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className },
                    react_1.default.createElement(antd_1.Select.Option, { value: 1 }, "1"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 2 }, "2"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 3 }, "3"));
            });
        });
    };
    return FlightPtcAdtSelect;
}(react_1.default.PureComponent));
exports.FlightPtcAdtSelect = FlightPtcAdtSelect;
var FlightPtcCnnSelect = /** @class */ (function (_super) {
    __extends(FlightPtcCnnSelect, _super);
    function FlightPtcCnnSelect() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightPtcCnnSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(antd_1.Select, { onChange: function (value) { return rc.handle('ptc_cnn', value); }, value: formValue.ptc_cnn, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className },
                    react_1.default.createElement(antd_1.Select.Option, { value: 0 }, "0"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 1 }, "1"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 2 }, "2"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 3 }, "3"));
            });
        });
    };
    return FlightPtcCnnSelect;
}(react_1.default.PureComponent));
exports.FlightPtcCnnSelect = FlightPtcCnnSelect;
var FlightPtcInfSelect = /** @class */ (function (_super) {
    __extends(FlightPtcInfSelect, _super);
    function FlightPtcInfSelect() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightPtcInfSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(antd_1.Select, { onChange: function (value) { return rc.handle('ptc_inf', value); }, value: formValue.ptc_inf, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className },
                    react_1.default.createElement(antd_1.Select.Option, { value: 0 }, "0"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 1 }, "1"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 2 }, "2"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 3 }, "3"));
            });
        });
    };
    return FlightPtcInfSelect;
}(react_1.default.PureComponent));
exports.FlightPtcInfSelect = FlightPtcInfSelect;
var FlightCabinSelect = /** @class */ (function (_super) {
    __extends(FlightCabinSelect, _super);
    function FlightCabinSelect() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightCabinSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(antd_1.Select, { onChange: function (value) { return rc.handle('cabin', value); }, value: formValue.cabin, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className },
                    react_1.default.createElement(antd_1.Select.Option, { value: 'Y' }, "Economy"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 'S' }, "Premium Economy"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 'C' }, "Business"),
                    react_1.default.createElement(antd_1.Select.Option, { value: 'F' }, "First"));
            });
        });
    };
    return FlightCabinSelect;
}(react_1.default.PureComponent));
exports.FlightCabinSelect = FlightCabinSelect;
var FlightOd2OriginDepartureDatePicker = /** @class */ (function (_super) {
    __extends(FlightOd2OriginDepartureDatePicker, _super);
    function FlightOd2OriginDepartureDatePicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightOd2OriginDepartureDatePicker.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(antd_1.DatePicker, { value: formValue['od2.origin_datetime'] == null || formValue['od2.origin_datetime'] === '' ? null : moment_1.default(formValue['od2.origin_datetime']), defaultPickerValue: formValue['od1.origin_datetime'] == null ? null : moment_1.default(formValue['od1.origin_datetime']).add(1, 'days'), disabledDate: rc.disabledEndDate, style: _this.props.style, placeholder: _this.props.placeholder || "(one way)", className: _this.props.className, format: "YYYY-MM-DD", onChange: function (value) { return rc.handle('od2.origin_datetime', value); } });
            });
        });
    };
    return FlightOd2OriginDepartureDatePicker;
}(react_1.default.PureComponent));
exports.FlightOd2OriginDepartureDatePicker = FlightOd2OriginDepartureDatePicker;
var FlightOd1OriginDepartureDatePicker = /** @class */ (function (_super) {
    __extends(FlightOd1OriginDepartureDatePicker, _super);
    function FlightOd1OriginDepartureDatePicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightOd1OriginDepartureDatePicker.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(antd_1.DatePicker, { value: formValue['od1.origin_datetime'] == null || formValue['od1.origin_datetime'] === '' ? null : moment_1.default(formValue['od1.origin_datetime']), disabledDate: rc.disabledStartDate, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, format: "YYYY-MM-DD", onChange: function (value) { return rc.handle('od1.origin_datetime', value); } });
            });
        });
    };
    return FlightOd1OriginDepartureDatePicker;
}(react_1.default.PureComponent));
exports.FlightOd1OriginDepartureDatePicker = FlightOd1OriginDepartureDatePicker;
var FlightOd1OriginCodeSelect = /** @class */ (function (_super) {
    __extends(FlightOd1OriginCodeSelect, _super);
    function FlightOd1OriginCodeSelect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            options: null
        };
        _this.handleSearch = function (rc, searchValue, currentValue) { return __awaiter(_this, void 0, void 0, function () {
            var params, result, options;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (searchValue.length < 3)
                            return [2 /*return*/];
                        params = { type: 'iata', search: searchValue };
                        if (currentValue != null) {
                            params.values = currentValue;
                        }
                        return [4 /*yield*/, rc.props.client.autoComplete(params)
                            //FIXME: don't crash when when result returns error
                        ];
                    case 1:
                        result = _a.sent();
                        options = result.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.value, value: d.value },
                            d.label,
                            " (",
                            d.value,
                            ")"); });
                        if (result.result != null) {
                            this.setState({
                                options: options
                            });
                        }
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    FlightOd1OriginCodeSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(FormState.Consumer, null, function (state) {
                    return react_1.default.createElement(antd_1.Select, { showSearch: true, onChange: function (value) { return rc.handle('od1.origin_airport.code', value); }, value: formValue['od1.origin_airport.code'], onSearch: function (searchValue) { return _this.handleSearch(rc, searchValue, formValue['od1.origin_airport.code']); }, filterOption: function () { return true; }, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, defaultActiveFirstOption: false }, _this.state.options || state.defaultOptions);
                });
            });
        });
    };
    return FlightOd1OriginCodeSelect;
}(react_1.default.PureComponent));
exports.FlightOd1OriginCodeSelect = FlightOd1OriginCodeSelect;
var FlightOd1DestinationCodeSelect = /** @class */ (function (_super) {
    __extends(FlightOd1DestinationCodeSelect, _super);
    function FlightOd1DestinationCodeSelect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            options: null
        };
        _this.handleSearch = function (rc, searchValue, currentValue) { return __awaiter(_this, void 0, void 0, function () {
            var params, result, options;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (searchValue.length < 3)
                            return [2 /*return*/];
                        params = { type: 'iata', search: searchValue };
                        if (currentValue != null) {
                            params.values = currentValue;
                        }
                        return [4 /*yield*/, rc.props.client.autoComplete(params)
                            //FIXME: don't crash when when result returns error
                        ];
                    case 1:
                        result = _a.sent();
                        options = result.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.value, value: d.value },
                            d.label,
                            " (",
                            d.value,
                            ")"); });
                        if (result.result != null) {
                            this.setState({
                                options: options
                            });
                        }
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    FlightOd1DestinationCodeSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(FormState.Consumer, null, function (state) {
                    return react_1.default.createElement(antd_1.Select, { showSearch: true, onChange: function (value) { return rc.handle('od1.destination_airport.code', value); }, value: formValue['od1.destination_airport.code'], onSearch: function (searchValue) { return _this.handleSearch(rc, searchValue, formValue['od1.destination_airport.code']); }, filterOption: function () { return true; }, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, defaultActiveFirstOption: false }, _this.state.options || state.defaultOptions);
                });
            });
        });
    };
    return FlightOd1DestinationCodeSelect;
}(react_1.default.PureComponent));
exports.FlightOd1DestinationCodeSelect = FlightOd1DestinationCodeSelect;
var FlightOd2OriginCodeSelect = /** @class */ (function (_super) {
    __extends(FlightOd2OriginCodeSelect, _super);
    function FlightOd2OriginCodeSelect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            options: null
        };
        _this.handleSearch = function (rc, searchValue, currentValue) { return __awaiter(_this, void 0, void 0, function () {
            var params, result, options;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (searchValue.length < 3)
                            return [2 /*return*/];
                        params = { type: 'iata', search: searchValue };
                        if (currentValue != null) {
                            params.values = currentValue;
                        }
                        return [4 /*yield*/, rc.props.client.autoComplete(params)
                            //FIXME: don't crash when when result returns error
                        ];
                    case 1:
                        result = _a.sent();
                        options = result.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.value, value: d.value },
                            d.label,
                            " (",
                            d.value,
                            ")"); });
                        if (result.result != null) {
                            this.setState({
                                options: options
                            });
                        }
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    FlightOd2OriginCodeSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(FormState.Consumer, null, function (state) {
                    return react_1.default.createElement(antd_1.Select, { showSearch: true, onChange: function (value) { return rc.handle('od2.origin_airport.code', value); }, value: formValue['od2.origin_airport.code'], onSearch: function (searchValue) { return _this.handleSearch(rc, searchValue, formValue['od2.origin_airport.code']); }, filterOption: function () { return true; }, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, defaultActiveFirstOption: false }, _this.state.options || state.defaultOptions);
                });
            });
        });
    };
    return FlightOd2OriginCodeSelect;
}(react_1.default.PureComponent));
exports.FlightOd2OriginCodeSelect = FlightOd2OriginCodeSelect;
var FlightOd2DestinationCodeSelect = /** @class */ (function (_super) {
    __extends(FlightOd2DestinationCodeSelect, _super);
    function FlightOd2DestinationCodeSelect() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            options: null
        };
        _this.handleSearch = function (rc, searchValue, currentValue) { return __awaiter(_this, void 0, void 0, function () {
            var params, result, options;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (searchValue.length < 3)
                            return [2 /*return*/];
                        params = { type: 'iata', search: searchValue };
                        if (currentValue != null) {
                            params.values = currentValue;
                        }
                        return [4 /*yield*/, rc.props.client.autoComplete(params)
                            //FIXME: don't crash when when result returns error
                        ];
                    case 1:
                        result = _a.sent();
                        options = result.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.value, value: d.value },
                            d.label,
                            " (",
                            d.value,
                            ")"); });
                        if (result.result != null) {
                            this.setState({
                                options: options
                            });
                        }
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    FlightOd2DestinationCodeSelect.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(FormState.Consumer, null, function (state) {
                    return react_1.default.createElement(antd_1.Select, { showSearch: true, onChange: function (value) { return rc.handle('od2.destination_airport.code', value); }, value: formValue['od2.destination_airport.code'], onSearch: function (searchValue) { return _this.handleSearch(rc, searchValue, formValue['od2.destination_airport.code']); }, filterOption: function () { return true; }, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, defaultActiveFirstOption: false }, _this.state.options || state.defaultOptions);
                });
            });
        });
    };
    return FlightOd2DestinationCodeSelect;
}(react_1.default.PureComponent));
exports.FlightOd2DestinationCodeSelect = FlightOd2DestinationCodeSelect;
var FlightsParamsForm = /** @class */ (function (_super) {
    __extends(FlightsParamsForm, _super);
    function FlightsParamsForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            endOpen: false,
            defaultOptions: null,
            data1: [],
            data2: []
        };
        _this.disabledStartDate = function (od1_origin_departure) {
            // not sure why od1_origin_departure might be null
            if (od1_origin_departure == null)
                return false;
            return od1_origin_departure.isBefore();
        };
        _this.disabledEndDate = function (od2_origin_departure) {
            if (od2_origin_departure == null)
                return false;
            var od1_origin_departure = _this.props.value['od1.origin_datetime'] == null ? moment_1.default() : moment_1.default(_this.props.value['od1.origin_datetime']);
            if (!od2_origin_departure || !od1_origin_departure) {
                return od2_origin_departure && od2_origin_departure < moment_1.default().endOf('day');
            }
            return od2_origin_departure.isBefore(moment_1.default(od1_origin_departure).add(1, 'days'));
        };
        return _this;
    }
    FlightsParamsForm.prototype.componentDidMount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var defaultClone, result, options;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        defaultClone = this.props.defaultIataCodes.slice(0);
                        if (this.props.value['od1.origin_airport.code'] != null) {
                            defaultClone.push(this.props.value['od1.origin_airport.code']);
                        }
                        if (this.props.value['od2.origin_airport.code'] != null) {
                            defaultClone.push(this.props.value['od2.origin_airport.code']);
                        }
                        return [4 /*yield*/, this.props.client.autoComplete({ type: 'iata', values: defaultClone })];
                    case 1:
                        result = _a.sent();
                        options = result.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.value, value: d.value },
                            d.label,
                            " (",
                            d.value,
                            ")"); });
                        if (result.result != null) {
                            this.setState({
                                defaultOptions: options
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    FlightsParamsForm.prototype.handle = function (key, value) {
        var _a;
        // Select passes string onChange
        // Input passes event onChange
        var update;
        if (value.target != null)
            value = value.target.value;
        if (value.format != null)
            value = value.format('YYYY-MM-DD');
        update = (_a = {},
            _a[key] = value,
            _a);
        var allData = Object.assign({}, this.props.value, update);
        for (var i in allData) {
            if (allData[i] == null)
                delete allData[i];
            else if (validKeys.indexOf(i) === -1)
                delete allData[i];
        }
        this.props.onChange(allData);
    };
    FlightsParamsForm.prototype.render = function () {
        var children = this.props.children || react_1.default.createElement(antd_1.Form, { layout: "vertical", style: { marginTop: 16 } },
            react_1.default.createElement(antd_1.Form.Item, { label: "From" },
                react_1.default.createElement(FlightOd1OriginCodeSelect, null)),
            react_1.default.createElement(antd_1.Form.Item, { label: "To" },
                react_1.default.createElement(FlightOd2OriginCodeSelect, null)),
            react_1.default.createElement(antd_1.Form.Item, { label: "Departure date" },
                react_1.default.createElement(FlightOd1OriginDepartureDatePicker, { style: { width: '100%' } })),
            react_1.default.createElement(antd_1.Form.Item, { label: "Return Date" },
                react_1.default.createElement(FlightOd2OriginDepartureDatePicker, { style: { width: '100%' } })),
            react_1.default.createElement(antd_1.Form.Item, { label: "Adults" },
                react_1.default.createElement(FlightPtcAdtSelect, null)),
            react_1.default.createElement(antd_1.Form.Item, { label: "Children" },
                react_1.default.createElement(FlightPtcCnnSelect, null)),
            react_1.default.createElement(antd_1.Form.Item, { label: "Infants" },
                react_1.default.createElement(FlightPtcInfSelect, null)));
        return (react_1.default.createElement(FormValueContext.Provider, { value: this.props.value },
            react_1.default.createElement(FormRcContext.Provider, { value: this },
                react_1.default.createElement(FormState.Provider, { value: this.state }, children))));
    };
    FlightsParamsForm.Od1OriginCodeSelect = FlightOd1OriginCodeSelect;
    FlightsParamsForm.Od1DestinationCodeSelect = FlightOd1DestinationCodeSelect;
    FlightsParamsForm.Od2OriginCodeSelect = FlightOd2OriginCodeSelect;
    FlightsParamsForm.Od2DestinationCodeSelect = FlightOd2DestinationCodeSelect;
    FlightsParamsForm.Od1OriginDepartureDatePicker = FlightOd1OriginDepartureDatePicker;
    FlightsParamsForm.Od2OriginDepartureDatePicker = FlightOd2OriginDepartureDatePicker;
    FlightsParamsForm.PtcAdtSelect = FlightPtcAdtSelect;
    FlightsParamsForm.PtcCnnSelect = FlightPtcCnnSelect;
    FlightsParamsForm.PtcInfSelect = FlightPtcInfSelect;
    return FlightsParamsForm;
}(react_1.default.PureComponent));
exports.FlightsParamsForm = FlightsParamsForm;
var FlightsParamsAdvancedForm = /** @class */ (function (_super) {
    __extends(FlightsParamsAdvancedForm, _super);
    function FlightsParamsAdvancedForm() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FlightsParamsAdvancedForm.prototype.render = function () {
        var _this = this;
        var rowStyle = { display: "flex", flexDirection: "row", maxWidth: "1000px", margin: "20px 0" };
        return react_1.default.createElement(FlightsParamsForm, __assign({}, this.props),
            react_1.default.createElement(antd_1.Radio.Group, { value: this.props.type, onChange: function (e) { return _this.props.onTypeChange(e.target.value); } },
                react_1.default.createElement(antd_1.Radio.Button, { value: "return" }, "Return"),
                react_1.default.createElement(antd_1.Radio.Button, { value: "one" }, "One way"),
                react_1.default.createElement(antd_1.Radio.Button, { value: "multi" }, "Multi-city")),
            this.props.type !== 'multi'
                ? react_1.default.createElement("div", { style: rowStyle },
                    react_1.default.createElement("div", { style: { flex: '2' } },
                        react_1.default.createElement("div", null, "From"),
                        react_1.default.createElement(FlightsParamsForm.Od1OriginCodeSelect, { style: { width: '95%' } })),
                    react_1.default.createElement("div", { style: { flex: '0' } },
                        react_1.default.createElement("div", null, "\u00A0"),
                        react_1.default.createElement("div", null,
                            react_1.default.createElement(antd_1.Icon, { type: "swap", onClick: function () { if (_this.props.onSwapClick != null)
                                    _this.props.onSwapClick(); }, style: { fontSize: '18px', padding: '5px 15px 5px' } }))),
                    react_1.default.createElement("div", { style: { flex: '2' } },
                        react_1.default.createElement("div", null, "To"),
                        react_1.default.createElement(FlightsParamsForm.Od2OriginCodeSelect, { style: { width: '95%' } })),
                    react_1.default.createElement("div", { style: { flex: '1' } },
                        react_1.default.createElement("div", null, "Depart"),
                        react_1.default.createElement(FlightsParamsForm.Od1OriginDepartureDatePicker, { style: { width: '95%' } })),
                    this.props.type === 'return' && react_1.default.createElement("div", { style: { flex: '1' } },
                        react_1.default.createElement("div", null, "Return"),
                        react_1.default.createElement(FlightsParamsForm.Od2OriginDepartureDatePicker, { style: { width: '95%' } })))
                : react_1.default.createElement("div", null,
                    react_1.default.createElement("div", { style: rowStyle },
                        react_1.default.createElement("div", { style: { flex: '2' } },
                            react_1.default.createElement(FlightsParamsForm.Od1OriginCodeSelect, { style: { width: '95%' } })),
                        react_1.default.createElement("div", { style: { width: '30px' } }, "\u2192"),
                        react_1.default.createElement("div", { style: { flex: '2' } },
                            react_1.default.createElement(FlightsParamsForm.Od1DestinationCodeSelect, { style: { width: '95%' } })),
                        react_1.default.createElement("div", { style: { flex: '1' } },
                            react_1.default.createElement(FlightsParamsForm.Od1OriginDepartureDatePicker, { style: { width: '95%' } }))),
                    react_1.default.createElement("div", { style: rowStyle },
                        react_1.default.createElement("div", { style: { flex: '2' } },
                            react_1.default.createElement(FlightsParamsForm.Od2OriginCodeSelect, { style: { width: '95%' } })),
                        react_1.default.createElement("div", { style: { width: '30px' } }, "\u2192"),
                        react_1.default.createElement("div", { style: { flex: '2' } },
                            react_1.default.createElement(FlightsParamsForm.Od2DestinationCodeSelect, { style: { width: '95%' } })),
                        react_1.default.createElement("div", { style: { flex: '1' } },
                            react_1.default.createElement(FlightsParamsForm.Od2OriginDepartureDatePicker, { style: { width: '95%' } })))),
            react_1.default.createElement("div", { style: rowStyle },
                react_1.default.createElement("div", { style: { flex: '1' } },
                    react_1.default.createElement("div", null, "Adults"),
                    react_1.default.createElement(FlightsParamsForm.PtcAdtSelect, { style: { width: '95%' } })),
                react_1.default.createElement("div", { style: { flex: '1' } },
                    react_1.default.createElement("div", null, "Children"),
                    react_1.default.createElement(FlightsParamsForm.PtcCnnSelect, { style: { width: '95%' } })),
                react_1.default.createElement("div", { style: { flex: '1' } },
                    react_1.default.createElement("div", null, "Infants"),
                    react_1.default.createElement(FlightsParamsForm.PtcInfSelect, { style: { width: '95%' } })),
                react_1.default.createElement("div", { style: { flex: '2' } },
                    react_1.default.createElement("div", null, "Cabin"),
                    react_1.default.createElement(FlightCabinSelect, { style: { width: '95%' } })),
                react_1.default.createElement("div", { style: { flex: '2' } },
                    react_1.default.createElement("div", null, "\u00A0"),
                    react_1.default.createElement(antd_1.Button, { type: "primary", disabled: this.props.loading === true, onClick: function () { return _this.props.onSearch(_this.props.value); }, style: { width: '95%' } }, "Search"))));
    };
    return FlightsParamsAdvancedForm;
}(react_1.default.PureComponent));
exports.FlightsParamsAdvancedForm = FlightsParamsAdvancedForm;
//# sourceMappingURL=flights-params-form.js.map