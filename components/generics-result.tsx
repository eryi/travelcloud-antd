import React from 'react'
import { List, Tabs, Button } from 'antd'
import { formatCurrency } from '../travelcloud'
import { IncDecInput } from './inc-dec-input'
import  Big from 'big.js'

export class GenericsResult extends React.PureComponent<{className?: string, order: any, genericsResult: any, onAddToCart?: (generic: any, form: {option_id: string, quantity: number}[]) => any, style?: React.CSSProperties}> {
  state: {[key:string]: any} = {
    order: null,
    orderState: {}
  }

  static getDerivedStateFromProps(props, currentState) {
    if (currentState != null && props.order.result != null && props.order !== currentState.order) {
      const products = props.order.result.products || []
      const orderState = products.reduce((acc, product) => {
        if (product.product_type !== 'nontour_product') return acc
        acc[product.product_id] = product.quantity
        return acc
      }, {})

      return {
        order: props.order,
        orderState,
        ...orderState
      }
    }
    return null
  }

  render () {
    const {genericsResult, onAddToCart, style, className} = this.props
    const Total = ({item}) => {
      const total = item.options.reduce((acc, option) => {
        const key = item.id + '/' + option.id
        const qty = this.state[key] || 0
        const orderQty = this.state.orderState[key] || 0
        const subTotal = Big(option.price).times(qty)
        acc.onAddToCartPayload.push({
          generic_id: item.id,
          option_id: option.id,
          quantity: qty
        })
        return {
          totalPrice: acc.totalPrice.add(subTotal),
          totalQty: acc.totalQty + qty,
          onAddToCartPayload: acc.onAddToCartPayload,
          notSameAsOrder: acc.notSameAsOrder || qty !== orderQty,
          orderIsEmpty: acc.orderIsEmpty && orderQty === 0}
      }, {
        totalPrice: Big(0),
        totalQty: 0,
        onAddToCartPayload: [],
        notSameAsOrder: false,
        orderIsEmpty: true})

      if (total.notSameAsOrder === false) return null
      return <tr style={{borderTop: '2px solid #999'}}>
          <td>Total ({total.totalQty} items)</td>
          <td style={{fontWeight: 'bold'}}>{formatCurrency(total.totalPrice.toFixed(2))}</td>
          <td>
            <Button style={{width: '100%', margin: "8px 0"}}
              type="primary"
              onClick={() => {
                onAddToCart && onAddToCart(item, total.onAddToCartPayload)}}>
              {total.orderIsEmpty
                ? "Add to cart"
                : "Update cart"}</Button></td>
        </tr>
    }
    return <div style={style} className={className}><List
      dataSource={genericsResult}
      renderItem={item => (
        <List.Item>
          <div style={{width: '100%'}}>
            <div style={{
              backgroundImage: `url(${item.photo_url})`,
              height: 400,
              width: "40%",
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              float: 'left'}} />
            <div style={{padding: '16px 32px', float: 'left', width: "60%"}}>
              <h2 className="tc-full-width-ellipsis">{item.name}</h2>
              <Tabs defaultActiveKey="1">
                <Tabs.TabPane tab="Prices" key="1">
                  <table>
                    <tbody>
                      {item.options.map(option => {
                        const key = item.id + '/' + option.id
                        const val = this.state[key] || 0
                        return <tr key={option.id}>
                          <td style={{paddingRight: 32, minWidth: 150}}><div className="tc-full-width-ellipsis">{option.name}</div></td>
                          <td style={{paddingRight: 32, fontWeight: 'bold'}}>{formatCurrency(option.price)}</td>
                          <td style={{width: 150}}><IncDecInput min={0} max={option.stock} value={val} onChange={(newVal) => this.setState({[key]: newVal})} /></td>
                        </tr>})}
                      <tr style={{height: 16}}>
                        <td colSpan={3}></td>
                      </tr>
                      <Total item={item} />
                    </tbody>
                  </table>
                </Tabs.TabPane>
                <Tabs.TabPane tab="Description" key="2"><div style={{height: 260, overflowY: 'auto'}} dangerouslySetInnerHTML={{__html: item.description}} /></Tabs.TabPane>
              </Tabs>
            </div>
          </div>
        </List.Item>
      )}
    /></div>}
}