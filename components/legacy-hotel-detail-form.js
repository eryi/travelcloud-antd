"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var moment_1 = __importDefault(require("moment"));
var antd_1 = require("antd");
var FormValueContext = react_1.default.createContext(null);
var FormRcContext = react_1.default.createContext(null);
var FormState = react_1.default.createContext(null);
var validKeys = [
    'cityCode',
    'checkInDate',
    'checkOutDate',
    'adults'
];
/**
 * Represents the cheack availability logic of the hotel page
 */
var LegacyHotelCheckOutDatePicker = /** @class */ (function (_super) {
    __extends(LegacyHotelCheckOutDatePicker, _super);
    function LegacyHotelCheckOutDatePicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LegacyHotelCheckOutDatePicker.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(FormState.Consumer, null, function (state) {
                    return react_1.default.createElement(antd_1.DatePicker, { value: formValue.checkOutDate == null ? null : moment_1.default(formValue.checkOutDate), defaultPickerValue: formValue.checkInDate == null ? null : moment_1.default(formValue.checkInDate).add(1, 'days'), disabledDate: function (x) { return rc.disabledEndDate(x); }, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, format: "YYYY-MM-DD", onChange: function (value) { return rc.handle('checkOutDate', value); }, open: state.endOpen, onOpenChange: rc.handleEndOpenChange });
                });
            });
        });
    };
    return LegacyHotelCheckOutDatePicker;
}(react_1.default.PureComponent));
exports.LegacyHotelCheckOutDatePicker = LegacyHotelCheckOutDatePicker;
var LegacyHotelCheckInDatePicker = /** @class */ (function (_super) {
    __extends(LegacyHotelCheckInDatePicker, _super);
    function LegacyHotelCheckInDatePicker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LegacyHotelCheckInDatePicker.prototype.render = function () {
        var _this = this;
        return react_1.default.createElement(FormValueContext.Consumer, null, function (formValue) {
            return react_1.default.createElement(FormRcContext.Consumer, null, function (rc) {
                return react_1.default.createElement(FormState.Consumer, null, function (state) {
                    return react_1.default.createElement(antd_1.DatePicker, { value: formValue.checkInDate == null ? null : moment_1.default(formValue.checkInDate), disabledDate: function (x) { return rc.disabledStartDate(x); }, style: _this.props.style, placeholder: _this.props.placeholder, className: _this.props.className, format: "YYYY-MM-DD", onChange: function (value) { return rc.handle('checkInDate', value); } });
                });
            });
        });
    };
    return LegacyHotelCheckInDatePicker;
}(react_1.default.PureComponent));
exports.LegacyHotelCheckInDatePicker = LegacyHotelCheckInDatePicker;
var LegacyHotelsDetailForm = /** @class */ (function (_super) {
    __extends(LegacyHotelsDetailForm, _super);
    function LegacyHotelsDetailForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        //static AdultOccupancySelect = LegacyHotelAdultOccupancySelect
        _this.state = {
            endOpen: false,
            defaultOptions: null
        };
        _this.disabledStartDate = function (checkInDate) {
            // not sure why checkInDate might be null
            if (checkInDate == null)
                return false;
            return checkInDate.isBefore();
        };
        _this.disabledEndDate = function (checkOutDate) {
            if (checkOutDate == null)
                return false;
            var checkInDate = _this.props.value.checkInDate == null ? moment_1.default() : moment_1.default(_this.props.value.checkInDate);
            if (!checkOutDate || !checkInDate) {
                return checkOutDate && checkOutDate < moment_1.default().endOf('day');
            }
            return checkOutDate.isBefore(moment_1.default(checkInDate).add(1, 'days'));
        };
        _this.handleStartOpenChange = function (open) {
            if (!open && _this.props.value.checkInDate == null) {
                _this.setState({ endOpen: true });
            }
        };
        _this.handleEndOpenChange = function (open) {
            _this.setState({ endOpen: open });
        };
        return _this;
    }
    LegacyHotelsDetailForm.prototype.componentDidMount = function () {
        return __awaiter(this, void 0, void 0, function () {
            var defaultClone, result, options;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        defaultClone = this.props.defaultCityCodes.slice(0);
                        if (this.props.value.cityCode != null) {
                            defaultClone.push(this.state['cityCode']);
                        }
                        return [4 /*yield*/, this.props.client.autoComplete({ type: 'bedsonline', values: defaultClone })];
                    case 1:
                        result = _a.sent();
                        options = result.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.value, value: d.value }, d.label); });
                        if (result.result != null) {
                            this.setState({
                                defaultOptions: options
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LegacyHotelsDetailForm.prototype.handle = function (key, value) {
        var _a;
        // Select passes string onChange
        // Input passes event onChange
        var update;
        if (value.target != null)
            value = value.target.value;
        if (value.format != null)
            value = value.format('YYYY-MM-DD');
        update = (_a = {},
            _a[key] = value,
            _a);
        if (key === 'checkInDate' && this.props.value.checkOutDate == null) {
            this.setState({
                endOpen: true
            });
        }
        var allData = Object.assign({}, this.props.value, update);
        for (var i in allData) {
            if (allData[i] == null)
                delete allData[i];
            else if (validKeys.indexOf(i) === -1)
                delete allData[i];
        }
        this.props.onChange(allData);
    };
    LegacyHotelsDetailForm.prototype.render = function () {
        var children = this.props.children || react_1.default.createElement(antd_1.Form, { layout: "vertical", style: { marginTop: 16 } },
            react_1.default.createElement(antd_1.Form.Item, { label: "Check-in" },
                react_1.default.createElement(LegacyHotelCheckInDatePicker, { style: { width: '100%' } })),
            react_1.default.createElement(antd_1.Form.Item, { label: "Check-out" },
                react_1.default.createElement(LegacyHotelCheckOutDatePicker, { style: { width: '100%' } })));
        return (react_1.default.createElement(FormValueContext.Provider, { value: this.props.value },
            react_1.default.createElement(FormRcContext.Provider, { value: this },
                react_1.default.createElement(FormState.Provider, { value: this.state }, children))));
    };
    //static CityCodeSelect = LegacyHotelCityCodeSelect
    LegacyHotelsDetailForm.CheckInDatePicker = LegacyHotelCheckInDatePicker;
    LegacyHotelsDetailForm.CheckOutDatePicker = LegacyHotelCheckOutDatePicker;
    return LegacyHotelsDetailForm;
}(react_1.default.PureComponent));
exports.LegacyHotelsDetailForm = LegacyHotelsDetailForm;
//# sourceMappingURL=legacy-hotel-detail-form.js.map