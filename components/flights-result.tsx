import React from 'react'
import { formatCurrency, TcResponse, Cart } from '../travelcloud'
import { Button, Icon } from 'antd'
import moment from 'moment'
import { NoResult } from './no-result'
import {FlightSearch, FlightDetail} from '../types'
import Big from 'big.js';

function ignoreTimezoneExtractTime( datetime ) {
  return moment( datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,'') ).format( 'h:mm:ss a' );
}

function ignoreTimezoneExtractDate( datetime ) {
  return moment( datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/,'') ).format( 'Do MMMM[,] YYYY' );
}

function computePrice(cart: Cart, flight: FlightDetail, fareId: string): {
  beforePriceRules: string,
  afterPriceRules: string
} {
  const fare = flight.fares.find((fare) => fare.id == fareId)
  if (fare == null) {
    return {
      afterPriceRules: "Invalid fare id",
      beforePriceRules: "Invalid fare id"}}

  if (cart == null) {
    return {
      afterPriceRules: formatCurrency(fare.price),
      beforePriceRules: formatCurrency(fare.price)}}

  const product = cart.addFlight(flight, fareId, true)

  if (product == null) {
    return {
      afterPriceRules: formatCurrency(fare.price),
      beforePriceRules: formatCurrency(fare.price)}
  }

  // more like "before discounts" and "after discounts"
  const beforePriceRules = product.items.filter((item) => parseInt(item['price']) > 0).reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
  const afterPriceRules = product.items.reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

  return {
    afterPriceRules: formatCurrency(afterPriceRules),
    beforePriceRules: formatCurrency(beforePriceRules)}
}

function renderFares(flightIndex: number, cart: Cart, flight: FlightDetail, onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any) {
  const indexServices = (acc2, fare, fareDetailIndex) => {
    if (acc2[fareDetailIndex] == null) acc2[fareDetailIndex] = {
      origin: fare.origin,
      destination: fare.destination,
      servicesSet: {}
    }
    const brand = fare.brand
    if (brand == null) return acc2
    const services = brand.services
    if (services == null) return acc2
    acc2[fareDetailIndex].servicesSet = services.reduce((acc3, service) => {
      acc3[service.name] = true
      return acc3
    }, acc2[fareDetailIndex].servicesSet)
    return acc2
  }

  const fares = flight.fares
  var fareServicesIndexed = fares.reduce((acc, fare) => {
    return fare.ptc_fare_detail_map.adt.reduce(indexServices, acc)
  }, new Array(fares[0].ptc_fare_detail_map.adt.length))

  fareServicesIndexed = fareServicesIndexed.map(x => {
    x.servicesSet = Object.keys(x.servicesSet)
    return x
  })

  const cellStyle = {height: 50, borderBottom: "1px solid #aaa", lineHeight: '50px', paddingLeft: 10, overflow: 'hidden'}
  const headerStyle = {fontSize: 16, backgroundColor: '#fed', fontWeight: 'bold' as any}

  return <div style={{margin: "30px"}}>
    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
      <div style={{display: 'block', width: 200, margin: '0 10px'}}>
        <div style={{height: 122, lineHeight: "122px", fontSize: 20, color: '#666', textAlign: 'center'}}>
        </div>
        {fareServicesIndexed.map((info, index) => {
          return <div key={index}>
            <div style={{...cellStyle, ...headerStyle}}>{info.origin.code} → {info.destination.code}</div>
            {info.servicesSet.map(serviceName => <div key={serviceName} style={cellStyle}>{serviceName}</div>)}
          </div>})}
      </div>
      {fares.map((detail, i1) => {
        const computed = computePrice(cart, flight, detail.id)
        return <div key={i1} style={{width: 200, border: "2px solid #eee", borderRadius: 5, margin: '0 10px'}}>
          <div style={{height: 120, textAlign: 'center', padding: 15}}>
            <div style={{fontSize: 20, color: '#666'}}>{computed.afterPriceRules}</div>
            <div style={{margin: 10}}><Button type="primary" onClick={() => onFareClick(flightIndex, flight, detail.id)}>Select</Button></div>
          </div>
        {detail.ptc_fare_detail_map.adt.map((fare, i2) => {
          const servicesIndexed = fare.brand == null || fare.brand.services == null ? {} : fare.brand.services.reduce((acc, service) => {
            acc[service.name] = service
            return acc
          }, {})
          return <div key={i2}>
            <div style={{...cellStyle, ...headerStyle}}>{fare.brand != null && fare.brand.name != null ? fare.brand.name : fare.cabin}</div>
            {fareServicesIndexed[i2].servicesSet.map((serviceName, index) => {
              return <div key={index} style={{...cellStyle, textAlign: 'center', fontSize: 28}}>
                {servicesIndexed[serviceName] != null && servicesIndexed[serviceName].included === true ? <Icon style={{color: '#7C7'}} type="check" /> : <Icon style={{color: '#C77'}} type="close" />}
            </div>})}
          </div>})}
      </div>})}
    </div>
  </div>
}

export const FlightsResult: React.StatelessComponent<{
  flights: FlightSearch,
  flightMap: {[key: string]: TcResponse<FlightDetail>},
  onFlightClick?: (index: number, flight: any) => any,
  onFareClick?: (index: number, flight: FlightDetail, fareId: string) => any,
  cart: Cart}>

  = ({flights, flightMap, onFlightClick, onFareClick, cart}) => {

    return <>{flights.map((flight, index) => {

      const departingFlight = flight.od1
      const departingFlightFirstSegment = departingFlight.segments[0]
      const departingFlightLastSegment = departingFlight.segments[departingFlight.segments.length - 1]
      const departingFlightFirstLeg = departingFlightFirstSegment.flights[0]
      const departingFlightLastLeg = departingFlightLastSegment.flights[departingFlightLastSegment.flights.length -1]
      //const departingFlightAdtFirstFare = flight.od1_ptc_fare_map.adt[0]
      //const departingFlightHasBaggage = departingFlightAdtFirstFare.baggage_pieces != null || departingFlightAdtFirstFare.baggage_weight_amount != null

      const returningFlight = flight.od2
      const returningFlightFirstSegment = returningFlight && returningFlight.segments[0]
      const returningFlightLastSegment = returningFlight && returningFlight.segments[returningFlight.segments.length - 1]
      const returningFlightFirstLeg = returningFlight && returningFlightFirstSegment.flights[0]
      const returningFlightLastLeg = returningFlight && returningFlightLastSegment.flights[returningFlightLastSegment.flights.length -1]
      //const returningFlightAdtFirstFare = returningFlight && flight.od2_ptc_fare_map.adt[0]
      //const returningFlightHasBaggage = returningFlight && (returningFlightAdtFirstFare.baggage_pieces != null || returningFlightAdtFirstFare.baggage_weight_amount != null)

      const computed = computePrice(cart, flight, flight.fares[0].id)

      return <div className="ant-card" style={{padding: 16, border: '1px solid #e8e8e8'}} key={index}>
        <table style={{width: '100%'}}><tbody>
          <tr>
            <td style={{width: '70%'}}>
              <table style={{width: '100%'}}><tbody>
                <tr><th colSpan={3}>Departing flight</th></tr>
                <tr>
                  <td style={{width: '30%'}}>
                    <img src={`//cdn.net.in/static/airlines/${departingFlightFirstSegment.marketing_carrier.code}.gif`} />
                  </td>
                  <td style={{width: '30%'}}>
                    <div>
                      <span>{departingFlightFirstLeg.departure_airport.code}</span>
                      <span> →</span>
                    </div>
                    <div>{ignoreTimezoneExtractTime(departingFlightFirstLeg.departure_datetime)}</div>
                    <div>{ignoreTimezoneExtractDate(departingFlightFirstLeg.departure_datetime)}</div>
                  </td>
                  <td style={{width: '30%'}}>
                    <div>
                      <span>{departingFlightLastLeg.arrival_airport.code}</span>
                    </div>
                    <div>{ignoreTimezoneExtractTime(departingFlightLastLeg.arrival_datetime)}</div>
                    <div>{ignoreTimezoneExtractDate(departingFlightLastLeg.arrival_datetime)}</div>
                  </td>
                </tr>
                {returningFlight
                  && [<tr key="1"><th colSpan={3}>Return flight</th></tr>,
                    <tr key="2">
                      <td style={{width: '30%'}}>
                        <img src={`//cdn.net.in/static/airlines/${returningFlightFirstSegment.marketing_carrier.code}.gif`} />
                      </td>
                      <td style={{width: '30%'}}>
                        <div>
                          <span>{returningFlightFirstLeg.departure_airport.code}</span>
                          <span> →</span>
                        </div>
                        <div>{ignoreTimezoneExtractTime(returningFlightFirstLeg.departure_datetime)}</div>
                        <div>{ignoreTimezoneExtractDate(returningFlightFirstLeg.departure_datetime)}</div>
                      </td>
                      <td style={{width: '30%'}}>
                        <div>
                          <span>{returningFlightLastLeg.arrival_airport.code}</span>
                        </div>
                        <div>{ignoreTimezoneExtractTime(returningFlightLastLeg.arrival_datetime)}</div>
                        <div>{ignoreTimezoneExtractDate(returningFlightLastLeg.arrival_datetime)}</div>
                      </td>
                    </tr>]}
              </tbody></table>
            </td>
            <td style={{width: '30%'}}>
              <div style={{margin: "20px 10px", fontSize: 16}}>{
                computed.beforePriceRules === computed.afterPriceRules
                ? computed.beforePriceRules
                : <>
                    <span style={{textDecoration: 'line-through'}}>{computed.beforePriceRules}</span>
                    &nbsp;
                    <b>{computed.afterPriceRules}</b>
                  </>}</div>
              <Button type="primary" disabled={flightMap[index] != null} onClick={() => onFlightClick(index, flight)}>Select flight</Button>
            </td>
          </tr>
          {flightMap[index] != null
            &&
              <tr>
                <td colSpan={4} style={{height:400}}>
                  {flightMap[index].result == null
                    ? <NoResult
                        response={flightMap[index]}
                        type="fares"
                        loadingMessage="Verifying prices and availability..." />
                    : renderFares(index, cart, flightMap[index].result, onFareClick)}
                </td>
              </tr>}
          </tbody></table>
      </div>
    })}</>}