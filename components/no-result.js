"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
exports.NoResult = function (_a) {
    var response = _a.response, style = _a.style, className = _a.className, loading = _a.loading, type = _a.type, children = _a.children, loadingMessage = _a.loadingMessage;
    if (response.error != null) {
        return react_1.default.createElement("div", { style: __assign({ textAlign: 'center' }, style), className: className }, "An error has occured. Please try again later.");
    }
    if (response.loading === true || loading === true) {
        if (loadingMessage == null) {
            loadingMessage = 'Searching...';
        }
        return react_1.default.createElement("div", { style: __assign({ textAlign: 'center' }, style), className: className },
            react_1.default.createElement(antd_1.Spin, { size: "large", style: { margin: 'auto' } }),
            react_1.default.createElement("h1", { style: { width: '100%', marginTop: 32 } }, loadingMessage));
    }
    if (response.result == null) {
        return react_1.default.createElement("div", { style: __assign({ textAlign: 'center' }, style), className: className }, children);
    }
    if (response.result.length === 0) {
        return react_1.default.createElement("div", { style: __assign({ textAlign: 'center' }, style), className: className },
            "No ",
            type,
            " matching your search criteria was found.");
    }
    return null;
};
//# sourceMappingURL=no-result.js.map