"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var moment_1 = __importDefault(require("moment"));
var big_js_1 = __importDefault(require("big.js"));
var Account = /** @class */ (function (_super) {
    __extends(Account, _super);
    function Account() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            loginLoading: false,
            loginRequested: 0,
            loginVerifyStatus: {},
            loginRefreshing: false,
            loginEmail: '',
            loginVerify: '',
        };
        return _this;
    }
    Account.prototype.getPassword = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setState({
                            loginLoading: true
                        });
                        return [4 /*yield*/, this.props.cart.getPassword(this.state.loginEmail)];
                    case 1:
                        _a.sent();
                        this.setState({
                            loginLoading: false,
                            loginRequested: Date.now()
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    Account.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.props.cart.logout();
                return [2 /*return*/];
            });
        });
    };
    Account.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customer;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.setState({
                            loginLoading: true,
                            loginVerifyStatus: {}
                        });
                        return [4 /*yield*/, this.props.cart.login(this.state.loginEmail, this.state.loginVerify)];
                    case 1:
                        customer = _a.sent();
                        if (customer.result == null) {
                            this.setState({
                                loginLoading: false,
                                loginVerify: "",
                                loginVerifyStatus: {
                                    validateStatus: "error",
                                    help: "Invalid password"
                                }
                            });
                        }
                        else {
                            this.setState({
                                loginLoading: false
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Account.prototype.render = function () {
        var _this = this;
        return (react_1.default.createElement("div", null,
            react_1.default.createElement("div", { style: { fontSize: 24, padding: "24px 0" } },
                "My Account ",
                this.props.customer.result != null && react_1.default.createElement("span", { style: { color: '#999', fontSize: 14, marginLeft: 16, cursor: 'pointer' }, onClick: function () { return _this.logout(); } },
                    react_1.default.createElement(antd_1.Icon, { type: "logout" }),
                    " Logout")),
            this.props.customer.result == null && this.state.loginLoading == true
                && react_1.default.createElement("div", null,
                    react_1.default.createElement(antd_1.Spin, { size: "large", style: { display: 'block', margin: '64px auto' } })),
            this.props.customer.result == null && this.state.loginLoading == false
                && (Date.now() - this.state.loginRequested > 600000
                    ? react_1.default.createElement("div", null,
                        react_1.default.createElement("div", null, "We will send a temporary password to your email"),
                        react_1.default.createElement(antd_1.Form, null,
                            react_1.default.createElement(antd_1.Form.Item, null,
                                react_1.default.createElement(antd_1.Input, { placeholder: "E-mail", onChange: function (e) { return _this.setState({ loginEmail: e.target.value }); }, onPressEnter: function () { return _this.getPassword(); } })),
                            react_1.default.createElement(antd_1.Form.Item, null,
                                react_1.default.createElement(antd_1.Button, { size: "large", type: "primary", disabled: this.state.loginLoading, onClick: function () { return _this.getPassword(); } },
                                    react_1.default.createElement(antd_1.Icon, { type: this.state.loginLoading ? "loading" : "mail" }),
                                    " Send email"))))
                    : react_1.default.createElement("div", null,
                        react_1.default.createElement("div", null, "Please check your email for your password"),
                        react_1.default.createElement(antd_1.Form, null,
                            react_1.default.createElement(antd_1.Form.Item, null,
                                react_1.default.createElement(antd_1.Input, { disabled: true, value: this.state.loginEmail })),
                            react_1.default.createElement(antd_1.Form.Item, __assign({}, this.state.loginVerifyStatus),
                                react_1.default.createElement(antd_1.Input, { placeholder: "password", onChange: function (e) { return _this.setState({ loginVerify: e.target.value }); }, onPressEnter: function () { return _this.login(); } })),
                            react_1.default.createElement(antd_1.Form.Item, null,
                                react_1.default.createElement(antd_1.Button, { size: "large", type: "primary", disabled: this.state.loginLoading, onClick: function () { return _this.login(); } },
                                    react_1.default.createElement(antd_1.Icon, { type: this.state.loginLoading ? "loading" : "login" }),
                                    " Login"))))),
            this.props.customer.result != null
                && react_1.default.createElement("div", null,
                    react_1.default.createElement(antd_1.Card, { style: { border: 0 } },
                        react_1.default.createElement(antd_1.Card.Meta, { avatar: react_1.default.createElement(antd_1.Avatar, { size: "large", icon: "user", style: { backgroundColor: "#af0201" } }), title: react_1.default.createElement("span", null,
                                react_1.default.createElement("span", { style: { marginRight: 8 } }, this.props.customer.result.name
                                    ? this.props.customer.result.name
                                    : this.props.customer.result.email),
                                this.props.customer.result.categories.map(function (cat) { return react_1.default.createElement(antd_1.Tag, { key: cat.id }, cat.name); })), description: react_1.default.createElement("span", null,
                                "Account balance $",
                                this.props.customer.result.payments.reduce(function (acc, payment) { return acc.plus(payment.amount); }, big_js_1.default(0)).times(-1).toFixed(2).toString()) })),
                    react_1.default.createElement(antd_1.Tabs, { defaultActiveKey: "1" },
                        react_1.default.createElement(antd_1.Tabs.TabPane, { tab: "Orders", key: "1" },
                            react_1.default.createElement(antd_1.Table, { columns: [{
                                        title: 'Date',
                                        render: function (x) { return react_1.default.createElement("span", null, moment_1.default(x.order_date).format('Do MMMM YYYY')); },
                                        sorter: function (x, y) { return parseInt(x.id) - parseInt(y.id); },
                                        sortOrder: 'descend',
                                        key: 'order_date',
                                    }, {
                                        title: 'Ref',
                                        render: function (x) { return react_1.default.createElement("a", { href: "/payment?ref=" + x.ref }, x.ref); },
                                        key: 'ref',
                                    }, {
                                        title: 'Amount',
                                        dataIndex: 'payment_required',
                                        key: 'payment_required',
                                    }, {
                                        title: 'Status',
                                        dataIndex: 'order_status',
                                        key: 'order_status',
                                    }, {
                                        title: 'Payment',
                                        dataIndex: 'payment_status',
                                        key: 'payment_status',
                                    }], dataSource: this.props.customer.result.orders, rowKey: "id" })),
                        react_1.default.createElement(antd_1.Tabs.TabPane, { tab: "Statement", key: "2" },
                            react_1.default.createElement(antd_1.Table, { columns: [{
                                        title: 'Date',
                                        render: function (x) { return react_1.default.createElement("span", null, moment_1.default(x.date_added).format('Do MMMM YYYY')); },
                                        sorter: function (x, y) { return parseInt(x.id) - parseInt(y.id); },
                                        sortOrder: 'descend',
                                        key: 'date_added',
                                    }, {
                                        title: 'Ref',
                                        render: function (x) { return x.order == null ? 'N/A' : react_1.default.createElement("a", { href: "/payment?ref=" + x.order.ref }, x.order.ref); },
                                        key: 'order.ref',
                                    }, {
                                        title: 'Description',
                                        dataIndex: 'description',
                                        key: 'description',
                                    }, {
                                        title: 'Amount',
                                        dataIndex: 'amount',
                                        key: 'amount',
                                    }], dataSource: this.props.customer.result.payments, rowKey: "id" }))))));
    };
    return Account;
}(react_1.default.Component));
exports.Account = Account;
//# sourceMappingURL=account.js.map