
import React from 'react'
import { Form, Input } from 'antd';
import { WrappedFormUtils } from 'antd/lib/form/Form';

export const ContactForm = Form.create()(
  class extends React.Component<{form: WrappedFormUtils}> {
    render() {
      const { form } = this.props;
      const { getFieldDecorator } = form;

      const formItemLayout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 14 },
      }

      return (
        <Form layout="vertical">
          <Form.Item
              {...formItemLayout}
              label="E-mail">
              {getFieldDecorator('email', {
                rules: [{
                  type: 'email', message: 'E-mail is not valid',
                }, {
                  required: true, message: 'Please enter your E-mail!',
                }],
              })(
                <Input />
              )}
            </Form.Item>
            <Form.Item
              {...formItemLayout}
              label="Phone">
              {getFieldDecorator('phone', {
                rules: [{ required: true, message: 'Please enter your phone number!' }],
              })(
                <Input />
              )}
            </Form.Item>
            <Form.Item
              {...formItemLayout}
              label="Name">
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Please enter your name!', whitespace: true }],
              })(
                <Input />
              )}
            </Form.Item>
            <Form.Item
              {...formItemLayout}
              label="Message">
              {getFieldDecorator('message', {
                rules: [{ required: true, message: 'Please enter your message!', whitespace: true }],
              })(
                <Input.TextArea autosize={{ minRows: 2, maxRows: 6 }} />
              )}
            </Form.Item>
        </Form>
      );
    }
  }
)