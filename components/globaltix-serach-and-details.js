"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var inc_dec_input_1 = require("./inc-dec-input");
var Panel = antd_1.Collapse.Panel;
exports.SearchAndResult = function (_a) {
    var offset = _a.offset, limit = _a.limit, data = _a.data, qtyMap = _a.qtyMap, onQtyChange = _a.onQtyChange, onAttractionClick = _a.onAttractionClick, onTicketClick = _a.onTicketClick, onAddToCart = _a.onAddToCart;
    return (react_1.default.createElement("div", { key: "main-container", className: "search-result" }, data == null || data.length === 0 ? (react_1.default.createElement("h4", null, "There is no record found!")) : (data.map(function (attraction, index) {
        if (index >= offset && index <= limit) {
            return (react_1.default.createElement(antd_1.Collapse, { className: "main-collaspe-relative", key: "collaspepanel" + index, accordion: true },
                react_1.default.createElement(Panel, { header: react_1.default.createElement("div", { className: "header-list1", key: "header" + index },
                        react_1.default.createElement(antd_1.Icon, { key: "starticon" + index, type: "star", className: "list-star" }),
                        react_1.default.createElement("img", { width: "10%", src: "https://uat-api.globaltix.com/api/image?name=" +
                                attraction.imagePath, alt: "No Image", key: "image" + index }),
                        react_1.default.createElement("span", { key: "title" + index }, attraction.title),
                        react_1.default.createElement("span", { key: "iconspan" + index },
                            react_1.default.createElement(antd_1.Icon, { type: "info-circle", className: "attractionInfo", onClick: function () { return onAttractionClick(attraction); } }))), key: "upperpanel" + index }, attraction.ticketTypes.map(function (ticket) { return (react_1.default.createElement("div", { className: "tickets-details", key: "ticket-details" + ticket.id },
                    react_1.default.createElement(antd_1.Row, { type: "flex", justify: "space-between", align: "middle", gutter: 10, key: "ticket-row" + ticket.id },
                        react_1.default.createElement(antd_1.Col, { lg: 1, md: 24, sm: 24, xs: 24, key: "ticket-col" + ticket.id },
                            react_1.default.createElement(antd_1.Icon, { type: "star", className: "list-star1", key: "ticket-star" + ticket.id })),
                        react_1.default.createElement(antd_1.Col, { lg: 10, md: 24, sm: 24, xs: 24, className: "ticket-name", key: "ticket-name" + ticket.id },
                            react_1.default.createElement("p", { key: "catgory-name" + ticket.id },
                                react_1.default.createElement("span", { className: "catgory-name", key: "catgory-name-span" + ticket.id }, ticket.variation.name),
                                react_1.default.createElement("strong", null, ticket.name),
                                react_1.default.createElement("span", { className: "ticketname-des", key: "ticketname-des" + ticket.id }, 'Usual Price ' + ticket.currency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)),
                                react_1.default.createElement("span", { className: "ticket-merchant", key: "ticket-merchant" + ticket.id }, 'Merchant ' + ticket.sourceName))),
                        react_1.default.createElement(antd_1.Col, { lg: 8, md: 24, sm: 24, xs: 24, className: "ticket-price", key: "ticket-price" + ticket.id },
                            react_1.default.createElement("p", { className: "rghtAlign-marginRght", key: "rghtAlign-marginRght" + ticket.id },
                                react_1.default.createElement("span", { key: "ticket-currency" + ticket.id },
                                    react_1.default.createElement("strong", null, ticket.currency + ' '),
                                    parseFloat(ticket.originalPrice).toFixed(2)),
                                react_1.default.createElement("span", { className: "price-des", key: "price-des" + ticket.id },
                                    "Before conversion rate ",
                                    ticket.currency + ' ' + parseFloat(ticket.originalPrice).toFixed(2)))),
                        react_1.default.createElement(antd_1.Col, { lg: 3, md: 24, sm: 24, xs: 24 },
                            react_1.default.createElement(inc_dec_input_1.IncDecInput, { min: 0, max: 9, value: qtyMap[ticket.id] || 0, onChange: function (newVal) { return onQtyChange(ticket.id, newVal); } })),
                        react_1.default.createElement(antd_1.Col, { lg: 2, md: 24, sm: 24, xs: 24, className: "addto-cart", key: "addto-cart" + ticket.id },
                            react_1.default.createElement(antd_1.Button, { disabled: qtyMap[ticket.id] === 0 || qtyMap[ticket.id] == null, type: "primary", onClick: function () { return onAddToCart(attraction, ticket); } }, "Add to cart"))),
                    react_1.default.createElement("div", { className: "ticketInfo-sec", key: "ticketInfo-sec" + ticket.id },
                        react_1.default.createElement(antd_1.Icon, { key: "ticketInfo-icon" + ticket.id, type: "info-circle", className: "ticketInfo", onClick: function () { return onTicketClick(ticket); } })))); }))));
        }
    }))));
};
//# sourceMappingURL=globaltix-serach-and-details.js.map