"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var country_dropdown_1 = require("../components/country-dropdown");
var travelcloud_1 = require("../travelcloud");
function vw(value) {
    return { value: value };
}
exports.initialCheckoutForm = function (order, demo) {
    if (demo === void 0) { demo = false; }
    if (order == null || order.products == null)
        return null;
    var maxAdult = order.products.reduce(function (acc, product) { return product.adult != null ? Math.max(parseInt(product.adult), acc) : acc; }, 0);
    var maxChild = order.products.reduce(function (acc, product) { return product.child != null ? Math.max(parseInt(product.child), acc) : acc; }, 0);
    var maxInfant = order.products.reduce(function (acc, product) { return product.infant != null ? Math.max(parseInt(product.infant), acc) : acc; }, 0);
    var types = Array(maxAdult).fill('adult').concat(Array(maxChild).fill('child')).concat(Array(maxInfant).fill('infant'));
    if (demo === false) {
        var travelers_1 = types.map(function (type) { return ({ type: vw(type) }); });
        return { travelers: travelers_1 };
    }
    var travelers = types.map(function (type, index) {
        var now = new Date();
        var birth_date = '1990-12-25';
        if (type === 'adult') {
            now.setFullYear(now.getFullYear() - 30);
            birth_date = travelcloud_1.dateToIsoDate(now);
        }
        else if (type === 'child') {
            now.setFullYear(now.getFullYear() - 6);
            birth_date = travelcloud_1.dateToIsoDate(now);
        }
        else if (type === 'infant') {
            now.setFullYear(now.getFullYear() - 1);
            birth_date = travelcloud_1.dateToIsoDate(now);
        }
        return {
            type: vw(type),
            first_name: vw("John" + String.fromCharCode('A'.charCodeAt(0) + index)),
            last_name: vw("Doe"),
            title: vw("Mr"),
            country: vw("SG"),
            passport: vw("S1234567A"),
            birth_date: vw(birth_date),
            expiry: vw("2025-12-25"),
        };
    });
    return {
        email: vw("demo@pytheas.travel"),
        phone: vw("12345678"),
        first_name: vw("John"),
        last_name: vw("Doe"),
        travelers: travelers
    };
};
exports.createCheckoutForm = antd_1.Form.create({
    onFieldsChange: function (props, changedFields) {
        if (props.onChange != null)
            props.onChange(changedFields);
    },
    mapPropsToFields: function (props) {
        var formState = props.formState;
        if (formState == null)
            return {};
        var fields = {
            email: antd_1.Form.createFormField(formState.email || { value: '' }),
            phone: antd_1.Form.createFormField(formState.phone || { value: '' }),
            first_name: antd_1.Form.createFormField(formState.first_name || { value: '' }),
            last_name: antd_1.Form.createFormField(formState.last_name || { value: '' }),
        };
        if (Array.isArray(formState.travelers)) {
            for (var k in formState.travelers) {
                fields["travelers[" + k + "].type"] = antd_1.Form.createFormField(formState.travelers[k].type || { value: '' });
                fields["travelers[" + k + "].first_name"] = antd_1.Form.createFormField(formState.travelers[k].first_name || { value: '' });
                fields["travelers[" + k + "].last_name"] = antd_1.Form.createFormField(formState.travelers[k].last_name || { value: '' });
                fields["travelers[" + k + "].title"] = antd_1.Form.createFormField(formState.travelers[k].title || { value: '' });
                fields["travelers[" + k + "].country"] = antd_1.Form.createFormField(formState.travelers[k].country || { value: '' });
                fields["travelers[" + k + "].passport"] = antd_1.Form.createFormField(formState.travelers[k].passport || { value: '' });
                fields["travelers[" + k + "].birth_date"] = antd_1.Form.createFormField(formState.travelers[k].birth_date || { value: '' });
                fields["travelers[" + k + "].expiry"] = antd_1.Form.createFormField(formState.travelers[k].expiry || { value: '' });
            }
        }
        return fields;
    }
});
var CheckoutFormComponent = /** @class */ (function (_super) {
    __extends(CheckoutFormComponent, _super);
    function CheckoutFormComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CheckoutFormComponent.prototype.render = function () {
        var _this = this;
        var _a = this.props, form = _a.form, formState = _a.formState;
        var getFieldDecorator = form.getFieldDecorator;
        var formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        };
        if (formState == null)
            return null;
        return (react_1.default.createElement(antd_1.Form, { layout: "horizontal" },
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "E-mail" }), getFieldDecorator('email', {
                rules: [{
                        type: 'email', message: 'E-mail is not valid',
                    }, {
                        required: true, message: 'Please input your E-mail!',
                    }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Phone Number" }), getFieldDecorator('phone', {
                rules: [{ required: true, message: 'Please input your phone number!' }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "First name" }), getFieldDecorator('first_name', {
                rules: [{ required: true, message: 'Please input your first name!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Last name" }), getFieldDecorator('last_name', {
                rules: [{ required: true, message: 'Please input your last name!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input, null))),
            formState.travelers.map(function (value, index) {
                return react_1.default.createElement(TravelerFormComponent, { key: index, form: _this.props.form, index: index });
            })));
    };
    return CheckoutFormComponent;
}(react_1.default.Component));
exports.CheckoutForm = exports.createCheckoutForm(CheckoutFormComponent);
var TravelerFormComponent = /** @class */ (function (_super) {
    __extends(TravelerFormComponent, _super);
    function TravelerFormComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TravelerFormComponent.prototype.render = function () {
        var getFieldDecorator = this.props.form.getFieldDecorator;
        var formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        };
        var type = this.props.form.getFieldValue("travelers[" + this.props.index + "].type");
        var minBirthDate = '1901-01-01';
        if (type === 'child') {
            var tmpDate = new Date();
            tmpDate.setFullYear(tmpDate.getFullYear() - 12);
            minBirthDate = travelcloud_1.dateToIsoDate(tmpDate);
        }
        else if (type === 'infant') {
            var tmpDate = new Date();
            tmpDate.setFullYear(tmpDate.getFullYear() - 2);
            minBirthDate = travelcloud_1.dateToIsoDate(tmpDate);
        }
        var tmpDate2 = new Date();
        tmpDate2.setMonth(tmpDate2.getMonth() + 6);
        var minPassportExpiry = travelcloud_1.dateToIsoDate(tmpDate2);
        tmpDate2.setFullYear(tmpDate2.getFullYear() + 20);
        var maxPassportExpiry = travelcloud_1.dateToIsoDate(tmpDate2);
        return react_1.default.createElement("div", null,
            react_1.default.createElement("h1", null,
                "Traveler ",
                this.props.index + 1,
                " ",
                type !== 'adult' && ("(" + type + ")")),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "First name" }), getFieldDecorator("travelers[" + this.props.index + "].first_name", {
                rules: [{ required: true, message: 'Please input your first name!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Last name" }), getFieldDecorator("travelers[" + this.props.index + "].last_name", {
                rules: [{ required: true, message: 'Please input your last name!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({ label: "Gender" }, formItemLayout), getFieldDecorator("travelers[" + this.props.index + "].title")(react_1.default.createElement(antd_1.Radio.Group, null,
                react_1.default.createElement(antd_1.Radio.Button, { value: "Mr" }, "Male"),
                react_1.default.createElement(antd_1.Radio.Button, { value: "Ms" }, "Female")))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Date of birth" }),
                react_1.default.createElement(DropdownDatePickerInputWrapper, { min: minBirthDate, form: this.props.form, name: "travelers[" + this.props.index + "].birth_date" })),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Nationality" }),
                react_1.default.createElement(country_dropdown_1.CountryDropdown, { form: this.props.form, name: "travelers[" + this.props.index + "].country" })),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Passport number" }), getFieldDecorator("travelers[" + this.props.index + "].passport", {
                rules: [{ required: true, message: 'Please input your passport number!', whitespace: true }],
            })(react_1.default.createElement(antd_1.Input, null))),
            react_1.default.createElement(antd_1.Form.Item, __assign({}, formItemLayout, { label: "Passport expiry" }),
                react_1.default.createElement(DropdownDatePickerInputWrapper, { min: minPassportExpiry, max: maxPassportExpiry, form: this.props.form, name: "travelers[" + this.props.index + "].expiry" })));
    };
    return TravelerFormComponent;
}(react_1.default.Component));
// only for parsing iso format string
function parseDateString(str) {
    var result = new Date(str);
    if (isNaN(result.getTime()) == false)
        return result;
    return new Date();
}
function getLastDayOfMonth(valDate) {
    var valDateClone = new Date(valDate.getTime());
    valDateClone.setMonth(valDateClone.getMonth() + 1);
    valDateClone.setDate(0);
    return valDateClone.getDate();
}
var DropdownDatePickerInputWrapper = function (props) {
    var getFieldDecorator = props.form.getFieldDecorator;
    var val = props.form.getFieldValue(props.name);
    var valDate = parseDateString(val);
    var minDate = parseDateString(props.min);
    var maxDate = parseDateString(props.max);
    // we only clip the year input
    // actual min/max should be validated elsewher
    // this is to avoid changing the user's input as he is entering
    if (valDate.getFullYear() < minDate.getFullYear())
        valDate.setFullYear(minDate.getFullYear());
    if (valDate.getFullYear() > maxDate.getFullYear())
        valDate.setFullYear(maxDate.getFullYear());
    var validYears = Array(maxDate.getFullYear() - minDate.getFullYear() + 1).fill(null).map(function (_, index) { return minDate.getFullYear() + index; });
    var maxValidDate = getLastDayOfMonth(valDate);
    var validDays = Array(maxValidDate).fill(null).map(function (_, index) { return index + 1; });
    var year = valDate.getFullYear();
    var month = valDate.getMonth();
    var day = valDate.getDate();
    var yearChanged = function (updated) {
        var _a;
        valDate.setFullYear(updated);
        if (valDate.getDate() != day) {
            valDate.setMonth(month);
            valDate.setDate(getLastDayOfMonth(valDate));
        }
        props.form.setFieldsValue((_a = {},
            _a[props.name] = travelcloud_1.dateToIsoDate(valDate),
            _a));
    };
    var monthChanged = function (updated) {
        var _a;
        valDate.setMonth(updated);
        if (valDate.getDate() != day) {
            valDate.setMonth(updated);
            valDate.setDate(getLastDayOfMonth(valDate));
        }
        props.form.setFieldsValue((_a = {},
            _a[props.name] = travelcloud_1.dateToIsoDate(valDate),
            _a));
    };
    // assume user fill year -> month -> day
    // we don't try to clip invalid day, just overflow
    // also, invalid days shouldn't show in dropdown
    var dayChanged = function (updated) {
        var _a;
        valDate.setDate(updated);
        props.form.setFieldsValue((_a = {},
            _a[props.name] = travelcloud_1.dateToIsoDate(valDate),
            _a));
    };
    getFieldDecorator(props.name);
    return react_1.default.createElement("div", null,
        react_1.default.createElement(antd_1.Select, { showSearch: true, value: year, onChange: yearChanged, style: { marginRight: '2%', width: '25%' } }, validYears.map(function (validYear) { return react_1.default.createElement(antd_1.Select.Option, { key: String(validYear), value: validYear }, validYear); })),
        react_1.default.createElement(antd_1.Select, { showSearch: true, value: month, onChange: monthChanged, style: { marginRight: '2%', width: '50%' }, filterOption: function (input, option) { return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                || option.props.value + 1 == parseInt(input); } },
            react_1.default.createElement(antd_1.Select.Option, { key: String(0), value: 0 }, "January"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(1), value: 1 }, "February"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(2), value: 2 }, "March"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(3), value: 3 }, "April"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(4), value: 4 }, "May"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(5), value: 5 }, "June"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(6), value: 6 }, "July"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(7), value: 7 }, "August"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(8), value: 8 }, "September"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(9), value: 9 }, "October"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(10), value: 10 }, "November"),
            react_1.default.createElement(antd_1.Select.Option, { key: String(11), value: 11 }, "December")),
        react_1.default.createElement(antd_1.Select, { showSearch: true, value: day, onChange: dayChanged, style: { width: '20%' } }, validDays.map(function (validDay) { return react_1.default.createElement(antd_1.Select.Option, { key: String(validDay), value: validDay }, validDay); })));
};
//# sourceMappingURL=checkout.js.map