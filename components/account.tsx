import React from 'react'
import { Icon, Form, Input, Button, Avatar, Tabs, Card, Tag, Table, Spin } from 'antd'
import moment from 'moment';
import  Big from 'big.js'
import { Cart } from '../travelcloud'

export class Account extends React.Component<{
  customer: any
  cart: Cart
}> {

  state = {
    loginLoading: false,
    loginRequested: 0,
    loginVerifyStatus: {},
    loginRefreshing: false,
    loginEmail: '',
    loginVerify: '',
  }

  async getPassword() {
    this.setState({
      loginLoading: true
    })
    await this.props.cart.getPassword(this.state.loginEmail)
    this.setState({
      loginLoading: false,
      loginRequested: Date.now()
    })
  }

  async logout() {
    this.props.cart.logout()
  }

  async login() {
    this.setState({
      loginLoading: true,
      loginVerifyStatus: {}
    })
    const customer = await this.props.cart.login(this.state.loginEmail, this.state.loginVerify)
    if (customer.result == null) {
      this.setState({
        loginLoading: false,
        loginVerify: "",
        loginVerifyStatus: {
          validateStatus:"error",
          help:"Invalid password"
        }
      })
    } else {
      this.setState({
        loginLoading: false
      })
    }
  }

  render() {
    return (
      <div>
        <div style={{fontSize: 24, padding: "24px 0"}}>My Account {this.props.customer.result != null && <span style={{color: '#999', fontSize: 14, marginLeft: 16, cursor: 'pointer'}} onClick={() => this.logout()}><Icon type="logout" /> Logout</span>}</div>
        {this.props.customer.result == null && this.state.loginLoading == true
          &&<div>
            <Spin size="large" style={{display: 'block', margin: '64px auto'}} />
          </div>
        }
        {this.props.customer.result == null && this.state.loginLoading == false
          && (Date.now() - this.state.loginRequested  > 600000
              ? <div>
                  <div>We will send a temporary password to your email</div>
                  <Form>
                    <Form.Item>
                      <Input placeholder="E-mail" onChange={(e) => this.setState({loginEmail: e.target.value})} onPressEnter={() => this.getPassword()} />
                    </Form.Item>
                    <Form.Item>
                      <Button size="large" type="primary" disabled={this.state.loginLoading} onClick={() => this.getPassword()}><Icon type={this.state.loginLoading ? "loading" : "mail"} /> Send email</Button>
                    </Form.Item>
                  </Form>
                </div>
              : <div>
                  <div>Please check your email for your password</div>
                  <Form>
                    <Form.Item>
                      <Input disabled value={this.state.loginEmail} />
                    </Form.Item>
                    <Form.Item {...this.state.loginVerifyStatus}>
                      <Input placeholder="password" onChange={(e) => this.setState({loginVerify: e.target.value})} onPressEnter={() => this.login()}/>
                    </Form.Item>
                    <Form.Item>
                      <Button size="large" type="primary" disabled={this.state.loginLoading} onClick={() => this.login()}><Icon type={this.state.loginLoading ? "loading" : "login"} /> Login</Button>
                    </Form.Item>
                  </Form>
                </div>)}
        {this.props.customer.result != null
            &&<div>
              <Card
                style={{border: 0}}>
                <Card.Meta
                  avatar={<Avatar size="large" icon="user" style={{backgroundColor: "#af0201"}} />}
                  title={<span>
                    <span style={{marginRight: 8}}>{this.props.customer.result.name
                      ? this.props.customer.result.name
                      : this.props.customer.result.email}</span>
                    {this.props.customer.result.categories.map(cat => <Tag key={cat.id}>{cat.name}</Tag>)}
                    </span>}
                  description={<span>Account balance ${this.props.customer.result.payments.reduce((acc, payment) => acc.plus(payment.amount), Big(0)).times(-1).toFixed(2).toString()}</span>} />
              </Card>
              <Tabs defaultActiveKey="1">
                <Tabs.TabPane tab="Orders" key="1">
                <Table
                  columns={[{
                      title: 'Date',
                      render: (x) => <span>{moment(x.order_date).format('Do MMMM YYYY')}</span>,
                      sorter: (x, y) => parseInt((x as any).id) - parseInt((y as any).id),
                      sortOrder: 'descend',
                      key: 'order_date',
                    }, {
                      title: 'Ref',
                      render: (x) => <a href={"/payment?ref=" + x.ref}>{x.ref}</a>,
                      key: 'ref',
                    }, {
                      title: 'Amount',
                      dataIndex: 'payment_required',
                      key: 'payment_required',
                    }, {
                      title: 'Status',
                      dataIndex: 'order_status',
                      key: 'order_status',
                    }, {
                      title: 'Payment',
                      dataIndex: 'payment_status',
                      key: 'payment_status',
                    }]}
                  dataSource={this.props.customer.result.orders}
                  rowKey="id" />
                </Tabs.TabPane>
                <Tabs.TabPane tab="Statement" key="2"><Table
                columns={[{
                    title: 'Date',
                    render: (x) => <span>{moment(x.date_added).format('Do MMMM YYYY')}</span>,
                    sorter: (x, y) => parseInt((x as any).id) - parseInt((y as any).id),
                    sortOrder: 'descend',
                    key: 'date_added',
                  }, {
                    title: 'Ref',
                    render: (x) => x.order == null ? 'N/A' : <a href={"/payment?ref=" + x.order.ref}>{x.order.ref}</a>,
                    key: 'order.ref',
                  }, {
                    title: 'Description',
                    dataIndex: 'description',
                    key: 'description',
                  }, {
                    title: 'Amount',
                    dataIndex: 'amount',
                    key: 'amount',
                  }]}
                  dataSource={this.props.customer.result.payments}
                  rowKey="id" /></Tabs.TabPane>
              </Tabs>
          </div>}
    </div>
    )
  }
}