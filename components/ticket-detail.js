"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
/* Use ANTD Modal */
var TicketModal = function (_a) {
    var data = _a.data, handleClose = _a.handleClose, show = _a.show;
    return (react_1.default.createElement(antd_1.Modal, { title: "Ticket Detail", visible: show, onCancel: handleClose, width: 800, footer: null },
        react_1.default.createElement("h3", { className: "redHeading" }, data.name),
        react_1.default.createElement("p", null, data.description),
        data.variation ? react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "Type: "),
            " ",
            react_1.default.createElement("br", null),
            data.variation.name) : '',
        react_1.default.createElement("p", { className: "tickets-details-terms" },
            react_1.default.createElement("strong", null, "Terms & Condition: "),
            react_1.default.createElement("br", null),
            data.termsAndConditions),
        react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "Product Id: "),
            react_1.default.createElement("br", null),
            data.id),
        react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "SKU Id: "),
            react_1.default.createElement("br", null),
            data.SKU),
        react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "Merchant: "),
            react_1.default.createElement("br", null),
            data.merchantProductCode),
        react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "validity: "),
            react_1.default.createElement("br", null),
            data.issuanceLimit),
        react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "Original Price: "),
            react_1.default.createElement("br", null),
            data.currency + ' ' + parseFloat(data.originalPrice).toFixed(2)),
        react_1.default.createElement("p", null,
            react_1.default.createElement("strong", null, "Settlement Price: "),
            react_1.default.createElement("br", null),
            data.currency + ' ' + parseFloat(data.settlementRate).toFixed(2))));
};
exports.default = TicketModal;
//# sourceMappingURL=ticket-detail.js.map