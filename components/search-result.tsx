import React from "react";
import { Collapse, Icon, Row, Col, Button, List, Avatar } from "antd";
import { IncDecInput } from "./inc-dec-input";
import Router from 'next/router'

const Panel = Collapse.Panel;

export const SearchResult: React.StatelessComponent<{
  offset: number;
  limit: number;
  data: any;
}> = ({ offset, limit, data }) => {
  return (
    <div key={"main-container"} className="search-result">
      {data == null || data.length === 0 ? (
        <h4>There is no record found!</h4>
      ) : (
          data.map((item, index) => {
            if (index >= offset && index <= limit) {
              return (
                <List
                  size="large"
                  itemLayout="horizontal"
                  dataSource={data}
                  className="main-collaspe-relative"
                  renderItem={item => (
                    <List.Item>
                      <List.Item.Meta
                        avatar={<Avatar src={
                                     "https://uat-api.globaltix.com/api/image?name=" +item.imagePath} />}
                        title={<a onClick={() => Router.push({
                          pathname: '/globaltix-details',
                          query: {'result': item.id },
                        })}>{item.title}</a>}
                      />
                    </List.Item>
                  )}
                />
              );
            }
          })
        )}
    </div>
  );
};

