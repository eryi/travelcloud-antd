"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var travelcloud_1 = require("../travelcloud");
var antd_1 = require("antd");
var moment_1 = __importDefault(require("moment"));
var no_result_1 = require("./no-result");
var big_js_1 = __importDefault(require("big.js"));
function ignoreTimezoneExtractTime(datetime) {
    return moment_1.default(datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/, '')).format('h:mm:ss a');
}
function ignoreTimezoneExtractDate(datetime) {
    return moment_1.default(datetime.replace(/[+-][0-9][0-9]:[0-9][0-9]$/, '')).format('Do MMMM[,] YYYY');
}
function computePrice(cart, flight, fareId) {
    var fare = flight.fares.find(function (fare) { return fare.id == fareId; });
    if (fare == null) {
        return {
            afterPriceRules: "Invalid fare id",
            beforePriceRules: "Invalid fare id"
        };
    }
    if (cart == null) {
        return {
            afterPriceRules: travelcloud_1.formatCurrency(fare.price),
            beforePriceRules: travelcloud_1.formatCurrency(fare.price)
        };
    }
    var product = cart.addFlight(flight, fareId, true);
    if (product == null) {
        return {
            afterPriceRules: travelcloud_1.formatCurrency(fare.price),
            beforePriceRules: travelcloud_1.formatCurrency(fare.price)
        };
    }
    // more like "before discounts" and "after discounts"
    var beforePriceRules = product.items.filter(function (item) { return parseInt(item['price']) > 0; }).reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0));
    var afterPriceRules = product.items.reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0));
    return {
        afterPriceRules: travelcloud_1.formatCurrency(afterPriceRules),
        beforePriceRules: travelcloud_1.formatCurrency(beforePriceRules)
    };
}
function renderFares(flightIndex, cart, flight, onFareClick) {
    var indexServices = function (acc2, fare, fareDetailIndex) {
        if (acc2[fareDetailIndex] == null)
            acc2[fareDetailIndex] = {
                origin: fare.origin,
                destination: fare.destination,
                servicesSet: {}
            };
        var brand = fare.brand;
        if (brand == null)
            return acc2;
        var services = brand.services;
        if (services == null)
            return acc2;
        acc2[fareDetailIndex].servicesSet = services.reduce(function (acc3, service) {
            acc3[service.name] = true;
            return acc3;
        }, acc2[fareDetailIndex].servicesSet);
        return acc2;
    };
    var fares = flight.fares;
    var fareServicesIndexed = fares.reduce(function (acc, fare) {
        return fare.ptc_fare_detail_map.adt.reduce(indexServices, acc);
    }, new Array(fares[0].ptc_fare_detail_map.adt.length));
    fareServicesIndexed = fareServicesIndexed.map(function (x) {
        x.servicesSet = Object.keys(x.servicesSet);
        return x;
    });
    var cellStyle = { height: 50, borderBottom: "1px solid #aaa", lineHeight: '50px', paddingLeft: 10, overflow: 'hidden' };
    var headerStyle = { fontSize: 16, backgroundColor: '#fed', fontWeight: 'bold' };
    return react_1.default.createElement("div", { style: { margin: "30px" } },
        react_1.default.createElement("div", { style: { display: 'flex', flexDirection: 'row', justifyContent: 'center' } },
            react_1.default.createElement("div", { style: { display: 'block', width: 200, margin: '0 10px' } },
                react_1.default.createElement("div", { style: { height: 122, lineHeight: "122px", fontSize: 20, color: '#666', textAlign: 'center' } }),
                fareServicesIndexed.map(function (info, index) {
                    return react_1.default.createElement("div", { key: index },
                        react_1.default.createElement("div", { style: __assign({}, cellStyle, headerStyle) },
                            info.origin.code,
                            " \u2192 ",
                            info.destination.code),
                        info.servicesSet.map(function (serviceName) { return react_1.default.createElement("div", { key: serviceName, style: cellStyle }, serviceName); }));
                })),
            fares.map(function (detail, i1) {
                var computed = computePrice(cart, flight, detail.id);
                return react_1.default.createElement("div", { key: i1, style: { width: 200, border: "2px solid #eee", borderRadius: 5, margin: '0 10px' } },
                    react_1.default.createElement("div", { style: { height: 120, textAlign: 'center', padding: 15 } },
                        react_1.default.createElement("div", { style: { fontSize: 20, color: '#666' } }, computed.afterPriceRules),
                        react_1.default.createElement("div", { style: { margin: 10 } },
                            react_1.default.createElement(antd_1.Button, { type: "primary", onClick: function () { return onFareClick(flightIndex, flight, detail.id); } }, "Select"))),
                    detail.ptc_fare_detail_map.adt.map(function (fare, i2) {
                        var servicesIndexed = fare.brand == null || fare.brand.services == null ? {} : fare.brand.services.reduce(function (acc, service) {
                            acc[service.name] = service;
                            return acc;
                        }, {});
                        return react_1.default.createElement("div", { key: i2 },
                            react_1.default.createElement("div", { style: __assign({}, cellStyle, headerStyle) }, fare.brand != null && fare.brand.name != null ? fare.brand.name : fare.cabin),
                            fareServicesIndexed[i2].servicesSet.map(function (serviceName, index) {
                                return react_1.default.createElement("div", { key: index, style: __assign({}, cellStyle, { textAlign: 'center', fontSize: 28 }) }, servicesIndexed[serviceName] != null && servicesIndexed[serviceName].included === true ? react_1.default.createElement(antd_1.Icon, { style: { color: '#7C7' }, type: "check" }) : react_1.default.createElement(antd_1.Icon, { style: { color: '#C77' }, type: "close" }));
                            }));
                    }));
            })));
}
exports.FlightsResult = function (_a) {
    var flights = _a.flights, flightMap = _a.flightMap, onFlightClick = _a.onFlightClick, onFareClick = _a.onFareClick, cart = _a.cart;
    return react_1.default.createElement(react_1.default.Fragment, null, flights.map(function (flight, index) {
        var departingFlight = flight.od1;
        var departingFlightFirstSegment = departingFlight.segments[0];
        var departingFlightLastSegment = departingFlight.segments[departingFlight.segments.length - 1];
        var departingFlightFirstLeg = departingFlightFirstSegment.flights[0];
        var departingFlightLastLeg = departingFlightLastSegment.flights[departingFlightLastSegment.flights.length - 1];
        //const departingFlightAdtFirstFare = flight.od1_ptc_fare_map.adt[0]
        //const departingFlightHasBaggage = departingFlightAdtFirstFare.baggage_pieces != null || departingFlightAdtFirstFare.baggage_weight_amount != null
        var returningFlight = flight.od2;
        var returningFlightFirstSegment = returningFlight && returningFlight.segments[0];
        var returningFlightLastSegment = returningFlight && returningFlight.segments[returningFlight.segments.length - 1];
        var returningFlightFirstLeg = returningFlight && returningFlightFirstSegment.flights[0];
        var returningFlightLastLeg = returningFlight && returningFlightLastSegment.flights[returningFlightLastSegment.flights.length - 1];
        //const returningFlightAdtFirstFare = returningFlight && flight.od2_ptc_fare_map.adt[0]
        //const returningFlightHasBaggage = returningFlight && (returningFlightAdtFirstFare.baggage_pieces != null || returningFlightAdtFirstFare.baggage_weight_amount != null)
        var computed = computePrice(cart, flight, flight.fares[0].id);
        return react_1.default.createElement("div", { className: "ant-card", style: { padding: 16, border: '1px solid #e8e8e8' }, key: index },
            react_1.default.createElement("table", { style: { width: '100%' } },
                react_1.default.createElement("tbody", null,
                    react_1.default.createElement("tr", null,
                        react_1.default.createElement("td", { style: { width: '70%' } },
                            react_1.default.createElement("table", { style: { width: '100%' } },
                                react_1.default.createElement("tbody", null,
                                    react_1.default.createElement("tr", null,
                                        react_1.default.createElement("th", { colSpan: 3 }, "Departing flight")),
                                    react_1.default.createElement("tr", null,
                                        react_1.default.createElement("td", { style: { width: '30%' } },
                                            react_1.default.createElement("img", { src: "//cdn.net.in/static/airlines/" + departingFlightFirstSegment.marketing_carrier.code + ".gif" })),
                                        react_1.default.createElement("td", { style: { width: '30%' } },
                                            react_1.default.createElement("div", null,
                                                react_1.default.createElement("span", null, departingFlightFirstLeg.departure_airport.code),
                                                react_1.default.createElement("span", null, " \u2192")),
                                            react_1.default.createElement("div", null, ignoreTimezoneExtractTime(departingFlightFirstLeg.departure_datetime)),
                                            react_1.default.createElement("div", null, ignoreTimezoneExtractDate(departingFlightFirstLeg.departure_datetime))),
                                        react_1.default.createElement("td", { style: { width: '30%' } },
                                            react_1.default.createElement("div", null,
                                                react_1.default.createElement("span", null, departingFlightLastLeg.arrival_airport.code)),
                                            react_1.default.createElement("div", null, ignoreTimezoneExtractTime(departingFlightLastLeg.arrival_datetime)),
                                            react_1.default.createElement("div", null, ignoreTimezoneExtractDate(departingFlightLastLeg.arrival_datetime)))),
                                    returningFlight
                                        && [react_1.default.createElement("tr", { key: "1" },
                                                react_1.default.createElement("th", { colSpan: 3 }, "Return flight")),
                                            react_1.default.createElement("tr", { key: "2" },
                                                react_1.default.createElement("td", { style: { width: '30%' } },
                                                    react_1.default.createElement("img", { src: "//cdn.net.in/static/airlines/" + returningFlightFirstSegment.marketing_carrier.code + ".gif" })),
                                                react_1.default.createElement("td", { style: { width: '30%' } },
                                                    react_1.default.createElement("div", null,
                                                        react_1.default.createElement("span", null, returningFlightFirstLeg.departure_airport.code),
                                                        react_1.default.createElement("span", null, " \u2192")),
                                                    react_1.default.createElement("div", null, ignoreTimezoneExtractTime(returningFlightFirstLeg.departure_datetime)),
                                                    react_1.default.createElement("div", null, ignoreTimezoneExtractDate(returningFlightFirstLeg.departure_datetime))),
                                                react_1.default.createElement("td", { style: { width: '30%' } },
                                                    react_1.default.createElement("div", null,
                                                        react_1.default.createElement("span", null, returningFlightLastLeg.arrival_airport.code)),
                                                    react_1.default.createElement("div", null, ignoreTimezoneExtractTime(returningFlightLastLeg.arrival_datetime)),
                                                    react_1.default.createElement("div", null, ignoreTimezoneExtractDate(returningFlightLastLeg.arrival_datetime))))]))),
                        react_1.default.createElement("td", { style: { width: '30%' } },
                            react_1.default.createElement("div", { style: { margin: "20px 10px", fontSize: 16 } }, computed.beforePriceRules === computed.afterPriceRules
                                ? computed.beforePriceRules
                                : react_1.default.createElement(react_1.default.Fragment, null,
                                    react_1.default.createElement("span", { style: { textDecoration: 'line-through' } }, computed.beforePriceRules),
                                    "\u00A0",
                                    react_1.default.createElement("b", null, computed.afterPriceRules))),
                            react_1.default.createElement(antd_1.Button, { type: "primary", disabled: flightMap[index] != null, onClick: function () { return onFlightClick(index, flight); } }, "Select flight"))),
                    flightMap[index] != null
                        &&
                            react_1.default.createElement("tr", null,
                                react_1.default.createElement("td", { colSpan: 4, style: { height: 400 } }, flightMap[index].result == null
                                    ? react_1.default.createElement(no_result_1.NoResult, { response: flightMap[index], type: "fares", loadingMessage: "Verifying prices and availability..." })
                                    : renderFares(index, cart, flightMap[index].result, onFareClick))))));
    }));
};
//# sourceMappingURL=flights-result.js.map