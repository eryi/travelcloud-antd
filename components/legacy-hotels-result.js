"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var travelcloud_1 = require("../travelcloud");
exports.LegacyHotelsResult = function (_a) {
    var hotels = _a.hotels, onHotelClick = _a.onHotelClick;
    return react_1.default.createElement(antd_1.List, { dataSource: hotels, renderItem: function (hotel) { return (react_1.default.createElement(antd_1.List.Item, { key: hotel.hotelId },
            react_1.default.createElement(antd_1.Row, { style: { width: '100%' } },
                react_1.default.createElement(antd_1.Col, { span: 8, style: { height: 350 } },
                    react_1.default.createElement("div", { style: {
                            backgroundImage: "url(" + (hotel.images[0] ? hotel.images[0].image : '') + ")",
                            height: '100%',
                            width: '100%',
                            backgroundSize: 'cover',
                            backgroundPosition: 'center'
                        } })),
                react_1.default.createElement(antd_1.Col, { span: 16, style: { height: 350, padding: "16px 32px" } },
                    react_1.default.createElement("h2", null,
                        hotel.name,
                        react_1.default.createElement(antd_1.Rate, { style: { paddingLeft: 16, lineHeight: '2.5em' }, value: parseInt(hotel.rating.charAt(0)) })),
                    react_1.default.createElement(antd_1.Button, { type: "primary", style: { position: 'absolute', right: 36, top: 24 }, onClick: function () { return onHotelClick(hotel); } },
                        "More details ",
                        react_1.default.createElement(antd_1.Icon, { type: "right" })),
                    react_1.default.createElement("div", null, hotel.description != null && (hotel.description.substr(0, 350) + (hotel.description.length > 350 ? "..." : ""))),
                    react_1.default.createElement("table", { style: { tableLayout: "fixed", width: '100%', marginTop: 16 } },
                        react_1.default.createElement("tbody", null,
                            react_1.default.createElement("tr", null,
                                react_1.default.createElement("th", { style: { width: '40%' } }, "Rooms"),
                                react_1.default.createElement("th", { style: { width: '40%' } }),
                                react_1.default.createElement("th", { style: { width: '10%' } }),
                                react_1.default.createElement("th", { style: { width: '10%' } })),
                            hotel.rooms.filter(function (room) { return room.prices.length > 0; }).slice(0, 5).map(function (room) {
                                return react_1.default.createElement("tr", { key: room.id },
                                    react_1.default.createElement("td", null, room.description),
                                    react_1.default.createElement("td", null, room.prices[0].meal),
                                    react_1.default.createElement("td", null,
                                        react_1.default.createElement("b", null, travelcloud_1.formatCurrency(room.prices[0].price))));
                            }))))))); } });
};
//# sourceMappingURL=legacy-hotels-result.js.map