"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var calendar_1 = __importDefault(require("../components/calendar"));
var travelcloud_1 = require("../travelcloud");
var big_js_1 = __importDefault(require("big.js"));
var inc_dec_input_1 = require("./inc-dec-input");
var order_product_compute_1 = require("../order-product-compute");
exports.initialTourBookingForm = function (tour_id, option_id, departure_date, rooms) {
    if (option_id === void 0) { option_id = ''; }
    if (departure_date === void 0) { departure_date = ''; }
    if (rooms === void 0) { rooms = [{ adult: 1 }]; }
    return ({
        tour_id: tour_id,
        option_id: option_id,
        departure_date: departure_date,
        rooms: rooms
    });
};
exports.createTourBookingForm = function (WrappedComponent) {
    return /** @class */ (function (_super) {
        __extends(class_1, _super);
        function class_1() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        class_1.prototype.render = function () {
            var _a = this.props, _b = _a.value, value = _b === void 0 ? {} : _b, tour = _a.tour, cart = _a.cart, _c = _a.onSubmit, onSubmit = _c === void 0 ? function () { } : _c, _d = _a.onChange, onChange = _d === void 0 ? function () { } : _d, _e = _a.priceComputationRooms, priceComputationRooms = _e === void 0 ? [{
                    adult: 2
                }] : _e;
            if (tour != null && value.tour_id !== tour.id)
                value.tour_id = tour.id;
            var selectedOption = null;
            var selectedDeparture = null;
            var option_id = value.option_id;
            var departure_date = value.departure_date;
            if (option_id !== '') {
                for (var i in tour.options) {
                    if (tour.options[i].id === option_id)
                        selectedOption = tour.options[i];
                }
            }
            if (selectedOption != null && departure_date !== '') {
                for (var i in selectedOption.departures) {
                    if (selectedOption.departures[i].id === departure_date)
                        selectedDeparture = selectedOption.departures[i];
                }
            }
            var priceComputationDivisor = priceComputationRooms.reduce(function (acc, room) {
                return Object.entries(room).reduce(function (acc2, kvp) { return acc2 + parseInt(kvp[1]); }, acc);
            }, 0);
            var product = cart == null
                ? order_product_compute_1.computeTour(tour, value).product
                : cart.addTour(tour, value, true);
            var totalPrice = product == null
                ? null
                : product.items
                    .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0));
            var totalDeposit = product == null
                ? null
                : product.items
                    .reduce(function (acc, item) { return acc.add(big_js_1.default(item.deposit).times(item.quantity)); }, big_js_1.default(0));
            var invoiceItemsGrouped = product == null ? {} : product.items.reduce(function (acc, line) {
                if (acc[line.sub_header] == null)
                    acc[line.sub_header] = [];
                acc[line.sub_header].push(line);
                return acc;
            }, {});
            // not sure how to enable Object.entries in babel
            var invoiceEntries = [];
            for (var i in invoiceItemsGrouped) {
                invoiceEntries.push([i, invoiceItemsGrouped[i]]);
            }
            var allowRooms = tour.price_type === 'ALL' || tour.price_type.indexOf('SGL') !== -1;
            var tourOptionsWithCheapestComputed;
            if (cart != null && tour != null) {
                tourOptionsWithCheapestComputed = tour.options.map(function (option) {
                    option = Object.assign({}, option);
                    option['_cheapest_computed'] =
                        travelcloud_1.mapTourOptionDepartures(option, function (departure, departureDate) {
                            if (departure != null && departure.slots_taken >= departure.slots_total)
                                return null;
                            var product = cart.addTour(tour, {
                                tour_id: tour.id,
                                option_id: option.id,
                                departure_date: departureDate,
                                rooms: priceComputationRooms
                            }, true);
                            if (product == null)
                                return null;
                            var beforeDiscounts = product.items
                                .filter(function (item) { return parseInt(item['price']) > 0; })
                                .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                                .div(priceComputationDivisor);
                            var afterDiscounts = product.items
                                .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                                .div(priceComputationDivisor);
                            var afterDiscountsDeposit = product.items
                                .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                                .div(priceComputationDivisor);
                            return {
                                beforeDiscounts: beforeDiscounts, afterDiscounts: afterDiscounts, afterDiscountsDeposit: afterDiscountsDeposit, product: product, departureDate: departureDate
                            };
                        })
                            // filter out invalid departures
                            .filter(function (val) { return val != null; })
                            // find the cheapest
                            .reduce(function (acc, computed) {
                            if (acc == null || computed.afterDiscounts < acc.afterDiscounts) {
                                return computed;
                            }
                            return acc;
                        }, null);
                    return option;
                });
            }
            var selectedOptionDateInfoMap = {};
            if (selectedOption != null && cart != null && tour != null) {
                var runningDate = new Date(selectedOption['_next_departure']);
                var endDate = new Date(selectedOption['_last_departure']);
                var departure_datesIndexed = selectedOption.departures.reduce(function (acc, departure) {
                    acc[departure.date] = departure;
                    return acc;
                }, {});
                while (endDate >= runningDate) {
                    var isoDate = travelcloud_1.dateToIsoDate(runningDate);
                    var departure = departure_datesIndexed[isoDate];
                    // no departure or departure full
                    var disabled = selectedOption.on_demand_advance_booking === '0'
                        ? (departure == null || parseInt(departure.slots_taken) >= parseInt(departure.slots_total))
                        : (isoDate < selectedOption['_next_departure'] || isoDate > selectedOption['_last_departure']);
                    var noDeparture = (selectedOption.on_demand_advance_booking === '0' && departure == null) || (selectedOption.on_demand_advance_booking !== '0' && disabled);
                    var product_1 = cart.addTour(tour, {
                        tour_id: tour.id,
                        option_id: selectedOption.id,
                        departure_date: isoDate,
                        rooms: priceComputationRooms
                    }, true);
                    var beforeDiscounts = product_1.items
                        .filter(function (item) { return parseInt(item['price']) > 0; })
                        .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                        .div(priceComputationDivisor);
                    var afterDiscounts = product_1.items
                        .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                        .div(priceComputationDivisor);
                    var slotsRemaining = selectedOption.on_demand_advance_booking === '0' && departure != null
                        ? departure.slots_total - departure.slots_taken
                        : null;
                    selectedOptionDateInfoMap[isoDate] = { noDeparture: noDeparture, disabled: disabled, date: new Date(runningDate.getTime()), afterDiscounts: afterDiscounts, beforeDiscounts: beforeDiscounts, slotsRemaining: slotsRemaining, product: product_1 };
                    runningDate.setDate(runningDate.getDate() + 1);
                }
            }
            var context = Object.assign({}, this.props, { tourOptionsWithCheapestComputed: tourOptionsWithCheapestComputed, tour: tour, value: value, onChange: onChange, onSubmit: onSubmit, cart: cart, selectedOptionDateInfoMap: selectedOptionDateInfoMap, allowRooms: allowRooms, selectedOption: selectedOption, totalPrice: totalPrice, totalDeposit: totalDeposit, invoiceEntries: invoiceEntries });
            return react_1.default.createElement(WrappedComponent, __assign({}, context));
        };
        return class_1;
    }(react_1.default.Component));
};
exports.TourBookingForm = exports.createTourBookingForm(/** @class */ (function (_super) {
    __extends(class_2, _super);
    function class_2() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            visible: false
        };
        _this.handleVisibleChange = function (visible) {
            _this.setState({ visible: visible });
        };
        _this.hide = function () {
            _this.setState({ visible: false });
        };
        return _this;
    }
    class_2.prototype.render = function () {
        var _a = this.props, tour = _a.tour, value = _a.value, onChange = _a.onChange, selectedOptionDateInfoMap = _a.selectedOptionDateInfoMap, allowRooms = _a.allowRooms, selectedOption = _a.selectedOption, totalPrice = _a.totalPrice, invoiceEntries = _a.invoiceEntries, tourOptionsWithCheapestComputed = _a.tourOptionsWithCheapestComputed;
        var stepStatus = function (stepNum, current) {
            if (current < stepNum)
                return 'wait';
            if (current > stepNum)
                return 'finish';
            return 'process';
        };
        if (tour == null || tour.options.length === 0) {
            return react_1.default.createElement("div", { style: { textAlign: 'center', padding: 64 } },
                "Departure details coming soon.",
                react_1.default.createElement("br", null),
                "Please check back later.");
        }
        // not sure why but the grid={gutter: 16} doesn't work on List
        // we pad List.Item manually here
        var roomList = [];
        var optionSelected = tour.options.find(function (option) { return option.id === value.option_id; });
        var departureSelected = optionSelected == null ? null : optionSelected.departures.find(function (departure) { return departure.date === value.departure_date; });
        var availableSlots = departureSelected == null ? null : parseInt(departureSelected.slots_total) - parseInt(departureSelected.slots_taken);
        var currentTotal = value.rooms.reduce(function (acc, room) {
            if (room.adult != null)
                acc += parseInt('' + room.adult);
            if (room.child_with_bed != null)
                acc += parseInt('' + room.child_with_bed);
            if (room.child_no_bed != null)
                acc += parseInt('' + room.child_no_bed);
            return acc;
        }, 0);
        var isFull = currentTotal >= availableSlots;
        if (optionSelected != null) {
            roomList = value.rooms.map(function (item, index) {
                return react_1.default.createElement(antd_1.List.Item, { style: { padding: 8 } },
                    react_1.default.createElement(exports.RoomForm, { tour: tour, value: item, index: index, isFull: isFull, numberOfSiblings: value.rooms.length, deleteSelf: function () {
                            value.rooms.splice(index, 1);
                            onChange(Object.assign({}, value));
                        }, onChange: function (formChanges) {
                            var changes = travelcloud_1.extractValueFromFormState(formChanges);
                            value.rooms[index] = Object.assign({}, value.rooms[index], changes);
                            onChange(Object.assign({}, value));
                        } }));
            });
            // there's no way to align height using List, so we generate a dummy RoomForm for the 'add room' button
            if (value.rooms.length < 4 && allowRooms)
                roomList.push(react_1.default.createElement(antd_1.List.Item, { style: { padding: 8 } },
                    react_1.default.createElement(exports.RoomForm, { className: "tc-blur-hide-inputs", tour: tour, title: "New room" }),
                    react_1.default.createElement(antd_1.Button, { type: "primary", style: { position: 'absolute', top: '50%', width: '70%', left: '15%' }, onClick: function () {
                            value.rooms.push({ adult: 1 });
                            onChange(Object.assign({}, value));
                        } }, "Add room")));
        }
        var currentStep;
        if (value.option_id == '')
            currentStep = 1;
        else if (value.departure_date == '')
            currentStep = 2;
        else
            currentStep = 3;
        return react_1.default.createElement(antd_1.Form, { layout: "horizontal", onSubmit: this.props.onSubmit, className: "tc-tour-form" },
            react_1.default.createElement("div", { className: "tc-with-border-bottom tc-steps-clickable" },
                react_1.default.createElement(antd_1.Steps, { size: "small", style: { padding: 32 } },
                    react_1.default.createElement(antd_1.Steps.Step, { status: stepStatus(1, currentStep), onClick: function () { return onChange(Object.assign({}, value, { option_id: '', departure_date: '' })); }, title: "Tour options", description: selectedOption != null ? selectedOption.name : '' }),
                    react_1.default.createElement(antd_1.Steps.Step, { status: stepStatus(2, currentStep), title: "Departure dates", onClick: function () { return onChange(Object.assign({}, value, { departure_date: '' })); }, description: value.departure_date }),
                    react_1.default.createElement(antd_1.Steps.Step, { status: stepStatus(3, currentStep), title: "Travellers" }))),
            currentStep === 1
                && react_1.default.createElement(antd_1.List, { size: "large", grid: { xs: 1, sm: 1, md: 1, lg: 2, xl: 2, xxl: 2 }, dataSource: tourOptionsWithCheapestComputed || tour.options, renderItem: function (option) {
                        return react_1.default.createElement(antd_1.List.Item, { key: option.id, style: { lineHeight: '24px', padding: '24px 30px' } },
                            react_1.default.createElement("h2", null, option.name),
                            option['_next_departure'] != null && react_1.default.createElement("div", null,
                                "Next departure on ",
                                react_1.default.createElement("b", null, option['_next_departure'])),
                            option['_cheapest_computed'] == null
                                ? react_1.default.createElement("div", null,
                                    "Prices from ",
                                    option['TWN'])
                                : (option['_cheapest_computed'].beforeDiscounts.eq(option['_cheapest_computed'].afterDiscounts)
                                    ? react_1.default.createElement("div", null,
                                        "Prices from ",
                                        react_1.default.createElement("b", null,
                                            "$",
                                            option['_cheapest_computed'].beforeDiscounts.toFixed(0)))
                                    : react_1.default.createElement("div", null,
                                        "Prices from ",
                                        react_1.default.createElement("span", { style: { textDecoration: 'line-through' } },
                                            "$",
                                            option['_cheapest_computed'].beforeDiscounts.toFixed(0)),
                                        " ",
                                        react_1.default.createElement("b", null,
                                            "$",
                                            option['_cheapest_computed'].afterDiscounts.toFixed(0)))),
                            option['_next_departure'] == null
                                ? react_1.default.createElement(antd_1.Button, { style: { marginTop: 6 }, disabled: true }, "No departures available")
                                : react_1.default.createElement(antd_1.Button, { style: { marginTop: 6 }, type: "primary", onClick: function () { return onChange(Object.assign({}, value, { option_id: option.id })); } },
                                    "View departures ",
                                    react_1.default.createElement(antd_1.Icon, { type: "right" })));
                    } }),
            currentStep === 2
                && react_1.default.createElement(calendar_1.default, { className: "tour-calendar", validRange: [new Date(selectedOption['_next_departure']), new Date(selectedOption['_last_departure'])], CalendarDayGenerator: function (date, isPadding, key) {
                        var isoDate = travelcloud_1.dateToIsoDate(date);
                        // if a month does not end on a sat, additional days are drawn to pad out the week
                        // if isPadding, we pretned there's no dayInfo and don't display any info
                        var dayInfo = isPadding ? {} : selectedOptionDateInfoMap[isoDate] || {};
                        var _a = dayInfo.disabled, disabled = _a === void 0 ? true : _a, afterDiscounts = dayInfo.afterDiscounts, _b = dayInfo.noDeparture, noDeparture = _b === void 0 ? true : _b, slotsRemaining = dayInfo.slotsRemaining;
                        return react_1.default.createElement("td", { role: "gridcell", key: key, className: "ant-fullcalendar-cell" + (disabled ? " ant-fullcalendar-disabled-cell" : ""), onClick: function () { return disabled === false && onChange(Object.assign({}, value, { departure_date: isoDate })); } },
                            react_1.default.createElement("div", { className: "ant-fullcalendar-date" },
                                react_1.default.createElement("div", { className: "ant-fullcalendar-value" }, date.getDate()),
                                noDeparture
                                    ? react_1.default.createElement("div", { className: "ant-fullcalendar-content" })
                                    : react_1.default.createElement("div", { className: "ant-fullcalendar-content" },
                                        react_1.default.createElement("div", null, travelcloud_1.formatCurrency(afterDiscounts)),
                                        slotsRemaining != null && react_1.default.createElement("div", null,
                                            slotsRemaining,
                                            " available"))));
                    } }),
            currentStep === 3
                && react_1.default.createElement("div", { style: { position: 'relative' } },
                    react_1.default.createElement(antd_1.List, { grid: { column: allowRooms ? 4 : 1 }, dataSource: roomList, renderItem: function (item) { return item; } }),
                    react_1.default.createElement("div", { className: "tc-with-border-top", style: { position: 'absolute', bottom: 0, padding: 20, width: '100%' } },
                        react_1.default.createElement("div", { style: { float: 'left', fontSize: 18, lineHeight: "32px" } },
                            "Total: ",
                            react_1.default.createElement("b", null, travelcloud_1.formatCurrency(totalPrice)),
                            react_1.default.createElement(antd_1.Popover, { content: react_1.default.createElement("table", { style: { width: 400 } },
                                    react_1.default.createElement("tbody", null, invoiceEntries.reduce(function (acc, group, groupIndex) {
                                        acc.push(react_1.default.createElement("tr", { key: groupIndex },
                                            react_1.default.createElement("th", { style: { paddingTop: 8 }, colSpan: 2 }, group[0])));
                                        for (var i in group[1]) {
                                            var line = group[1][i];
                                            acc.push(react_1.default.createElement("tr", { key: groupIndex + " " + i },
                                                react_1.default.createElement("td", { style: { width: "99%" } },
                                                    line.name,
                                                    " x",
                                                    line.quantity),
                                                react_1.default.createElement("td", null,
                                                    "$",
                                                    big_js_1.default(line.price).times(line.quantity).toFixed(2).toString())));
                                        }
                                        return acc;
                                    }, []))), title: "Price Breakdown", trigger: "click", visible: this.state.visible, onVisibleChange: this.handleVisibleChange },
                                react_1.default.createElement("a", { style: { color: '#999', marginLeft: 32 } },
                                    react_1.default.createElement(antd_1.Icon, { type: "info-circle-o" }),
                                    " Details"))),
                        react_1.default.createElement(antd_1.Button, { type: "primary", htmlType: "submit", style: { float: 'right' } }, "Book now"))));
    };
    return class_2;
}(react_1.default.Component)));
exports.RoomForm = antd_1.Form.create({
    onFieldsChange: function (props, changedFields) {
        if (props.onChange != null)
            props.onChange(changedFields);
    },
    mapPropsToFields: function (props) {
        var value = props.value || { adult: 1 };
        return {
            adult: antd_1.Form.createFormField(value.adult != null ? { value: value.adult } : { value: 1 }),
            child_with_bed: antd_1.Form.createFormField(value.child_with_bed != null ? { value: value.child_with_bed } : { value: 0 }),
            child_no_bed: antd_1.Form.createFormField(value.child_no_bed != null ? { value: value.child_no_bed } : { value: 0 }),
            infant: antd_1.Form.createFormField(value.infant != null ? { value: value.infant } : { value: 0 })
        };
    }
})(function (props) {
    var isFull = props.isFull == null ? false : props.isFull;
    var maxBeds = parseInt(props.tour['max_beds']);
    var maxAdults = parseInt(props.tour['max_adults']);
    var maxChildrenNoBed = parseInt(props.tour['max_children_no_bed']);
    var maxInfants = parseInt(props.tour['max_infants']);
    var allowRooms = props.tour.price_type === 'ALL' || props.tour.price_type.indexOf('SGL') !== -1;
    var allowChildBed = props.tour.price_type === 'ALL' || props.tour.price_type.indexOf('CWB') !== -1;
    var allowChildNoBed = props.tour.price_type === 'ALL' || allowRooms && allowChildBed && props.tour.price_type.indexOf('CNB') !== -1 && maxChildrenNoBed > 0;
    var allowInfant = allowRooms && maxInfants > 0 && (props.tour.price_type === 'ALL' || props.tour.price_type.indexOf('INF') !== -1);
    var adult = props.form.getFieldValue('adult');
    var childBed = props.form.getFieldValue('child_with_bed');
    var childNoBed = props.form.getFieldValue('child_no_bed');
    var infant = props.form.getFieldValue('infant');
    var canAddAdult = isFull === false && (allowRooms === false || (adult < maxAdults && (adult + childBed) < maxBeds));
    var canAddChildBed = isFull === false && (allowRooms === false || ((adult + childBed) < maxBeds));
    var canAddChildNoBed = isFull === false && (childNoBed < maxChildrenNoBed && (adult + childBed) >= 2);
    var canAddInfant = infant < maxInfants;
    var canSubtractChildBed = childBed > 0 && (childBed > 1 || childNoBed === 0);
    var autoTitle = allowRooms ? "Room " + (props.index + 1) : "Travellers";
    return react_1.default.createElement(antd_1.Card, { className: "tc-room-form " + props.className, title: props.title || autoTitle, extra: props.numberOfSiblings > 1 && react_1.default.createElement("a", { onClick: props.deleteSelf }, "Delete"), style: props.style },
        react_1.default.createElement(antd_1.Form.Item, { label: "Adults" },
            react_1.default.createElement(inc_dec_input_1.IncDecInput, { name: "adult", form: props.form, min: 1, max: 9, allowAdd: canAddAdult })),
        allowChildBed
            && react_1.default.createElement(antd_1.Form.Item, { label: "Children" },
                react_1.default.createElement(inc_dec_input_1.IncDecInput, { name: "child_with_bed", form: props.form, min: 0, max: 9, allowAdd: canAddChildBed, allowSubtract: canSubtractChildBed })),
        allowChildNoBed
            && react_1.default.createElement(antd_1.Form.Item, { label: "Children (sharing bed)" },
                react_1.default.createElement(inc_dec_input_1.IncDecInput, { name: "child_no_bed", form: props.form, min: 0, max: 9, allowAdd: canAddChildNoBed })),
        allowInfant
            && react_1.default.createElement(antd_1.Form.Item, { label: "Infants" },
                react_1.default.createElement(inc_dec_input_1.IncDecInput, { name: "infant", form: props.form, min: 0, max: 9, allowAdd: canAddInfant })));
});
//# sourceMappingURL=tour-booking.js.map