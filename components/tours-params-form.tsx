import React from 'react'
import { Form, Input, Select } from 'antd'
import { TourParams } from '../travelcloud'

function filterParams(params) {
  const validKeys: string[] = [
    'categories.name',
    '~boolean']
  for (var i in params){
    if (params[i] == null || params[i] == '') delete params[i]
    else if (validKeys.indexOf(i) === -1) delete params[i]
  }
  return params
}

export class ToursParamsForm extends React.PureComponent<{
  onChange: (legacyHotelParams) => void,
  value: TourParams,
  categories: any
}> {
  timer
  state = {
    boolean: '',
    validateStatus: ('success' as any),
    errorMsg: ''
  }

  protected handle(key, value) {
    // Select passes string onChange
    // Input passes event onChange
    if (value.target != null) value = value.target.value

    // debounce ~boolean param
    if (key === '~boolean') {
      this.setState({
        boolean: value
      })
      if (value != null && value.length < 3 && value.length !== 0) {
        this.setState({
          validateStatus: 'error',
          errorMsg: 'Search term must be at least 3 characters',
        })
        return
      } else {
        this.setState({
          validateStatus: 'success',
          errorMsg: null
        })
      }
      if (value == null || value == '' || value.length < 3) value = null
      if (this.timer != null) {
        clearTimeout(this.timer)
      }
      this.timer = setTimeout(() => {
        const allData = filterParams(Object.assign({}, this.props.value, {'~boolean': value}))
        this.props.onChange(allData)
      }, 500)
    } else {
      value = {
        [key]: value
      }
      const allData = filterParams(Object.assign({}, this.props.value, value))
      this.props.onChange(allData)
    }
  }

  render() {
    var options = [<Select.Option value='' key='EmptyAny'>Any</Select.Option>]

    if (this.props.categories != null && this.props.categories.result != null) {
      options = options.concat(this.props.categories.result.map(d => <Select.Option key={d.name} value={d.name}>{d.name}</Select.Option>));
    }

    return (
      <Form layout="vertical" style={{marginTop: 16}}>
        <Form.Item label="Category">
          <Select onChange={(value) => this.handle('categories.name',  value)} value={this.props.value['categories.name'] || ""}>
            {options}
          </Select>
        </Form.Item>
        <Form.Item
          validateStatus={this.state.validateStatus}
          help={this.state.errorMsg || ''}
          label="Search">
          <Input onChange={(value) => this.handle('~boolean', value)} value={this.state.boolean} />
        </Form.Item>
      </Form>
    )
  }
}
