"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var travelcloud_1 = require("../travelcloud");
var big_js_1 = __importDefault(require("big.js"));
// utility function to
// 1. clone tours
// 2. filter tours based on criteria
// 3. add _next_departure and _cheapest_computed to each tour
function computeTourPricesAndNextDeparture(tours, options) {
    var toursResultCloned = JSON.parse(JSON.stringify(tours));
    var _a = options.rooms, rooms = _a === void 0 ? [{ adult: 2 }] : _a, _b = options.divisor, divisor = _b === void 0 ? 2 : _b, filterOptionType = options.filterOptionType, cart = options.cart;
    return toursResultCloned
        // filter base on filterOptionType
        .map(function (tour) {
        if (filterOptionType != null)
            tour.options = tour.options.filter(function (option) { return option.type === filterOptionType; });
        tour['_next_departure'] = travelcloud_1.nextDeparture(tour.options);
        if (cart == null)
            return tour;
        tour.options = tour.options.map(function (option) {
            option['_cheapest_computed'] =
                //compute price for each departure
                travelcloud_1.mapTourOptionDepartures(option, function (departure, departureDate) {
                    if (departure != null && departure.slots_taken >= departure.slots_total)
                        return null;
                    var product = cart.addTour(tour, {
                        tour_id: tour.id,
                        option_id: option.id,
                        departure_date: departureDate,
                        rooms: rooms
                    }, true);
                    if (product == null)
                        return null;
                    var beforePriceRules = product.items
                        .filter(function (item) { return parseInt(item['price']) > 0; })
                        .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                        .div(divisor);
                    var afterPriceRules = product.items
                        .reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
                        .div(divisor);
                    return {
                        beforePriceRules: beforePriceRules, afterPriceRules: afterPriceRules, option: option, departureDate: departureDate, debug: afterPriceRules.toFixed(2)
                    };
                })
                    // filter out invalid departures
                    .filter(function (val) { return val != null; })
                    // find the cheapest
                    .reduce(function (acc, computed) {
                    if (acc == null || computed.afterPriceRules.cmp(acc.afterPriceRules) === -1) {
                        return computed;
                    }
                    return acc;
                }, null);
            return option;
        });
        tour['_cheapest_computed'] = tour.options.reduce(function (acc, option) {
            if (option['_cheapest_computed'] == null)
                return acc;
            if (acc == null || option['_cheapest_computed'].afterPriceRules.cmp(acc.afterPriceRules) === -1) {
                return option['_cheapest_computed'];
            }
            return acc;
        }, null);
        return tour;
    });
}
exports.computeTourPricesAndNextDeparture = computeTourPricesAndNextDeparture;
exports.ToursResult = function (_a) {
    var toursResult = _a.toursResult, onTourClick = _a.onTourClick, style = _a.style, className = _a.className, cart = _a.cart, _b = _a.rooms, rooms = _b === void 0 ? [{ adult: 2 }] : _b, _c = _a.divisor, divisor = _c === void 0 ? 2 : _c, filterOptionType = _a.filterOptionType;
    var toursResultComputed = computeTourPricesAndNextDeparture(toursResult, { cart: cart, rooms: rooms, divisor: divisor, filterOptionType: filterOptionType });
    return react_1.default.createElement("div", { style: style, className: className },
        react_1.default.createElement(antd_1.List, { grid: { gutter: 16, xs: 1, sm: 1, md: 1, lg: 2, xl: 3, xxl: 4 }, dataSource: toursResultComputed, renderItem: function (tour) {
                return react_1.default.createElement(antd_1.List.Item, null,
                    react_1.default.createElement("div", { className: "ant-card ant-card-bordered ant-card-hoverable", onClick: function () { return onTourClick(tour); } },
                        react_1.default.createElement("div", { style: {
                                backgroundImage: "url(" + tour.photo_url + ")",
                                height: '200px',
                                backgroundSize: 'cover',
                                backgroundPosition: 'center'
                            } }),
                        react_1.default.createElement("div", { style: { padding: '8px' } },
                            react_1.default.createElement("div", { className: "tc-full-width-ellipsis", style: { fontWeight: 'bold', fontSize: '16px' } }, tour.name),
                            tour['_next_departure'] == null || tour.options.length === 0
                                ? react_1.default.createElement("div", null, "No departure scheduled")
                                : react_1.default.createElement("div", null,
                                    "Next departure on: ",
                                    react_1.default.createElement("b", null, tour['_next_departure'])),
                            tour['_cheapest_computed'] == null && react_1.default.createElement("div", null, "\u00A0"),
                            tour['_cheapest_computed'] != null && (tour['_cheapest_computed'].beforePriceRules.eq(tour['_cheapest_computed'].afterPriceRules)
                                ? react_1.default.createElement("div", null,
                                    "Prices from: ",
                                    react_1.default.createElement("b", null,
                                        "$",
                                        tour['_cheapest_computed'].beforePriceRules.toFixed(0)))
                                : react_1.default.createElement("div", null,
                                    "Prices from: ",
                                    react_1.default.createElement("span", { style: { textDecoration: 'line-through' } },
                                        "$",
                                        tour['_cheapest_computed'].beforePriceRules.toFixed(0)),
                                    " ",
                                    react_1.default.createElement("b", null,
                                        "$",
                                        tour['_cheapest_computed'].afterPriceRules.toFixed(0)))))));
            } }));
};
//# sourceMappingURL=tours-result.js.map