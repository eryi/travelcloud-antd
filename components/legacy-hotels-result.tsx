import React from 'react'
import { List, Row, Col, Rate, Icon, Button } from 'antd'
import { formatCurrency } from '../travelcloud';

export const LegacyHotelsResult = ({hotels, onHotelClick} : {hotels: any, onHotelClick: (any) => any}) => {
  return <List
  dataSource={hotels}
  renderItem={hotel => (
    <List.Item key={hotel.hotelId}>
      <Row style={{width: '100%'}}>
        <Col span={8} style={{height: 350}}>
          <div style={{
            backgroundImage: `url(${hotel.images[0] ? hotel.images[0].image : ''})`,
            height: '100%',
            width: '100%',
            backgroundSize: 'cover',
            backgroundPosition: 'center'}} />
        </Col>
        <Col span={16} style={{height: 350, padding: "16px 32px"}}>
            <h2>{hotel.name}<Rate style={{paddingLeft: 16, lineHeight: '2.5em'}} value={parseInt(hotel.rating.charAt(0))} /></h2>
            <Button type="primary" style={{position: 'absolute', right: 36, top: 24}} onClick={() => onHotelClick(hotel)}>More details <Icon type="right" /></Button>
            <div>{hotel.description != null && (hotel.description.substr(0,350) + (hotel.description.length > 350 ? "..." : ""))}</div>
            <table style={{tableLayout: "fixed", width: '100%', marginTop: 16}}>
              <tbody>
                <tr>
                  <th style={{width: '40%'}}>Rooms</th>
                  <th style={{width: '40%'}}></th>
                  <th style={{width: '10%'}}></th>
                  <th style={{width: '10%'}}></th>
                </tr>
                {hotel.rooms.filter(room => room.prices.length > 0).slice(0,5).map(room =>
                  <tr key={room.id}>
                    <td>{room.description}</td>
                    <td>{room.prices[0].meal}</td>
                    <td><b>{formatCurrency(room.prices[0].price)}</b></td>
                  </tr>
                )}
              </tbody>
            </table>
        </Col>
      </Row>
    </List.Item>
)} />}