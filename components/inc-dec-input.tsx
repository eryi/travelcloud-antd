import React from 'react'
import { Input, Icon } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";

// copied from InputNumber, will prune later
// https://github.com/ant-design/ant-design/blob/fdb943e8878d79aca8f10c3595f37f89adddf6ff/components/input-number/index.tsx
export interface InputNumberProps {
  prefixCls?: string;
  min?: number;
  max?: number;
  value?: number;
  step?: number | string;
  defaultValue?: number;
  onChange?: (value: number | string | undefined) => void;
  disabled?: boolean;
  size?: 'large' | 'small' | 'default';
  formatter?: (value: number | string | undefined) => string;
  parser?: (displayValue: string | undefined) => number;
  placeholder?: string;
  style?: React.CSSProperties;
  className?: string;
  name?: string;
  precision?: number;
  allowAdd?: boolean;
  allowSubtract?: boolean;
}

export const IncDecInput = (props: InputNumberProps & React.HTMLProps<HTMLDivElement> & {form?: WrappedFormUtils}) => {
  var getFieldDecorator = (_) => (x) => x
  var val
  var valProp: any = {}

  if (props.form == null) {
    val = props.value
    valProp.value = val
  } else {
    val = props.form.getFieldValue(props.name)
    getFieldDecorator = props.form.getFieldDecorator
  }

  const handleInc = () => {
    if (props.allowAdd != null && props.allowAdd === false) return
    val++
    if (props.max != null && val > props.max) val = props.max

    if (props.form != null) {
      props.form.setFieldsValue({
        [props.name]: val
      })
    }
    if (props.onChange != null) {
      props.onChange(val)
    }
  }

  const handleDec = () => {
    if (props.allowSubtract != null && props.allowSubtract === false) return
    val--
    if (props.min != null && val < props.min) val = props.min

    if (props.form != null) {
      props.form.setFieldsValue({
        [props.name]: val
      })
    }
    if (props.onChange != null) {
      props.onChange(val)
    }
  }

  const effectiveAllowAdd = props.allowAdd == null ? val < props.max : props.allowAdd && val < props.max
  const effectiveAllowSubtract = props.allowSubtract == null ? val > props.min : props.allowSubtract && val > props.min
  var className = ""
  if (effectiveAllowAdd) className = className + ' tc-right-addon-primary'
  if (effectiveAllowSubtract) className = className + ' tc-left-addon-primary'


  // not sure why TypeScript freaks out if we don't cast to any
  // getFieldDecorator probably has some incorrect types
  return <div className={className}>
    {getFieldDecorator(props.name)(
      <Input
        {...valProp}
        className="tc-inc-dec-input"
        type="number"
        addonAfter={<Icon type="plus" onClick={handleInc} />}
        addonBefore={<Icon type="minus" onClick={handleDec} />}
        min={props.min}
        max={props.max}
        // prevent readonly warning
        onChange={() => {}}
      />)}</div> as any
}