"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
/* Use ANTD Modal */
var AttractionModal = function (_a) {
    var data = _a.data, handleClose = _a.handleClose, show = _a.show;
    return (react_1.default.createElement(antd_1.Modal, { title: "Attraction Detail", visible: show, onCancel: handleClose, width: 800, footer: null },
        react_1.default.createElement("p", null,
            react_1.default.createElement("img", { alt: "No Image", width: "100%", src: "https://uat-api.globaltix.com/api/image?name=" + data.imagePath })),
        react_1.default.createElement("h4", null, data.title),
        react_1.default.createElement("p", null, data.description),
        react_1.default.createElement("p", null,
            react_1.default.createElement("hr", null)),
        react_1.default.createElement("p", null, "Operating Hours"),
        react_1.default.createElement("p", null, data.hoursOfOperation)));
};
exports.default = AttractionModal;
//# sourceMappingURL=attraction-detail.js.map