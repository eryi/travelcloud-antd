"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
function filterParams(params) {
    var validKeys = [
        'categories.name',
        '~boolean'
    ];
    for (var i in params) {
        if (params[i] == null || params[i] == '')
            delete params[i];
        else if (validKeys.indexOf(i) === -1)
            delete params[i];
    }
    return params;
}
var ToursParamsForm = /** @class */ (function (_super) {
    __extends(ToursParamsForm, _super);
    function ToursParamsForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            boolean: '',
            validateStatus: 'success',
            errorMsg: ''
        };
        return _this;
    }
    ToursParamsForm.prototype.handle = function (key, value) {
        var _this = this;
        var _a;
        // Select passes string onChange
        // Input passes event onChange
        if (value.target != null)
            value = value.target.value;
        // debounce ~boolean param
        if (key === '~boolean') {
            this.setState({
                boolean: value
            });
            if (value != null && value.length < 3 && value.length !== 0) {
                this.setState({
                    validateStatus: 'error',
                    errorMsg: 'Search term must be at least 3 characters',
                });
                return;
            }
            else {
                this.setState({
                    validateStatus: 'success',
                    errorMsg: null
                });
            }
            if (value == null || value == '' || value.length < 3)
                value = null;
            if (this.timer != null) {
                clearTimeout(this.timer);
            }
            this.timer = setTimeout(function () {
                var allData = filterParams(Object.assign({}, _this.props.value, { '~boolean': value }));
                _this.props.onChange(allData);
            }, 500);
        }
        else {
            value = (_a = {},
                _a[key] = value,
                _a);
            var allData = filterParams(Object.assign({}, this.props.value, value));
            this.props.onChange(allData);
        }
    };
    ToursParamsForm.prototype.render = function () {
        var _this = this;
        var options = [react_1.default.createElement(antd_1.Select.Option, { value: '', key: 'EmptyAny' }, "Any")];
        if (this.props.categories != null && this.props.categories.result != null) {
            options = options.concat(this.props.categories.result.map(function (d) { return react_1.default.createElement(antd_1.Select.Option, { key: d.name, value: d.name }, d.name); }));
        }
        return (react_1.default.createElement(antd_1.Form, { layout: "vertical", style: { marginTop: 16 } },
            react_1.default.createElement(antd_1.Form.Item, { label: "Category" },
                react_1.default.createElement(antd_1.Select, { onChange: function (value) { return _this.handle('categories.name', value); }, value: this.props.value['categories.name'] || "" }, options)),
            react_1.default.createElement(antd_1.Form.Item, { validateStatus: this.state.validateStatus, help: this.state.errorMsg || '', label: "Search" },
                react_1.default.createElement(antd_1.Input, { onChange: function (value) { return _this.handle('~boolean', value); }, value: this.state.boolean }))));
    };
    return ToursParamsForm;
}(react_1.default.PureComponent));
exports.ToursParamsForm = ToursParamsForm;
//# sourceMappingURL=tours-params-form.js.map