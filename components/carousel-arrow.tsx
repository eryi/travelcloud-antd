import React from 'react'
import { Icon } from 'antd'

export default class CarouselArrow extends React.Component<{ iconType:any, theme?:any, classname?:any, onClick?:any }>{
  render() {
    const { iconType, theme, classname, onClick } = this.props
    return (
      <div className={classname} onClick={onClick}>
        <Icon type={iconType} theme={theme} />
      </div>
    )
  }
}
