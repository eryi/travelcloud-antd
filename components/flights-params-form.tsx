import React from 'react'
import moment from 'moment'
import { Form, DatePicker, Select, Button, Row, Col, Radio, Icon } from 'antd'
import { TravelCloudClient, FlightsParams } from '../travelcloud'
import { stringify } from 'querystring';

const FormValueContext = React.createContext<FlightsParams>(null);
const FormRcContext = React.createContext<FlightsParamsForm>(null);
const FormState = React.createContext<any>(null);

const validKeys: string[] = [
  "source",
  "od1.origin_airport.code",
  "od1.origin_datetime",
  "od1.destination_airport.code",
  "od2.origin_airport.code",
  "od2.origin_datetime",
  "od2.destination_airport.code",
  "cabin",
  "ptc_adt",
  "ptc_cnn",
  "ptc_inf"]

export class FlightPtcAdtSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('ptc_adt', value)}
            value={formValue.ptc_adt}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>

  }
}

export class FlightPtcCnnSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('ptc_cnn', value)}
            value={formValue.ptc_cnn}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={0}>0</Select.Option>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class FlightPtcInfSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('ptc_inf', value)}
            value={formValue.ptc_inf}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={0}>0</Select.Option>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class FlightCabinSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('cabin', value)}
            value={formValue.cabin}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value='Y'>Economy</Select.Option>
            <Select.Option value='S'>Premium Economy</Select.Option>
            <Select.Option value='C'>Business</Select.Option>
            <Select.Option value='F'>First</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class FlightOd2OriginDepartureDatePicker extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <DatePicker
            value={formValue['od2.origin_datetime'] == null || formValue['od2.origin_datetime'] === '' ? null : moment(formValue['od2.origin_datetime'])}
            defaultPickerValue={formValue['od1.origin_datetime'] == null ? null : moment(formValue['od1.origin_datetime']).add(1, 'days')}
            disabledDate={rc.disabledEndDate}
            style={this.props.style}
            placeholder={this.props.placeholder || "(one way)"}
            className={this.props.className}
            format="YYYY-MM-DD"
            onChange={(value) => rc.handle('od2.origin_datetime', value)} />
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightOd1OriginDepartureDatePicker extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <DatePicker
            value={formValue['od1.origin_datetime'] == null || formValue['od1.origin_datetime'] === '' ? null : moment(formValue['od1.origin_datetime'])}
            disabledDate={rc.disabledStartDate}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}
            format="YYYY-MM-DD"
            onChange={(value) => rc.handle('od1.origin_datetime', value)} />
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightOd1OriginCodeSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  state ={
    options: null
  }
  handleSearch = async (rc, searchValue, currentValue) => {
    if (searchValue.length < 3) return
    const params = {type: 'iata', search: searchValue} as any
    if (currentValue != null) {
      params.values = currentValue
    }
    const result = await rc.props.client.autoComplete(params)
    //FIXME: don't crash when when result returns error
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label} ({d.value})</Select.Option>);
    if (result.result != null) {
      this.setState({
        options
      })}
  }
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
          <Select
            showSearch
            onChange={(value) => rc.handle('od1.origin_airport.code', value)}
            value={formValue['od1.origin_airport.code']}
            onSearch={(searchValue) => this.handleSearch(rc, searchValue, formValue['od1.origin_airport.code'])}
            filterOption={() => true}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}
            defaultActiveFirstOption={false}>
            {this.state.options || state.defaultOptions}
          </Select>
        }</FormState.Consumer>
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightOd1DestinationCodeSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  state ={
    options: null
  }
  handleSearch = async (rc, searchValue, currentValue) => {
    if (searchValue.length < 3) return
    const params = {type: 'iata', search: searchValue} as any
    if (currentValue != null) {
      params.values = currentValue
    }
    const result = await rc.props.client.autoComplete(params)
    //FIXME: don't crash when when result returns error
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label} ({d.value})</Select.Option>);
    if (result.result != null) {
      this.setState({
        options
      })}
  }
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
          <Select
            showSearch
            onChange={(value) => rc.handle('od1.destination_airport.code', value)}
            value={formValue['od1.destination_airport.code']}
            onSearch={(searchValue) => this.handleSearch(rc, searchValue, formValue['od1.destination_airport.code'])}
            filterOption={() => true}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}
            defaultActiveFirstOption={false}>
            {this.state.options || state.defaultOptions}
          </Select>
        }</FormState.Consumer>
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightOd2OriginCodeSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  state ={
    options: null
  }
  handleSearch = async (rc, searchValue, currentValue) => {
    if (searchValue.length < 3) return
    const params = {type: 'iata', search: searchValue} as any
    if (currentValue != null) {
      params.values = currentValue
    }
    const result = await rc.props.client.autoComplete(params)
    //FIXME: don't crash when when result returns error
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label} ({d.value})</Select.Option>);
    if (result.result != null) {
      this.setState({
        options
      })}
  }
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
              <Select
                showSearch
                onChange={(value) => rc.handle('od2.origin_airport.code', value)}
                value={formValue['od2.origin_airport.code']}
                onSearch={(searchValue) => this.handleSearch(rc, searchValue, formValue['od2.origin_airport.code'])}
                filterOption={() => true}
                style={this.props.style}
                placeholder={this.props.placeholder}
                className={this.props.className}
                defaultActiveFirstOption={false}>
                {this.state.options || state.defaultOptions}
          </Select>
        }</FormState.Consumer>
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightOd2DestinationCodeSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  state ={
    options: null
  }
  handleSearch = async (rc, searchValue, currentValue) => {
    if (searchValue.length < 3) return
    const params = {type: 'iata', search: searchValue} as any
    if (currentValue != null) {
      params.values = currentValue
    }
    const result = await rc.props.client.autoComplete(params)
    //FIXME: don't crash when when result returns error
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label} ({d.value})</Select.Option>);
    if (result.result != null) {
      this.setState({
        options
      })}
  }
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
              <Select
                showSearch
                onChange={(value) => rc.handle('od2.destination_airport.code', value)}
                value={formValue['od2.destination_airport.code']}
                onSearch={(searchValue) => this.handleSearch(rc, searchValue, formValue['od2.destination_airport.code'])}
                filterOption={() => true}
                style={this.props.style}
                placeholder={this.props.placeholder}
                className={this.props.className}
                defaultActiveFirstOption={false}>
                {this.state.options || state.defaultOptions}
          </Select>
        }</FormState.Consumer>
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class FlightsParamsForm extends React.PureComponent<{
  onChange: (FlightParams) => void,
  value: FlightsParams,
  client: TravelCloudClient,
  defaultIataCodes: string[],
  loading?: boolean
}> {

  static Od1OriginCodeSelect = FlightOd1OriginCodeSelect
  static Od1DestinationCodeSelect = FlightOd1DestinationCodeSelect
  static Od2OriginCodeSelect = FlightOd2OriginCodeSelect
  static Od2DestinationCodeSelect = FlightOd2DestinationCodeSelect
  static Od1OriginDepartureDatePicker = FlightOd1OriginDepartureDatePicker
  static Od2OriginDepartureDatePicker = FlightOd2OriginDepartureDatePicker
  static PtcAdtSelect = FlightPtcAdtSelect
  static PtcCnnSelect = FlightPtcCnnSelect
  static PtcInfSelect = FlightPtcInfSelect

  state = {
    endOpen: false,
    defaultOptions: null,
    data1: [],
    data2: []
  }

  async componentDidMount() {
    const defaultClone = this.props.defaultIataCodes.slice(0)
    if (this.props.value['od1.origin_airport.code'] != null) {
      defaultClone.push(this.props.value['od1.origin_airport.code'])
    }
    if (this.props.value['od2.origin_airport.code'] != null) {
      defaultClone.push(this.props.value['od2.origin_airport.code'])
    }
    const result = await this.props.client.autoComplete({type: 'iata', values: defaultClone})
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label} ({d.value})</Select.Option>);
    if (result.result != null) {
      this.setState({
        defaultOptions: options
      })}
  }

  handle(key, value) {
    // Select passes string onChange
    // Input passes event onChange
    var update
    if (value.target != null) value = value.target.value

    if (value.format != null) value = value.format('YYYY-MM-DD')
    update = {
      [key]: value
    }

    const allData = Object.assign({}, this.props.value, update)

    for (var i in allData){
      if (allData[i] == null) delete allData[i]
      else if (validKeys.indexOf(i) === -1) delete allData[i]
    }

    this.props.onChange(allData)
  }
  disabledStartDate = (od1_origin_departure) => {
    // not sure why od1_origin_departure might be null
    if (od1_origin_departure == null) return false
    return od1_origin_departure.isBefore();
  }
  disabledEndDate = (od2_origin_departure) => {
    if (od2_origin_departure == null) return false
    const od1_origin_departure = this.props.value['od1.origin_datetime'] == null ? moment() : moment(this.props.value['od1.origin_datetime']);
    if (!od2_origin_departure || !od1_origin_departure) {
      return od2_origin_departure && od2_origin_departure < moment().endOf('day');
    }
    return od2_origin_departure.isBefore(moment(od1_origin_departure).add(1, 'days'));
  }

  render() {
    const children = this.props.children || <Form layout="vertical" style={{marginTop: 16}}>
      <Form.Item label="From">
        <FlightOd1OriginCodeSelect />
      </Form.Item>
      <Form.Item label="To">
        <FlightOd2OriginCodeSelect />
      </Form.Item>
      <Form.Item label="Departure date">
        <FlightOd1OriginDepartureDatePicker style={{width: '100%'}} />
      </Form.Item>
      <Form.Item label="Return Date">
        <FlightOd2OriginDepartureDatePicker style={{width: '100%'}} />
      </Form.Item>
      <Form.Item label="Adults">
        <FlightPtcAdtSelect />
      </Form.Item>
      <Form.Item label="Children">
        <FlightPtcCnnSelect />
      </Form.Item>
      <Form.Item label="Infants">
        <FlightPtcInfSelect />
      </Form.Item>
    </Form>

    return (
      <FormValueContext.Provider value={this.props.value}><FormRcContext.Provider value={this}><FormState.Provider value={this.state}>
        {children}
      </FormState.Provider></FormRcContext.Provider></FormValueContext.Provider>
    )
  }
}

export class FlightsParamsAdvancedForm extends React.PureComponent<{
  onChange: (FlightParams) => void,
  onSearch: (FlightParams) => void,
  onTypeChange: (FlightParams) => void,
  onSwapClick?: () => void,
  value: FlightsParams,
  client: TravelCloudClient,
  defaultIataCodes: string[],
  type: string,
  loading?: boolean
}> {

  render() {
    const rowStyle = {display: "flex", flexDirection: "row" as any, maxWidth: "1000px", margin: "20px 0"}
    return <FlightsParamsForm {...this.props}>
      <Radio.Group value={this.props.type} onChange={(e) => this.props.onTypeChange(e.target.value)}>
        <Radio.Button value="return">Return</Radio.Button>
        <Radio.Button value="one">One way</Radio.Button>
        <Radio.Button value="multi">Multi-city</Radio.Button>
      </Radio.Group>
      {this.props.type !== 'multi'
        ? <div style={rowStyle}>
            <div style={{flex: '2'}}>
              <div>From</div>
              <FlightsParamsForm.Od1OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '0'}}>
              <div>&nbsp;</div>
              <div>
                <Icon type="swap" onClick={() => {if (this.props.onSwapClick != null) this.props.onSwapClick()}} style={{fontSize: '18px', padding: '5px 15px 5px'}} />
              </div>
            </div>
            <div style={{flex: '2'}}>
              <div>To</div>
              <FlightsParamsForm.Od2OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '1'}}>
              <div>Depart</div>
              <FlightsParamsForm.Od1OriginDepartureDatePicker style={{width: '95%'}} />
            </div>
            {this.props.type === 'return' && <div style={{flex: '1'}}>
              <div>Return</div>
              <FlightsParamsForm.Od2OriginDepartureDatePicker style={{width: '95%'}} />
            </div>}
          </div>
        : <div>
          <div style={rowStyle}>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od1OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{width: '30px'}}>→</div>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od1DestinationCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '1'}}>
              <FlightsParamsForm.Od1OriginDepartureDatePicker style={{width: '95%'}} />
            </div>
          </div>
          <div style={rowStyle}>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od2OriginCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{width: '30px'}}>→</div>
            <div style={{flex: '2'}}>
              <FlightsParamsForm.Od2DestinationCodeSelect style={{width: '95%'}} />
            </div>
            <div style={{flex: '1'}}>
              <FlightsParamsForm.Od2OriginDepartureDatePicker style={{width: '95%'}} />
            </div>
          </div>
        </div>
      }
      <div style={rowStyle}>
        <div style={{flex: '1'}}>
          <div>Adults</div>
          <FlightsParamsForm.PtcAdtSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '1'}}>
          <div>Children</div>
          <FlightsParamsForm.PtcCnnSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '1'}}>
          <div>Infants</div>
          <FlightsParamsForm.PtcInfSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '2'}}>
          <div>Cabin</div>
          <FlightCabinSelect style={{width: '95%'}} />
        </div>
        <div style={{flex: '2'}}>
          <div>&nbsp;</div>
          <Button type="primary" disabled={this.props.loading === true} onClick={() => this.props.onSearch(this.props.value)} style={{width: '95%'}}>Search</Button>
        </div>
      </div>
  </FlightsParamsForm>

  }
}