import React from 'react'
import moment from 'moment'
import { Form, DatePicker, Select } from 'antd'
import { TravelCloudClient, LegacyHotelsParams } from '../travelcloud'

const FormValueContext = React.createContext<LegacyHotelsParams>(null);
const FormRcContext = React.createContext<LegacyHotelsParamsForm>(null);
const FormState = React.createContext<any>(null);

const validKeys: string[] = [
  'cityCode',
  'checkInDate',
  'checkOutDate',
  'adults']

export class LegacyHotelAdultOccupancySelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <Select
            onChange={(value) => rc.handle('adults', value)}
            value={formValue.adults}
            style={this.props.style}
            placeholder={this.props.placeholder}
            className={this.props.className}>
            <Select.Option value={1}>Single</Select.Option>
            <Select.Option value={2}>Double</Select.Option>
            <Select.Option value={3}>Triple</Select.Option>
          </Select>}
      </FormRcContext.Consumer>}
    </FormValueContext.Consumer>
  }
}

export class LegacyHotelCheckOutDatePicker extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
              <DatePicker
                value={formValue.checkOutDate == null ? null : moment(formValue.checkOutDate)}
                defaultPickerValue={formValue.checkInDate == null ? null : moment(formValue.checkInDate).add(1, 'days')}
                disabledDate={(x) => rc.disabledEndDate(x)}
                style={this.props.style}
                placeholder={this.props.placeholder}
                className={this.props.className}
                format="YYYY-MM-DD"
                onChange={(value) => rc.handle('checkOutDate', value)}
                open={state.endOpen}
                onOpenChange={rc.handleEndOpenChange}
            />
        }</FormState.Consumer>
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class LegacyHotelCheckInDatePicker extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
              <DatePicker
                value={formValue.checkInDate == null ? null : moment(formValue.checkInDate)}
                disabledDate={(x) =>rc.disabledStartDate(x)}
                style={this.props.style}
                placeholder={this.props.placeholder}
                className={this.props.className}
                format="YYYY-MM-DD"
                onChange={(value) => rc.handle('checkInDate', value)} />
          }</FormState.Consumer>
        }</FormRcContext.Consumer>
      }</FormValueContext.Consumer>
  }
}

export class LegacyHotelCityCodeSelect extends React.PureComponent<{className?: string, style?: React.CSSProperties, placeholder?: string}> {
  state = {
    options: null
  }
  handleSearch = async (rc, value) => {
    if (value.length < 3) return
    const result = await rc.props.client.autoComplete({type: 'bedsonline', search: value, values: [this.state['cityCode']]})
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label}</Select.Option>);
    if (result.result != null) {
      this.setState({
        options
      })}
  }
  render() {
    return <FormValueContext.Consumer>
    {(formValue) =>
      <FormRcContext.Consumer>
        {(rc) =>
          <FormState.Consumer>
            {(state) =>
              <Select
                showSearch
                onChange={(value) => rc.handle('cityCode', value)}
                value={formValue['cityCode']}
                onSearch={(val) => this.handleSearch(rc, val)}
                filterOption={() => true}
                style={this.props.style}
                placeholder={this.props.placeholder}
                className={this.props.className}
                defaultActiveFirstOption={false}>
                {this.state.options || state.defaultOptions}
              </Select>
          }</FormState.Consumer>
      }</FormRcContext.Consumer>
    }</FormValueContext.Consumer>
  }
}

export class LegacyHotelsParamsForm extends React.PureComponent<{
  onChange: (legacyHotelParams) => void,
  value: LegacyHotelsParams,
  client: TravelCloudClient,
  defaultCityCodes: string[]
}> {

  static CityCodeSelect = LegacyHotelCityCodeSelect
  static CheckInDatePicker = LegacyHotelCheckInDatePicker
  static CheckOutDatePicker = LegacyHotelCheckOutDatePicker
  static AdultOccupancySelect = LegacyHotelAdultOccupancySelect

  state = {
    endOpen: false,
    defaultOptions: null
  }

  async componentDidMount() {
    const defaultClone = this.props.defaultCityCodes.slice(0)
    if (this.props.value.cityCode != null) {
      defaultClone.push(this.state['cityCode'])
    }
    const result = await this.props.client.autoComplete({type: 'bedsonline', values: defaultClone})
    const options = result.result.map(d => <Select.Option key={d.value} value={d.value}>{d.label}</Select.Option>);
    if (result.result != null) {
      this.setState({
        defaultOptions: options
      })}
  }

  handle(key, value) {
    // Select passes string onChange
    // Input passes event onChange
    var update
    if (value.target != null) value = value.target.value

    // to prevent useless seach, we always clear checkOutDate if user setting checkInDate
    /*
    if (key === 'checkInDate') {
      update = {
        checkInDate: value.format('YYYY-MM-DD'),
        checkOutDate: null
      }
    }
    */

    if (value.format != null) value = value.format('YYYY-MM-DD')
    update = {
      [key]: value
    }

    if (key === 'checkInDate' && this.props.value.checkOutDate == null) {
      this.setState({
        endOpen: true
      })
    }

    const allData = Object.assign({}, this.props.value, update)

    for (var i in allData){
      if (allData[i] == null) delete allData[i]
      else if (validKeys.indexOf(i) === -1) delete allData[i]
    }

    this.props.onChange(allData)
  }
  disabledStartDate = (checkInDate) => {
    // not sure why checkInDate might be null
    if (checkInDate == null) return false
    return checkInDate.isBefore();
  }
  disabledEndDate = (checkOutDate) => {
    if (checkOutDate == null) return false
    const checkInDate = this.props.value.checkInDate == null ? moment() : moment(this.props.value.checkInDate);
    if (!checkOutDate || !checkInDate) {
      return checkOutDate && checkOutDate < moment().endOf('day');
    }
    return checkOutDate.isBefore(moment(checkInDate).add(1, 'days'));
  }

  handleStartOpenChange = (open) => {
    if (!open && this.props.value.checkInDate == null) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }

  render() {
    const children = this.props.children || <Form layout="vertical" style={{marginTop: 16}}>
      <Form.Item label="City">
        <LegacyHotelCityCodeSelect />
      </Form.Item>
      <Form.Item label="Check-in">
        <LegacyHotelCheckInDatePicker style={{width: '100%'}} />
      </Form.Item>
      <Form.Item label="Check-out">
        <LegacyHotelCheckOutDatePicker style={{width: '100%'}} />
      </Form.Item>
      <Form.Item label="Room occupancy">
        <LegacyHotelAdultOccupancySelect />
      </Form.Item>
    </Form>
    return (
      <FormValueContext.Provider value={this.props.value}><FormRcContext.Provider value={this}><FormState.Provider value={this.state}>
        {children}
      </FormState.Provider></FormRcContext.Provider></FormValueContext.Provider>
    )
  }
}