"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
var router_1 = __importDefault(require("next/router"));
var Panel = antd_1.Collapse.Panel;
exports.SearchResult = function (_a) {
    var offset = _a.offset, limit = _a.limit, data = _a.data;
    return (react_1.default.createElement("div", { key: "main-container", className: "search-result" }, data == null || data.length === 0 ? (react_1.default.createElement("h4", null, "There is no record found!")) : (data.map(function (item, index) {
        if (index >= offset && index <= limit) {
            return (react_1.default.createElement(antd_1.List, { size: "large", itemLayout: "horizontal", dataSource: data, className: "main-collaspe-relative", renderItem: function (item) { return (react_1.default.createElement(antd_1.List.Item, null,
                    react_1.default.createElement(antd_1.List.Item.Meta, { avatar: react_1.default.createElement(antd_1.Avatar, { src: "https://uat-api.globaltix.com/api/image?name=" + item.imagePath }), title: react_1.default.createElement("a", { onClick: function () { return router_1.default.push({
                                pathname: '/globaltix-details',
                                query: { 'result': item.id },
                            }); } }, item.title) }))); } }));
        }
    }))));
};
//# sourceMappingURL=search-result.js.map