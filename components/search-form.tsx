import React from "react";
import { Form, Select, Input, Button, Row, Col } from "antd";
// import queryString from "query-string";

const { Option } = Select;
const Search = Input.Search;

export const SearchForm: React.StatelessComponent<{
  countryList: any;
  cityList: any;
  searchFormKeys: any;
  onSearch: any;
}> = ({ countryList, cityList, searchFormKeys, onSearch }) => {
  return (
    <div className="search-deals" key={"Searchdiv"}>
      <Form onSubmit={(e) => e.preventDefault()} key={"SearchForm"}>
        <Row gutter={16} key={"row1"}>
          <Col md={24} sm={24} key={"col1"}>
            <Form.Item key={"formItem1"}>
              <Search
                defaultValue={searchFormKeys.keyword}
                placeholder="Keyword"
                onSearch={value => onSearch(value, 'keyword')}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16} key={"row2"}>
          <Col lg={8} md={12} sm={12} xs={24} key={"col2"}>
            <Form.Item key={"formItem2"}>
              <Select
                key="country"
                placeholder="Country"
                defaultValue={searchFormKeys.countryId}
                onChange={e => onSearch(e, "countryId")}
              >
                <Option value="" key={"emptycountry"}>Select Country</Option>
                {countryList.map((item, index) => (
                  <Option value={"" + item.id} key={"country" + item.id}>{item.name}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col lg={8} md={12} sm={12} xs={24} key={"col3"}>
            <Form.Item key={"formItem3"}>
              <Select
                key="city"
                placeholder="City"
                defaultValue={searchFormKeys.cityId}
                onChange={e => onSearch(e, "cityId")}
              >
                <Option value="" key={"emptycity"}>Select City</Option>
                {cityList.map((item, index) => {
                  if (parseInt(item.countryId) == parseInt(searchFormKeys.countryId)) {
                    return <Option value={"" + item.id} key={"city" + item.id}>{item.name}</Option>
                  }
                })}
              </Select>
            </Form.Item>
          </Col>
          <Col lg={8} md={12} sm={12} xs={24} key={"col4"}>
            <Form.Item key={"formItem4"}>
              <Select
                key="category"
                placeholder="Category"
                defaultValue={"" + searchFormKeys.tab}
                onChange={e => onSearch(e, "tab")}
              >
                <Option value="ALL" key={"emptytab"}>ALL</Option>
                <Option value="Attractions" key={"Attractionstab"}>Attractions</Option>
                <Option value="Tours" key={"Tourstab"}>Tours</Option>
                <Option value="Lifestyle" key={"Lifestyletab"}>Lifestyle</Option>
              </Select>
            </Form.Item>
          </Col>

        </Row>
      </Form>
    </div>
  );
};

