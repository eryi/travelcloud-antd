import React, { Children } from 'react'
import { Form, Button, Icon, List, Steps, Card, Popover } from 'antd'
import Calendar from '../components/calendar'
import { dateToIsoDate, formatCurrency, extractValueFromFormState, Cart, mapTourOptionDepartures } from '../travelcloud'
import  Big from 'big.js'
import { IncDecInput } from './inc-dec-input'
import { computeTour, TourFormValue } from '../order-product-compute';

export interface Room {
  adult: number,
  child_with_bed?: number,
  child_no_bed?: number,
  infant?: number
}

export interface TourBookingFormValue {
  tour_id: string,
  rooms?: Room[],
  option_id?: string,
  departure_date?: string
}

export interface SelectedOptionDayInfo {date: Date, disabled, afterDiscounts, beforeDiscounts, product, noDeparture, slotsRemaining}

export interface TourBookingContext {
  tour? : any,
  cart: Cart,
  value : TourBookingFormValue,
  onChange: (TourBookingFormValue) => void,
  onSubmit: (TourBookingFormValue) => void,

  //computed values
  allowRooms: boolean,
  selectedOption: any,
  selectedOptionDateInfoMap: {[key: string]: SelectedOptionDayInfo},
  tourOptionsWithCheapestComputed,
  totalPrice,
  totalDeposit,
  invoiceEntries
}

export const initialTourBookingForm = (tour_id: string, option_id: string = '', departure_date: string = '', rooms = [{adult: 1}]) => ({
  tour_id,
  option_id,
  departure_date,
  rooms
})

export const createTourBookingForm = (WrappedComponent) => {
  return class extends React.Component<{
    value,
    tour,
    cart: Cart,
    onSubmit?,
    onChange?,
    priceComputationRooms?: Room[],
    [key: string] : any //properties of WrappedComponent
  }> {
    render() {
      const {
        value = {},
        tour,
        cart,
        onSubmit = () => {},
        onChange = () => {},
        priceComputationRooms = [{
          adult: 2
        }]
    } = this.props
      if (tour != null && value.tour_id !== tour.id) value.tour_id = tour.id

      var selectedOption = null
      var selectedDeparture = null

      const option_id = value.option_id
      const departure_date = value.departure_date

      if (option_id !== '') {
        for (var i in tour.options) {
          if (tour.options[i].id === option_id) selectedOption = tour.options[i]
        }
      }

      if (selectedOption != null && departure_date !== '') {
        for (var i in selectedOption.departures) {
          if (selectedOption.departures[i].id === departure_date) selectedDeparture = selectedOption.departures[i]
        }
      }

      const priceComputationDivisor = priceComputationRooms.reduce((acc, room) =>
        Object.entries(room).reduce((acc2, kvp) => acc2 + parseInt(kvp[1]), acc)
      , 0)

      const product
        = cart == null
          ? computeTour(tour, value).product
          : cart.addTour(tour, value, true)

      const totalPrice
          = product == null
            ? null
            : product.items
                .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))

      const totalDeposit
        = product == null
          ? null
          : product.items
              .reduce((acc, item) => acc.add(Big(item.deposit).times(item.quantity)), Big(0))

      const invoiceItemsGrouped = product == null ? {} : product.items.reduce((acc, line) => {
        if (acc[line.sub_header] == null) acc[line.sub_header] = []
        acc[line.sub_header].push(line)
        return acc
      }, {})

      // not sure how to enable Object.entries in babel
      const invoiceEntries = []
      for (var i in invoiceItemsGrouped) {
        invoiceEntries.push([i, invoiceItemsGrouped[i]])
      }

      const allowRooms  = tour.price_type === 'ALL' || tour.price_type.indexOf('SGL') !== -1

      var tourOptionsWithCheapestComputed

      if (cart != null && tour != null) {
        tourOptionsWithCheapestComputed = tour.options.map(option => {
          option = Object.assign({}, option)
          option['_cheapest_computed'] =
            mapTourOptionDepartures(option, (departure, departureDate) => { //compute price for each departure

              if (departure != null && departure.slots_taken >= departure.slots_total) return null

              const product = cart.addTour(tour, {
                tour_id: tour.id,
                option_id: option.id,
                departure_date: departureDate,
                rooms: priceComputationRooms
              }, true)

              if (product == null) return null
              const beforeDiscounts = product.items
                .filter((item) => parseInt(item['price']) > 0)
                .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
                .div(priceComputationDivisor)
              const afterDiscounts = product.items
                .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
                .div(priceComputationDivisor)
              const afterDiscountsDeposit = product.items
                .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
                .div(priceComputationDivisor)
              return {
                beforeDiscounts, afterDiscounts, afterDiscountsDeposit, product, departureDate
              }
            })
          // filter out invalid departures
          .filter((val) => val != null)

          // find the cheapest
          .reduce((acc, computed) => {
            if (acc == null || computed.afterDiscounts < acc.afterDiscounts) {
              return computed
            }
            return acc
          }, null)

          return option
        })

      }

      const selectedOptionDateInfoMap: {[key: string]: SelectedOptionDayInfo} = {}

      if (selectedOption != null && cart != null && tour != null) {
        const runningDate = new Date(selectedOption['_next_departure'])
        const endDate = new Date(selectedOption['_last_departure'])
        const departure_datesIndexed = selectedOption.departures.reduce(
          (acc, departure) => {
            acc[departure.date] = departure;
            return acc;
          }, {})

        while (endDate >= runningDate) {
          const isoDate = dateToIsoDate(runningDate)
          const departure = departure_datesIndexed[isoDate]

          // no departure or departure full
          const disabled = selectedOption.on_demand_advance_booking === '0'
            ? (departure == null || parseInt(departure.slots_taken) >= parseInt(departure.slots_total))
            : (isoDate < selectedOption['_next_departure'] || isoDate > selectedOption['_last_departure'])

          const noDeparture = (selectedOption.on_demand_advance_booking === '0' && departure == null) || (selectedOption.on_demand_advance_booking !== '0' && disabled)

          const product = cart.addTour(tour, {
              tour_id: tour.id,
              option_id: selectedOption.id,
              departure_date: isoDate,
              rooms: priceComputationRooms
            }, true)

          const beforeDiscounts = product.items
            .filter((item) => parseInt(item['price']) > 0)
            .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
            .div(priceComputationDivisor)

          const afterDiscounts = product.items
            .reduce((acc, item) => acc.add(Big(item.price).times(item.quantity)), Big(0))
            .div(priceComputationDivisor)

          const slotsRemaining
            = selectedOption.on_demand_advance_booking === '0' && departure != null
              ? departure.slots_total - departure.slots_taken
              : null

          selectedOptionDateInfoMap[isoDate] = {noDeparture, disabled, date: new Date(runningDate.getTime()), afterDiscounts, beforeDiscounts, slotsRemaining, product}

          runningDate.setDate(runningDate.getDate() + 1)
        }
      }

      const context: TourBookingContext = Object.assign({}, this.props, {tourOptionsWithCheapestComputed, tour, value, onChange, onSubmit, cart, selectedOptionDateInfoMap, allowRooms, selectedOption, totalPrice, totalDeposit, invoiceEntries})

      return <WrappedComponent {...context} />
    }
  }
}

export const TourBookingForm = createTourBookingForm(
  class extends React.Component<TourBookingContext> {
    state = {
      visible: false
    }

    handleVisibleChange = (visible) => {
      this.setState({ visible });
    }

    hide = () => {
      this.setState({visible: false})
    }

    render() {
      const {tour, value, onChange, selectedOptionDateInfoMap, allowRooms, selectedOption, totalPrice, invoiceEntries, tourOptionsWithCheapestComputed} = this.props

      const stepStatus = (stepNum: number, current: number) => {
        if (current < stepNum) return 'wait'
        if (current > stepNum) return 'finish'
        return 'process'
      }

      if (tour == null || tour.options.length === 0) {
        return <div style={{textAlign: 'center', padding: 64}}>Departure details coming soon.<br />Please check back later.</div>
      }

      // not sure why but the grid={gutter: 16} doesn't work on List
      // we pad List.Item manually here
      var roomList = []
      const optionSelected = tour.options.find((option) => option.id === value.option_id)
      const departureSelected = optionSelected == null ? null : optionSelected.departures.find((departure) => departure.date === value.departure_date)
      const availableSlots = departureSelected == null ? null : parseInt(departureSelected.slots_total) - parseInt(departureSelected.slots_taken)
      const currentTotal = value.rooms.reduce((acc, room) => {
        if (room.adult != null) acc += parseInt('' + room.adult)
        if (room.child_with_bed != null) acc += parseInt('' + room.child_with_bed)
        if (room.child_no_bed != null) acc += parseInt('' + room.child_no_bed)
        return acc
      }, 0)

      const isFull = currentTotal >= availableSlots

      if (optionSelected != null) {
        roomList = value.rooms.map((item, index) =>
          <List.Item style={{padding: 8}}>
            <RoomForm
              tour={tour}
              value={item}
              index={index}
              isFull={isFull}
              numberOfSiblings={value.rooms.length}
              deleteSelf={()=>{
                value.rooms.splice(index, 1)
                onChange(Object.assign({}, value))
              }}
              onChange={(formChanges) => {
                const changes = extractValueFromFormState(formChanges)
                value.rooms[index] = Object.assign({}, value.rooms[index], changes)
                onChange(Object.assign({}, value))}} /></List.Item>)

        // there's no way to align height using List, so we generate a dummy RoomForm for the 'add room' button
        if (value.rooms.length < 4 && allowRooms) roomList.push(
          <List.Item style={{padding: 8}}>
              <RoomForm className="tc-blur-hide-inputs" tour={tour} title="New room" />
              <Button
                type="primary"
                style={{position: 'absolute', top: '50%', width: '70%', left: '15%'}}
                onClick={
                  () => {
                    value.rooms.push({adult: 1})
                    onChange(Object.assign({}, value))}}>
                Add room
              </Button>
          </List.Item>)
      }

      var currentStep
      if (value.option_id == '') currentStep = 1
      else if (value.departure_date == '') currentStep = 2
      else currentStep = 3
      return <Form layout="horizontal" onSubmit={this.props.onSubmit} className="tc-tour-form">
        <div className="tc-with-border-bottom tc-steps-clickable">
          <Steps size="small" style={{padding: 32}}>
            <Steps.Step status={stepStatus(1, currentStep)} onClick={() => onChange(Object.assign({}, value, {option_id: '', departure_date: ''}))} title="Tour options" description={selectedOption != null ? selectedOption.name : ''} />
            <Steps.Step status={stepStatus(2, currentStep)} title="Departure dates" onClick={() => onChange(Object.assign({}, value, {departure_date: ''}))}  description={value.departure_date} />
            <Steps.Step status={stepStatus(3, currentStep)} title="Travellers" />
          </Steps>
        </div>
        { currentStep === 1
          &&  <List
          size="large"
          grid={{ xs: 1, sm: 1, md: 1, lg: 2, xl: 2, xxl: 2 }}
          dataSource={tourOptionsWithCheapestComputed || tour.options}
          renderItem={option => {
            return <List.Item key={option.id} style={{lineHeight: '24px', padding: '24px 30px'}}>
              <h2>{option.name}</h2>
              { option['_next_departure'] != null && <div>Next departure on <b>{option['_next_departure']}</b></div>}
              { option['_cheapest_computed'] == null
                ? <div>Prices from {option['TWN']}</div>
                : (option['_cheapest_computed'].beforeDiscounts.eq(option['_cheapest_computed'].afterDiscounts)
                    ? <div>Prices from <b>${option['_cheapest_computed'].beforeDiscounts.toFixed(0)}</b></div>
                    : <div>Prices from <span style={{textDecoration: 'line-through'}}>${option['_cheapest_computed'].beforeDiscounts.toFixed(0)}</span> <b>${option['_cheapest_computed'].afterDiscounts.toFixed(0)}</b></div>
                )}
              { option['_next_departure'] == null
                ? <Button style={{marginTop: 6}} disabled>No departures available</Button>
                : <Button style={{marginTop: 6}} type="primary" onClick={() => onChange(Object.assign({}, value, {option_id: option.id}))}>View departures <Icon type="right" /></Button>}
            </List.Item>}}/>
        }

        { currentStep === 2
          &&  <Calendar
                className="tour-calendar"
                validRange={[new Date(selectedOption['_next_departure']), new Date(selectedOption['_last_departure'])]}
                CalendarDayGenerator={(date, isPadding, key) => {
                  const isoDate = dateToIsoDate(date)

                  // if a month does not end on a sat, additional days are drawn to pad out the week
                  // if isPadding, we pretned there's no dayInfo and don't display any info
                  const dayInfo = isPadding ? {} : selectedOptionDateInfoMap[isoDate] || {} as any
                  const {disabled = true, afterDiscounts, noDeparture = true, slotsRemaining} = dayInfo

                  return <td role="gridcell" key={key}
                    className={"ant-fullcalendar-cell" + (disabled ? " ant-fullcalendar-disabled-cell" : "")}
                    onClick={() => disabled === false && onChange(Object.assign({}, value, {departure_date: isoDate}))}>
                    <div className="ant-fullcalendar-date">
                      <div className="ant-fullcalendar-value">{date.getDate()}</div>
                      { noDeparture
                        ? <div className="ant-fullcalendar-content"></div>
                        : <div className="ant-fullcalendar-content">
                            <div>{formatCurrency(afterDiscounts)}</div>
                            {slotsRemaining != null && <div>{slotsRemaining} available</div>}
                          </div>}
                    </div>
                  </td>}}
                />}

        { currentStep === 3
          && <div style={{position: 'relative'}}>
                <List
                  grid={{ column: allowRooms ? 4 : 1 }}
                  dataSource={roomList}
                  renderItem={item => item} />
                <div className="tc-with-border-top" style={{position: 'absolute', bottom: 0, padding: 20, width: '100%'}}>
                  <div style={{float: 'left', fontSize: 18, lineHeight: "32px"}}>
                    Total: <b>{formatCurrency(totalPrice)}</b>
                    <Popover
                      content={
                        <table style={{width: 400}}><tbody>{invoiceEntries.reduce(
                          (acc, group, groupIndex) => {
                            acc.push(<tr key={groupIndex}><th style={{paddingTop: 8}} colSpan={2}>{group[0]}</th></tr>)
                            for(var i in group[1]) {
                              const line = group[1][i]
                              acc.push(<tr key={groupIndex + " " + i}><td style={{width: "99%"}}>{line.name} x{line.quantity}</td><td>${Big(line.price).times(line.quantity).toFixed(2).toString()}</td></tr>)
                            }
                            return acc
                          }, [])}
                        </tbody></table>}
                      title="Price Breakdown"
                      trigger="click"
                      visible={this.state.visible}
                      onVisibleChange={this.handleVisibleChange}>
                      <a style={{color: '#999', marginLeft: 32}}><Icon type="info-circle-o" /> Details</a>
                    </Popover>
                  </div>
                  <Button type="primary" htmlType="submit" style={{float: 'right'}}>Book now</Button>
                </div>
              </div>}
      </Form>
    }
  }
);

export const RoomForm = Form.create<{tour, value?: Room, onChange?}>({
  onFieldsChange(props, changedFields) {
    if (props.onChange != null) props.onChange(changedFields)
  },
  mapPropsToFields(props) {
    const value = props.value || {adult: 1}
    return {
      adult: Form.createFormField(
        value.adult != null ? {value: value.adult} : {value: 1}
      ),
      child_with_bed: Form.createFormField(
        value.child_with_bed != null ? {value: value.child_with_bed} : {value: 0}
      ),
      child_no_bed: Form.createFormField(
        value.child_no_bed != null ? {value: value.child_no_bed} : {value: 0}
      ),
      infant: Form.createFormField(
        value.infant != null ? {value: value.infant} : {value: 0}
      )
    };
  }
})((props: {index?, form, tour, deleteSelf?, numberOfSiblings?, title?, value?: Room, onChange?, className?, style?, isFull?}) => {
  const isFull = props.isFull == null ? false : props.isFull
  const maxBeds = parseInt(props.tour['max_beds'])
  const maxAdults = parseInt(props.tour['max_adults'])
  const maxChildrenNoBed = parseInt(props.tour['max_children_no_bed'])
  const maxInfants = parseInt(props.tour['max_infants'])

  const allowRooms  = props.tour.price_type === 'ALL' || props.tour.price_type.indexOf('SGL') !== -1
  const allowChildBed = props.tour.price_type === 'ALL' || props.tour.price_type.indexOf('CWB') !== -1
  const allowChildNoBed = props.tour.price_type === 'ALL' || allowRooms && allowChildBed && props.tour.price_type.indexOf('CNB') !== -1 && maxChildrenNoBed > 0
  const allowInfant = allowRooms && maxInfants > 0 && (props.tour.price_type === 'ALL' || props.tour.price_type.indexOf('INF') !== -1)

  const adult = props.form.getFieldValue('adult')
  const childBed = props.form.getFieldValue('child_with_bed')
  const childNoBed = props.form.getFieldValue('child_no_bed')
  const infant = props.form.getFieldValue('infant')

  const canAddAdult = isFull === false && (allowRooms === false || (adult < maxAdults && (adult + childBed) < maxBeds))
  const canAddChildBed = isFull === false &&  (allowRooms === false || ((adult + childBed) < maxBeds))
  const canAddChildNoBed = isFull === false &&  (childNoBed < maxChildrenNoBed && (adult + childBed) >= 2)
  const canAddInfant = infant < maxInfants
  const canSubtractChildBed = childBed > 0 && (childBed > 1 || childNoBed === 0)

  const autoTitle = allowRooms ? "Room " + (props.index + 1) : "Travellers"

  return <Card
    className={"tc-room-form " + props.className}
    title={props.title || autoTitle}
    extra={props.numberOfSiblings > 1 && <a onClick={props.deleteSelf}>Delete</a>}
    style={props.style}>
    <Form.Item label="Adults">
      <IncDecInput name="adult" form={props.form} min={1} max={9} allowAdd={canAddAdult} />
    </Form.Item>
    { allowChildBed
      &&  <Form.Item label="Children">
            <IncDecInput name="child_with_bed" form={props.form} min={0} max={9} allowAdd={canAddChildBed} allowSubtract={canSubtractChildBed} />
          </Form.Item>}
    { allowChildNoBed
      &&  <Form.Item label="Children (sharing bed)">
            <IncDecInput name="child_no_bed" form={props.form} min={0} max={9} allowAdd={canAddChildNoBed} />
          </Form.Item>}
    { allowInfant
      &&  <Form.Item label="Infants">
            <IncDecInput name="infant" form={props.form} min={0} max={9} allowAdd={canAddInfant} />
          </Form.Item>}
  </Card>
})