"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("isomorphic-unfetch");
var big_js_1 = __importDefault(require("big.js"));
var moment_1 = __importDefault(require("moment"));
var order_product_compute_1 = require("./order-product-compute");
var react_1 = __importDefault(require("react"));
function postRedirect(post) {
    var f = document.createElement('form');
    f.action = post.action;
    f.method = 'POST';
    for (var _i = 0, _a = post.form; _i < _a.length; _i++) {
        var input = _a[_i];
        var i = document.createElement('input');
        i.type = 'hidden';
        i.name = input.name;
        i.value = input.value;
        f.appendChild(i);
    }
    document.body.appendChild(f);
    f.submit();
}
exports.setupUserback = function (token) {
    if (token === void 0) { token = '4386|6572|8OWEwPsjAdNcLi51kGOXy3CKGebHwlJ2sEcrPYy9SEPvizoqNF'; }
    if (window.location.hostname.indexOf('.travel.click') === -1)
        return;
    var window2 = window;
    var id = 'userback-sdk';
    var Userback = window2.Userback || {};
    Userback.access_token = token;
    window2.Userback = Userback;
    if (document.getElementById(id)) {
        return;
    }
    var s = document.createElement('script');
    s.id = id;
    s.async = true;
    s.src = 'https://static.userback.io/widget/v1.js';
    var parent_node = document.head || document.body;
    parent_node.appendChild(s);
};
exports.setupGtag = function (Router, config) {
    var window2 = window;
    var travelClickTrackingId = 'UA-132985038-1';
    if (window == null || window2.dataLayer != null)
        return;
    window2.dataLayer = [];
    window2.gtag = function () { window2.dataLayer.push(arguments); };
    // prevent logging localhost byt leave the gtag stub there
    if (window.location.hostname === 'localhost')
        return;
    window2.gtag('js', new Date());
    window2.gtag('config', travelClickTrackingId, {
        'dimension1': config.tcUser
    });
    if (config.googleAnalyticsTrackingId != null && config.googleAnalyticsTrackingId.trim() !== "") {
        window2.gtag('config', config.googleAnalyticsTrackingId.trim());
    }
    var tag = document.createElement('script');
    tag.src = "https://www.googletagmanager.com/gtag/js?id=" + (config.googleAnalyticsTrackingId || 'UA-132985038-1');
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var lastTrackedUrl = null;
    Router.events.on('routeChangeComplete', function (newUrl) {
        if (newUrl === lastTrackedUrl || window2.gtag == null) {
            return;
        }
        // Don't double track the same URL
        lastTrackedUrl = newUrl;
        window2.gtag('config', travelClickTrackingId, {
            page_path: newUrl,
            'dimension1': config.tcUser
        });
        if (config.googleAnalyticsTrackingId != null && config.googleAnalyticsTrackingId.trim() !== "") {
            window2.gtag('config', config.googleAnalyticsTrackingId.trim(), {
                page_path: newUrl
            });
        }
    });
};
function redirectToPayment(options) {
    return __awaiter(this, void 0, void 0, function () {
        var client, success, failure, host, prefix, payload, post, params;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    client = new TravelCloudClient(options.tcUser);
                    success = options.successPage.replace(/^\/+/g, '');
                    failure = options.failurePage.replace(/^\/+/g, '');
                    host = location.host;
                    prefix = location.protocol + '//' + location.host;
                    if (!(options.paymentProcessor === 'paypal')) return [3 /*break*/, 2];
                    payload = {
                        order_ref: options.orderRef,
                        cancel: prefix + location.pathname + location.search,
                        success: prefix + '/' + success,
                        failure: prefix + '/' + success,
                        payment_processor: options.paymentProcessor,
                        amount: options.amount
                    };
                    return [4 /*yield*/, client.post('rpc/payment_redirect', null, payload)];
                case 1:
                    post = _a.sent();
                    postRedirect(post.result);
                    return [2 /*return*/];
                case 2:
                    params = {
                        ref: options.orderRef,
                        tcUser: options.tcUser,
                        amount: options.amount,
                        origin: host,
                        return: location.pathname + location.search,
                        success: success, failure: failure
                    };
                    window.location.href
                        = "https://" + options.domain + "/payment/" + options.paymentProcessor + '?'
                            + urlParam(params);
                    return [2 /*return*/];
            }
        });
    });
}
exports.redirectToPayment = redirectToPayment;
function urlParam(params) {
    if (params == null)
        return '';
    return Object.keys(params)
        .filter(function (k) { return params[k] !== ''; })
        .map(function (k) { return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]); })
        .join('&');
}
function hasPrefix(key, prefixes, target) {
    var result = prefixes.find(function (prefix) { return key.indexOf(prefix) === 0; });
    if (result == null)
        console.warn("Invalid param " + key + " for " + target);
    return result != null;
}
var TravelCloudClient = /** @class */ (function () {
    //todo: add customer token
    function TravelCloudClient(tcUser) {
        this.cache = {};
        this.tcUser = encodeURIComponent(tcUser);
    }
    TravelCloudClient.prototype.get = function (endpoint, params, noCache) {
        var _this = this;
        if (noCache === void 0) { noCache = false; }
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var query, currentTime, cache, res, resJson, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        query = params == null ? '' : '?' + urlParam(params);
                        currentTime = Date.now();
                        if (typeof window === 'undefined') {
                            cache = this.cache[endpoint + '/' + query];
                            if (cache == null) {
                                cache = {
                                    response: null,
                                    time: 0,
                                    resolves: []
                                };
                                this.cache[endpoint + '/' + query] = cache;
                            }
                        }
                        else {
                            // store cache on window so that cache survives even when react component crashes
                            if (window.travelCloudClientCache == null)
                                window.travelCloudClientCache = {};
                            cache = window.travelCloudClientCache[endpoint + '/' + query];
                            if (cache == null) {
                                cache = {
                                    response: null,
                                    time: 0,
                                    resolves: []
                                };
                                window.travelCloudClientCache[endpoint + '/' + query] = cache;
                            }
                        }
                        // cached response available
                        if (noCache === false && cache.time + 30000 > currentTime) {
                            resolve(cache.response);
                            return [2 /*return*/];
                        }
                        cache.resolves.push(resolve);
                        // another request taking place
                        if (cache.time === -1) {
                            return [2 /*return*/];
                        }
                        cache.time = -1;
                        return [4 /*yield*/, fetch("https://" + this.tcUser + ".travelcloud.app/" + endpoint + query)
                            //const res = await fetch(`http://localhost:9000/${endpoint}${query}`)
                        ];
                    case 1:
                        res = _a.sent();
                        return [4 /*yield*/, res.json()];
                    case 2:
                        resJson = _a.sent();
                        cache.response = resJson;
                        cache.time = currentTime;
                        for (i in cache.resolves) {
                            cache.resolves[i](resJson);
                        }
                        cache.resolves = [];
                        return [2 /*return*/];
                }
            });
        }); });
    };
    TravelCloudClient.prototype.cancelPendingGetRequests = function (endpoint) {
        if (typeof window === 'undefined') {
            for (var cacheKey in this.cache) {
                if (cacheKey.indexOf(endpoint) !== 0)
                    continue;
                var entry = this.cache[cacheKey];
                for (var index in entry.resolves) {
                    entry.resolves[index]({});
                }
                entry.resolves.length = 0;
            }
        }
        else {
            if (window.travelCloudClientCache == null)
                return;
            for (var cacheKey in window.travelCloudClientCache) {
                if (cacheKey.indexOf(endpoint) !== 0)
                    continue;
                var entry = window.travelCloudClientCache[cacheKey];
                for (var index in entry.resolves) {
                    entry.resolves[index]({});
                }
                entry.resolves.length = 0;
            }
        }
    };
    TravelCloudClient.prototype.post = function (endpoint, params, data) {
        return __awaiter(this, void 0, void 0, function () {
            var query;
            return __generator(this, function (_a) {
                query = params == null ? '' : '?' + urlParam(params);
                return [2 /*return*/, fetch("https://" + this.tcUser + ".travelcloud.app/" + endpoint + query, {
                        //return fetch(`http://localhost:9000/${endpoint}${query}`, {
                        body: JSON.stringify(data),
                        cache: 'no-cache',
                        headers: {
                            'content-type': 'application/json'
                        },
                        method: 'POST',
                        mode: 'cors',
                    })
                        .then(function (response) { return response.json(); })]; // parses response to JSON
            });
        });
    };
    TravelCloudClient.prototype.autoComplete = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (Array.isArray(params.values))
                            params.values = params.values.join(',');
                        return [4 /*yield*/, this.get('rpc/autocomplete', params)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TravelCloudClient.prototype.priceRules = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return ['is_public'].indexOf(key) !== -1; })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/price_rules', params)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.tours = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return hasPrefix(key, ['~boolean', 'categories.name'], 'tours'); })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/tours', params)];
                    case 1:
                        json = _a.sent();
                        json.result = json.result.map(function (tour) {
                            tour['_next_departure'] = nextDeparture(tour.options);
                            tour['_cheapest'] = cheapest(tour.options);
                            tour.options = tour.options.map(function (option) {
                                if (option['_subset_departures'] == null)
                                    option['_subset_departures'] = option['departures'];
                                if (option['departures'] == null)
                                    option['departures'] = option['_subset_departures'];
                                return option;
                            });
                            return tour;
                        });
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.generics = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return hasPrefix(key, ['categories.name'], 'generics'); })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/generics', params)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.generic = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return ['id'].indexOf(key) !== -1; })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/generic', params)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.documents = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return hasPrefix(key, ['categories.name'], 'documents'); })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/documents', params)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.document = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return ['id'].indexOf(key) !== -1; })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/document', params)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.tour = function (paramsRaw) {
        return __awaiter(this, void 0, void 0, function () {
            var params, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = Object.keys(paramsRaw)
                            .filter(function (key) { return ['id'].indexOf(key) !== -1; })
                            .reduce(function (res, key) { return (res[key] = paramsRaw[key], res); }, {});
                        return [4 /*yield*/, this.get('api/tour', params)];
                    case 1:
                        json = _a.sent();
                        json.result['_next_departure'] = nextDeparture(json.result.options);
                        json.result['_cheapest'] = cheapest(json.result.options);
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.globaltix = function (endpoint, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.get('globaltix/' + endpoint, params)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.cancelFlights = function () {
        this.cancelPendingGetRequests('rpc/flight_search_new');
    };
    TravelCloudClient.prototype.flights = function (params, sample) {
        if (sample === void 0) { sample = false; }
        return __awaiter(this, void 0, void 0, function () {
            var converted, path, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        converted = exports.validateFlightsParams(params);
                        if (converted == null)
                            return [2 /*return*/, {}];
                        path = sample ? 'sample/flight_search.json' : 'rpc/flight_search_new';
                        return [4 /*yield*/, this.get(path, converted)
                            //const json = await this.get('flight.json', converted)
                        ];
                    case 1:
                        json = _a.sent();
                        //const json = await this.get('flight.json', converted)
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.flight = function (params, sample) {
        if (sample === void 0) { sample = false; }
        return __awaiter(this, void 0, void 0, function () {
            var converted, path, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        converted = exports.validateFlightParams(params);
                        if (converted == null)
                            return [2 /*return*/, {}];
                        path = sample ? 'sample/flight_detail.json' : 'rpc/flight_detail';
                        return [4 /*yield*/, this.get(path, converted)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.legacyHotels = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var converted, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        converted = exports.validateLegacyHotelsParams(params);
                        if (converted == null)
                            return [2 /*return*/, {}];
                        return [4 /*yield*/, this.get('rpc/legacy_hotel_search', converted)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.legacyHotel = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var converted, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        converted = exports.validateLegacyHotelParams(params);
                        if (converted == null)
                            return [2 /*return*/, {}];
                        return [4 /*yield*/, this.get('rpc/legacy_hotel_detail', converted)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    TravelCloudClient.prototype.order = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.get('api/order', params, true)];
            });
        });
    };
    TravelCloudClient.prototype.categories = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.get('api/categories', params)];
            });
        });
    };
    TravelCloudClient.prototype.submitContactForm = function (structured, unstructured) {
        return __awaiter(this, void 0, void 0, function () {
            var referral_site, subject, customer_name, customer_email, payload;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        referral_site = structured.referral_site, subject = structured.subject, customer_name = structured.customer_name, customer_email = structured.customer_email;
                        payload = {
                            referral_site: referral_site, subject: subject,
                            '~customer_add_unstructured_message': {
                                unstructured: unstructured, customer_name: customer_name, customer_email: customer_email
                            }
                        };
                        return [4 /*yield*/, this.post('api/conversation', {}, payload)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    return TravelCloudClient;
}());
exports.TravelCloudClient = TravelCloudClient;
exports.validateFlightsParams = function (state) {
    if (state == null)
        return null;
    if (state['od1.origin_airport.code'] == null)
        return null;
    if (state['od2.origin_airport.code'] == null)
        return null;
    if (state['od1.origin_datetime'] == null)
        return null;
    if (state.ptc_adt == null)
        return null;
    var filtered = {
        source: state.source,
        'od1.origin_airport.code': state['od1.origin_airport.code'],
        'od2.origin_airport.code': state['od2.origin_airport.code'],
        'od1.destination_airport.code': state['od1.destination_airport.code'],
        'od2.destination_airport.code': state['od2.destination_airport.code'],
        'od1.origin_datetime': state['od1.origin_datetime'],
        'od2.origin_datetime': state['od2.origin_datetime'],
        ptc_adt: state.ptc_adt,
        ptc_cnn: state.ptc_cnn,
        ptc_inf: state.ptc_inf,
        cabin: state.cabin || 'Y'
    };
    if (filtered['od2.origin_datetime'] == null)
        delete filtered['od2.origin_datetime'];
    if (filtered['od1.destination_airport.code'] == null)
        delete filtered['od1.destination_airport.code'];
    if (filtered['od2.destination_airport.code'] == null)
        delete filtered['od2.destination_airport.code'];
    return filtered;
};
exports.validateFlightParams = function (state) {
    if (state == null)
        return null;
    if (state['od1.id'] == null)
        return null;
    if (state.ptc_adt == null)
        return null;
    var filtered = {
        source: state.source,
        'od1.id': state['od1.id'],
        'od2.id': state['od2.id'],
        ptc_adt: state.ptc_adt,
        ptc_cnn: state.ptc_cnn,
        ptc_inf: state.ptc_inf,
    };
    if (filtered['od2.id'] == null)
        delete filtered['od2.id'];
    return filtered;
};
exports.validateLegacyHotelParams = function (state) {
    if (state == null)
        return null;
    if (state.hotelId == null)
        return null;
    if (state.checkInDate == null)
        return null;
    if (state.checkOutDate == null)
        return null;
    if (state.adults == null)
        return null;
    return {
        hotelId: state.hotelId,
        checkInDate: moment_1.default(state.checkInDate).format('YYYY-MM-DD'),
        duration: moment_1.default(state.checkOutDate).diff(state.checkInDate, 'days'),
        adults: state.adults,
        children: state.children || 0
    };
};
exports.validateLegacyHotelsParams = function (state) {
    if (state == null)
        return null;
    if (state.cityCode == null)
        return null;
    if (state.checkInDate == null)
        return null;
    if (state.checkOutDate == null)
        return null;
    if (state.adults == null)
        return null;
    return {
        cityCode: state.cityCode,
        checkInDate: moment_1.default(state.checkInDate).format('YYYY-MM-DD'),
        duration: moment_1.default(state.checkOutDate).diff(state.checkInDate, 'days'),
        adults: state.adults,
        children: state.children || 0
    };
};
// mutates each option to include _next_departure
function nextDeparture(options) {
    if (options.length === 0)
        return null;
    for (var index in options) {
        var option = options[index];
        var departuresOrSubset = option.departures || option._subset_departures;
        if (option['on_demand_advance_booking'] === '0') {
            option['_next_departure'] = departuresOrSubset.reduce(function (acc2, departure) {
                return parseInt(departure.slots_total) > parseInt(departure.slots_taken)
                    ? (acc2 == null || acc2 > departure.date ? departure.date : acc2)
                    : acc2;
            }, null);
            option['_last_departure'] = departuresOrSubset.reduce(function (acc2, departure) {
                return parseInt(departure.slots_total) > parseInt(departure.slots_taken)
                    ? (acc2 == null || acc2 < departure.date ? departure.date : acc2)
                    : acc2;
            }, null);
        }
        else {
            var todayDate = (new Date()).getDate();
            var minDate = new Date();
            var maxDate = new Date();
            minDate.setDate(todayDate + parseInt(option['min_lead_time']));
            maxDate.setDate(todayDate + parseInt(option['on_demand_advance_booking']));
            option['_next_departure'] = dateToIsoDate(minDate);
            option['_last_departure'] = dateToIsoDate(maxDate);
        }
    }
    return options.reduce(function (acc, option) {
        return acc === false || acc > option['_next_departure'] ? option['_next_departure'] : acc;
    }, false);
}
exports.nextDeparture = nextDeparture;
function cheapest(options) {
    return options.reduce(function (acc, price) { return acc == null || acc > price.TWN ? price.TWN : acc; }, null);
}
function dateToIsoDate(date) {
    var pad = function (x) { return x < 10 ? '0' + x : x; };
    return "" + date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
}
exports.dateToIsoDate = dateToIsoDate;
function extractValueFromFormState(form) {
    var acc = {};
    for (var key in form) {
        if (form.hasOwnProperty(key)) {
            var val = form[key].value;
            // typeof Array is also object
            if (Array.isArray(val)) {
                // assume array of objects
                // not sure how to distinguish between array of objects and array of primitives
                acc[key] = val.map(function (item) { return extractValueFromFormState(item); });
            }
            else if (typeof val === 'object') {
                acc[key] = extractValueFromFormState(val);
            }
            else {
                acc[key] = val;
            }
        }
    }
    return acc;
}
exports.extractValueFromFormState = extractValueFromFormState;
// mutate existing to avoid re-rendering form in pure components
// only works for values and array of objects
function mergeFormState(existing, updates) {
    if (updates == null)
        return existing;
    for (var key in updates) {
        var val = updates[key];
        if (Array.isArray(val)) {
            for (var index in val) {
                existing[key][index] = mergeFormState(existing[key][index], val[index]);
            }
        }
        else {
            existing[key] = val;
        }
    }
    return existing;
}
exports.updateFormState = function (rc, key) {
    return function (changes) {
        var _a;
        rc.setState((_a = {}, _a[key] = mergeFormState(rc.state[key], changes), _a));
    };
};
function getStateFromLocalStorage() {
    var order = {};
    var customer = {};
    var token = null;
    var localStorageAvailable = false;
    try {
        localStorage.setItem(Cart.localStorageTestKey, Cart.localStorageTestKey);
        localStorage.removeItem(Cart.localStorageTestKey);
        order = JSON.parse(localStorage.getItem(Cart.localStorageOrderKey));
        customer = JSON.parse(localStorage.getItem(Cart.localStorageCustomerKey));
        token = localStorage.getItem(Cart.localStorageTokenKey);
        localStorageAvailable = true;
    }
    catch (e) { }
    var result = {
        order: { result: order }, customer: { result: customer }, token: token,
        localStorageAvailable: localStorageAvailable,
    };
    return result;
}
exports.getStateFromLocalStorage = getStateFromLocalStorage;
function sleep(ms) {
    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
}
exports.sleep = sleep;
// Probably should reimplement as context provider when it is fixed
// https://github.com/zeit/next.js/issues/4194
var Cart = /** @class */ (function () {
    // for order/customer
    // null - server-side, not initialized
    // {} - nothing to show/empty
    function Cart(options) {
        this.demo = false;
        this.priceRules = {};
        this.bcryptCache = {};
        this.priceRulesLoaded = false;
        this.client = options.client;
        this.onOrderChange = options.onOrderChange;
        this.onCustomerChange = options.onCustomerChange;
        this.onPriceRulesChange = options.onPriceRulesChange;
        var restored = getStateFromLocalStorage();
        this.token = restored.token;
        this.updateCustomer(restored['customer']);
        this.updateOrder(restored['order']);
    }
    // for testing
    Cart.prototype.saveOrderSnapshot = function () {
        var str = JSON.stringify(this.order);
        localStorage.setItem('orderSnapshotStr', str);
    };
    Cart.prototype.restoreOrderSnapshot = function () {
        var str = localStorage.getItem('orderSnapshotStr');
        this.updateOrder(JSON.parse(str));
    };
    Cart.prototype.getPassword = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        email = email.trim();
                        return [4 /*yield*/, this.client.post('api/customer', { email: email }, { '~send_login_password': { method: 'email' } })];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    Cart.prototype.login = function (email, login_password) {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        email = email.trim();
                        login_password = login_password.trim();
                        return [4 /*yield*/, this.client.post('api/customer', { email: email }, { '~get_login_token': { login_password: login_password } })];
                    case 1:
                        json = _a.sent();
                        if (json.result != null) {
                            //json.result['_discounts'] = json.result.categories.reduce((acc, cat) => Object.assign(acc, parseCategoryDescription(cat.description)), {})
                            this.updateCustomer(json);
                            this.token = json.result['_get_login_token'];
                            try {
                                localStorage.setItem(Cart.localStorageTokenKey, json.result['_get_login_token']);
                                localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(json.result));
                            }
                            catch (e) { }
                        }
                        else {
                            this.updateCustomer({});
                            try {
                                localStorage.removeItem(Cart.localStorageTokenKey);
                                localStorage.removeItem(Cart.localStorageCustomerKey);
                            }
                            catch (e) { }
                        }
                        return [2 /*return*/, json];
                }
            });
        });
    };
    Cart.prototype.payOrderWithCustomerCredit = function (ref) {
        return __awaiter(this, void 0, void 0, function () {
            var customer, login_token, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customer = this.customer;
                        login_token = this.token;
                        if (login_token == null || customer == null || customer.result == null)
                            return [2 /*return*/];
                        return [4 /*yield*/, this.client.post('api/customer', { email: customer.result.email }, {
                                '~authenticate_with_login_token': { login_token: login_token },
                                '~pay_order': { ref: ref }
                            })];
                    case 1:
                        json = _a.sent();
                        if (json.result != null) {
                            this.updateCustomer(json);
                            localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(json.result));
                        }
                        return [2 /*return*/, json];
                }
            });
        });
    };
    Cart.prototype.logout = function () {
        this.updateCustomer({});
        this.updateOrder({});
        this.token = null;
        localStorage.removeItem(Cart.localStorageTokenKey);
    };
    Cart.prototype.refreshCustomer = function () {
        return __awaiter(this, void 0, void 0, function () {
            var customer, login_token, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        customer = this.customer;
                        login_token = this.token;
                        if (login_token == null || customer == null || customer.result == null)
                            return [2 /*return*/];
                        return [4 /*yield*/, this.client.post('api/customer', { email: customer.result.email }, { '~authenticate_with_login_token': { login_token: login_token } })];
                    case 1:
                        json = _a.sent();
                        if (json.result != null) {
                            this.updateCustomer(json);
                            try {
                                localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(json.result));
                            }
                            catch (e) { }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Cart.prototype.reset = function () {
        this.updateOrder({});
        return this;
    };
    Cart.prototype.addTour = function (tour, tourFormValue, mock) {
        if (mock === void 0) { mock = false; }
        var computed = order_product_compute_1.computeTour(tour, tourFormValue);
        if (computed.product == null)
            return null;
        computed.product.form = tourFormValue;
        if (computed.total == null)
            return null;
        return this.addProduct(computed.product, mock);
    };
    Cart.prototype.addGlobaltix = function (item, globaltixFormValue, mock) {
        if (mock === void 0) { mock = false; }
        var computed = order_product_compute_1.computeGlobaltix(item, globaltixFormValue);
        if (computed.product == null)
            return null;
        computed.product.form = globaltixFormValue;
        return this.addProduct(computed.product, mock);
    };
    Cart.prototype.addLegacyHotel = function (details, legacyHotelFormValue, mock) {
        if (mock === void 0) { mock = false; }
        var computed = order_product_compute_1.computeLegacyHotel(details, legacyHotelFormValue);
        if (computed.product == null)
            return null;
        computed.product.form = legacyHotelFormValue;
        console.log(legacyHotelFormValue);
        return this.addProduct(computed.product, mock);
    };
    Cart.prototype.addFlight = function (flight, fareId, mock) {
        if (mock === void 0) { mock = false; }
        var selectedFare = flight.fares.find(function (x) { return x.id === fareId; });
        if (selectedFare == null)
            return null;
        var ptc_adt = selectedFare.ptc_breakdown_map['adt'].quantity;
        var ptc_cnn = selectedFare.ptc_breakdown_map['cnn'] != null ? selectedFare.ptc_breakdown_map['cnn'].quantity : 0;
        var ptc_inf = selectedFare.ptc_breakdown_map['inf'] != null ? selectedFare.ptc_breakdown_map['inf'].quantity : 0;
        var form = flight.od2 == null
            ? {
                od1_id: flight.od1.id,
                fare_id: fareId,
                ptc_adt: ptc_adt, ptc_cnn: ptc_cnn, ptc_inf: ptc_inf
            }
            : {
                od1_id: flight.od1.id,
                od2_id: flight.od2.id,
                fare_id: fareId,
                ptc_adt: ptc_adt, ptc_cnn: ptc_cnn, ptc_inf: ptc_inf
            };
        var computed = order_product_compute_1.computeFlight(flight, form);
        computed.product.form = form;
        if (computed.total == null)
            return null;
        return this.addProduct(computed.product, mock);
    };
    Cart.prototype.addGeneric = function (generic, params, mock) {
        if (mock === void 0) { mock = false; }
        var computed = order_product_compute_1.computeGeneric(generic, params);
        computed.product.form = params;
        if (computed.total == null)
            return null;
        return this.addProduct(computed.product, mock);
    };
    Cart.prototype.checkout = function (checkoutFormValue) {
        return __awaiter(this, void 0, void 0, function () {
            var order, oc, customer, login_token, addRemoveProducts, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        order = Object.assign({}, this.order.result || {}, checkoutFormValue);
                        oc = JSON.parse(JSON.stringify(order));
                        oc['~set_email'] = oc['email'];
                        delete oc['email'];
                        delete oc['payment_required'];
                        delete oc['deposit_required'];
                        customer = this.customer;
                        login_token = this.token;
                        if (login_token != null && customer != null && customer.result != null) {
                            oc['~customer_authenticate_with_login_token'] = {
                                email: customer.result.email,
                                login_token: login_token
                            };
                        }
                        addRemoveProducts = oc['products'].map(function (product) {
                            if (product.product_type === 'tour_package') {
                                return {
                                    tour: product.form
                                };
                            }
                            else if (product.product_type === 'flight') {
                                return {
                                    flight: product.form
                                };
                            }
                            else if (product.product_type === 'nontour_product') {
                                return {
                                    generic: product.form
                                };
                            }
                            else if (product.product_type === 'hotel_room') {
                                return {
                                    legacy_hotel: product.form
                                };
                            }
                            else if (product.product_type === 'hotel_room') {
                                return {
                                    legacy_hotel: product.form
                                };
                            }
                            else if (product.product_source === 'globaltix') {
                                return {
                                    globaltix: product.form
                                };
                            }
                            throw new Error("Unknown product type " + product.product_type);
                        });
                        delete oc['products'];
                        oc['~add_remove_products'] = addRemoveProducts;
                        return [4 /*yield*/, this.client.post("api/order", null, oc)];
                    case 1:
                        json = _a.sent();
                        if (json.result != null) {
                            this.updateOrder({});
                            try {
                                localStorage.removeItem(Cart.localStorageOrderKey);
                            }
                            catch (e) {
                                console.warn("Cannot save order details to localStorage");
                            }
                        }
                        return [2 /*return*/, json];
                }
            });
        });
    };
    Cart.prototype.empty = function () {
        this.updateOrder({});
        localStorage.removeItem(Cart.localStorageOrderKey);
        return true;
    };
    Cart.prototype.addProduct = function (product, mock) {
        if (mock === void 0) { mock = false; }
        var order = JSON.parse(JSON.stringify(this.order.result || {}));
        order['products'] = order['products'] || [];
        if (mock) {
            order['products'].push(product);
            if (this.priceRulesLoaded === false) {
                this.priceRulesLoaded = true;
                this.refreshPriceRules();
            }
            order_product_compute_1.computePriceRules(order, this.priceRules.result, this.bcryptCache, this.customer.result);
        }
        else {
            // if we find that an existing product already exists, we overwrite it
            // merging products is too complex due to pricing and availability
            var existingProductIndex = order['products'].findIndex(function (existingProduct) {
                return product.product_id === existingProduct.product_id
                    && product.product_type === existingProduct.product_type
                    && product.product_code === existingProduct.product_code
                    && product.product_source === existingProduct.product_source
                    && product.product_name === existingProduct.product_name;
            });
            if (existingProductIndex == -1) {
                if (product.quantity > 0)
                    order['products'].push(product);
                else
                    return null;
            }
            else {
                if (product.quantity <= 0) {
                    this.removeProductByIndex(existingProductIndex);
                    return null;
                }
                else
                    order['products'][existingProductIndex] = product;
            }
            this.updateOrder({ result: order });
        }
        // product will be mutated by computePriceRules
        return product;
    };
    Cart.prototype.removeProductByIndex = function (index) {
        var order = Object.assign({}, this.order.result || {});
        order['products'] = order['products'] || [];
        if (order['products'][index] != null)
            order.products.splice(index, 1);
        this.updateOrder({ result: order });
    };
    // utility for invoice where we group products by name
    Cart.prototype.removeProductByName = function (name) {
        var order = Object.assign({}, this.order.result || {});
        var products = order['products'] || [];
        order['products'] = products.filter(function (product) { return product.product_name !== name; });
        this.updateOrder({ result: order });
    };
    Cart.prototype.refreshPriceRules = function () {
        var _this = this;
        this.priceRules = { loading: true };
        if (this.onPriceRulesChange != null)
            this.onPriceRulesChange(this.priceRules);
        this.client.priceRules({ is_public: '1' })
            .then(function (priceRulesResults) {
            _this.priceRules = priceRulesResults;
            _this.updateOrder(_this.order);
            if (_this.onPriceRulesChange != null)
                _this.onPriceRulesChange(_this.priceRules);
        })
            .catch(function (err) {
            console.warn("Failed to fetch price rules", err);
        });
    };
    Cart.prototype.updateOrder = function (order) {
        if (this.priceRulesLoaded === false) {
            this.priceRulesLoaded = true;
            this.refreshPriceRules();
        }
        if (order.result != null && order.result.products != null) {
            order_product_compute_1.computePriceRules(order.result, this.priceRules.result, this.bcryptCache, this.customer.result);
            order.result['payment_required'] = order.result['products'].reduce(function (acc, product) {
                return product.items.reduce(function (acc2, item) { return acc2.add(big_js_1.default(item.price).times(item.quantity)); }, acc);
            }, big_js_1.default(0));
            order.result['deposit_required'] = order.result['products'].reduce(function (acc, product) {
                return product.items.reduce(function (acc2, item) { return acc2.add(big_js_1.default(item.deposit).times(item.quantity)); }, acc);
            }, big_js_1.default(0));
        }
        this.order = order;
        this.onOrderChange(order);
        try {
            if (order.result)
                localStorage.setItem(Cart.localStorageOrderKey, JSON.stringify(order.result));
            else
                localStorage.removeItem(Cart.localStorageOrderKey);
        }
        catch (e) {
            console.warn("Cannot save order details to localStorage");
        }
    };
    Cart.prototype.updateCustomer = function (customer) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.customer = customer;
                this.onCustomerChange(customer);
                try {
                    if (customer.result) {
                        if (this.order != null && this.order.result != null && this.order.result.products != null) {
                            this.updateOrder(this.order);
                        }
                        localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(customer.result));
                    }
                    else {
                        localStorage.removeItem(Cart.localStorageCustomerKey);
                    }
                }
                catch (e) {
                    console.warn("Cannot save order details to localStorage");
                    console.error(e);
                }
                return [2 /*return*/];
            });
        });
    };
    Cart.localStorageTestKey = '__LOCAL_STORAGE_TEST__';
    Cart.localStorageOrderKey = 'travelcloud-order';
    Cart.localStorageCustomerKey = 'travelcloud-customer';
    Cart.localStorageTokenKey = 'travelcloud-token';
    return Cart;
}());
exports.Cart = Cart;
/************
* Utilities *
*************/
function formatCurrency(x) {
    if (x == null)
        return '';
    var neg = parseFloat(x) < 0 ? '-' : '';
    return neg + '$' + big_js_1.default(x).abs().toFixed(2).toString();
}
exports.formatCurrency = formatCurrency;
// not sure how to enable Object.entries in babel
function objectEntries(obj) {
    var groups = [];
    for (var i in obj) {
        groups.push([i, obj[i]]);
    }
    return groups;
}
exports.objectEntries = objectEntries;
// will only work for array of forms, need some modifications to work for array of inputs
function formArrayWrapper(_a) {
    var RenderForm = _a.RenderForm, name = _a.name, form = _a.form;
    form.getFieldDecorator(name);
    var value = form.getFieldValue(name);
    if (value == null)
        value = [];
    if (!Array.isArray(value)) {
        console.warn("Expecting form state of " + name + " to be array", value);
        value = [];
    }
    var onChange = function (index) { return function (change) {
        var _a;
        value[index] = Object.assign(value[index], change);
        form.setFieldsValue((_a = {}, _a[name] = value, _a));
    }; };
    var deleteSelf = function (index) { return function () {
        var _a;
        value.splice(index, 1);
        form.setFieldsValue((_a = {}, _a[name] = value, _a));
    }; };
    return value.map(function (item, index) { return react_1.default.createElement(RenderForm, { key: index, index: index, numberOfSiblings: value.length, value: item, deleteSelf: deleteSelf(index), onChange: onChange(index) }); });
}
exports.formArrayWrapper = formArrayWrapper;
function mapTourOptionDepartures(option, func) {
    if (option['on_demand_advance_booking'] === '0') {
        return option.departures.map(function (departure) { return func(departure, departure.date); });
    }
    else {
        var todayDate = (new Date()).getDate();
        var runningDate = new Date();
        runningDate.setDate(todayDate + parseInt(option['min_lead_time']));
        var endDate = new Date();
        endDate.setDate(todayDate + parseInt(option['on_demand_advance_booking']));
        var result = [];
        while (runningDate < endDate) {
            result.push(func(null, dateToIsoDate(runningDate)));
            runningDate.setDate(runningDate.getDate() + 1);
        }
        return result;
    }
}
exports.mapTourOptionDepartures = mapTourOptionDepartures;
//# sourceMappingURL=travelcloud.js.map