"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var big_js_1 = __importDefault(require("big.js"));
var moment_1 = __importDefault(require("moment"));
var rules_compute_1 = __importDefault(require("./rules-compute"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
function computeTour(tour, tourFormValue) {
    var optionSelected = tour.options.find(function (option) { return option.id === tourFormValue.option_id; });
    if (optionSelected == null) {
        return {
            error: 'option_id ' + tourFormValue.option_id + ' not found'
        };
    }
    var departureSelected = optionSelected.departures.find(function (departure) { return departure.date === tourFormValue.departure_date; });
    // mutate tourFormValue to ensure everything is number
    // there is no way to enforce types at runtime
    tourFormValue.rooms = tourFormValue.rooms.map(function (room) { return ({
        adult: parseInt(String(room.adult || 0), 10),
        child_with_bed: parseInt(String(room.child_with_bed || 0), 10),
        child_no_bed: parseInt(String(room.child_no_bed || 0), 10),
        infant: parseInt(String(room.infant || 0), 10),
    }); });
    var adult = tourFormValue.rooms.reduce(function (acc, room) { return (room.adult || 0) + acc; }, 0);
    var child = tourFormValue.rooms.reduce(function (acc, room) { return (room.child_with_bed || 0) + (room.child_no_bed || 0) + acc; }, 0);
    var infant = tourFormValue.rooms.reduce(function (acc, room) { return (room.infant || 0) + acc; }, 0);
    var traveler = adult + child;
    var tmpDepartureDate = new Date(tourFormValue.departure_date);
    var context = {
        adult: adult, child: child, infant: infant, traveler: traveler,
        slots_taken: departureSelected == null ? [] : Array(traveler).fill(null).map(function (_, idx) { return departureSelected.slots_taken + idx; }),
        departure_date: [tourFormValue.departure_date],
        travel_dates: Array(parseInt(optionSelected.duration, 10)).fill(null).map(function (_, idx) {
            var isoDate = dateToIsoDate(tmpDepartureDate);
            tmpDepartureDate.setDate(tmpDepartureDate.getDate() + 1);
            return isoDate;
        }),
        coupon: '',
        days_before_departure: Math.ceil(((new Date(tourFormValue.departure_date)).getTime() - (new Date()).getTime()) / 24 / 60 / 60 / 1000),
        room: tourFormValue.rooms.length
    };
    var rulesResult = rules_compute_1.default(optionSelected.rule_parsed, context);
    var effectivePrice = Object.assign({}, optionSelected);
    var currentPrice = rulesResult.prices.pop();
    if (currentPrice != null) {
        effectivePrice.name += ' (' + currentPrice.name + ')';
        for (var j in currentPrice.price) {
            if (currentPrice.price.hasOwnProperty(j)) {
                var k = currentPrice.price[j];
                effectivePrice[k.code] = k.price;
            }
        }
    }
    var pushLines = function (adultPriceType, price, subHeader, room, acc) {
        acc.push({
            sub_header: subHeader,
            name: 'Adult',
            price: price[adultPriceType],
            deposit: big_js_1.default(price['fixed_deposit']).plus(big_js_1.default(price[adultPriceType]).times(price['percentage_deposit']).div(100)).toFixed(2).toString(),
            price_type: adultPriceType,
            quantity: room.adult
        });
        var childBed = room.child_with_bed || 0;
        if (adult === 1 && childBed > 0) {
            childBed--;
            acc.push({
                sub_header: subHeader,
                name: 'Child (half twin)',
                price: effectivePrice['CHT'],
                deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['CHT']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                price_type: 'CHT',
                quantity: 1
            });
        }
        if (childBed > 0) {
            acc.push({
                sub_header: subHeader,
                name: 'Child (with bed)',
                price: effectivePrice['CWB'],
                deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['CWB']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                price_type: 'CWB',
                quantity: childBed
            });
        }
        if ((room.child_no_bed || 0) > 0) {
            acc.push({
                sub_header: subHeader,
                name: 'Child (sharing bed)',
                price: effectivePrice['CNB'],
                deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['CNB']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                price_type: 'CNB',
                quantity: room.child_no_bed
            });
        }
        if ((room.infant || 0) > 0) {
            acc.push({
                sub_header: subHeader,
                name: 'Infant',
                price: effectivePrice['INF'],
                deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['INF']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                price_type: 'INF',
                quantity: room.infant
            });
        }
    };
    var invoiceItems = tourFormValue.rooms.reduce(function (acc, room, index) {
        var subHeader = tourFormValue.rooms.length > 1 ? 'Room ' + (index + 1) : 'Base price';
        var beds = room.adult + (room.child_with_bed || 0);
        if (tour.price_type.indexOf('ALL') === -1 && tour.price_type.indexOf('SGL') === -1 && tour.price_type.indexOf('CWB') === -1) {
            var totalTravellers = (room.adult || 0) + (room.child_no_bed || 0) + (room.child_with_bed || 0);
            if (totalTravellers > 0) {
                acc.push({
                    sub_header: subHeader,
                    name: 'Traveller',
                    price: effectivePrice['TWN'],
                    deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['TWN']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                    price_type: 'TWN',
                    quantity: totalTravellers
                });
            }
            return acc;
        }
        if (tour.price_type.indexOf('ALL') === -1 && tour.price_type.indexOf('SGL') === -1) {
            if ((room.adult || 0) > 0) {
                acc.push({
                    sub_header: subHeader,
                    name: 'Adult',
                    price: effectivePrice['TWN'],
                    deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['TWN']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                    price_type: 'TWN',
                    quantity: room.adult
                });
            }
            var totalChildren = (room.child_no_bed || 0) + (room.child_with_bed || 0);
            if (totalChildren > 0) {
                acc.push({
                    sub_header: subHeader,
                    name: 'Child',
                    price: effectivePrice['CWB'],
                    deposit: big_js_1.default(effectivePrice['fixed_deposit']).plus(big_js_1.default(effectivePrice['CWB']).times(effectivePrice['percentage_deposit']).div(100)).toFixed(2).toString(),
                    price_type: 'CWB',
                    quantity: totalChildren
                });
            }
            return acc;
        }
        if (beds === 1) {
            pushLines('SGL', effectivePrice, subHeader, room, acc);
        }
        else if (beds === 2) {
            pushLines('TWN', effectivePrice, subHeader, room, acc);
        }
        else if (beds === 3) {
            pushLines('TRP', effectivePrice, subHeader, room, acc);
        }
        else {
            pushLines('QUAD', effectivePrice, subHeader, room, acc);
        }
        return acc;
    }, []);
    var totalPrice = invoiceItems.reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0));
    var items = rulesResult.surcharges.reduce(function (acc, surcharge) {
        var price = big_js_1.default(surcharge.price['fixed']).plus(big_js_1.default(surcharge.price['percentage']).times(totalPrice).div(100)).toFixed(2);
        var line = {
            sub_header: 'Adjustments',
            name: surcharge.name,
            price: price.toString(),
            deposit: price.toString(),
            price_type: 'ADJUST',
            quantity: surcharge.qty
        };
        acc.push(line);
        totalPrice = totalPrice.add(big_js_1.default(price).times(surcharge.qty));
        return acc;
    }, invoiceItems);
    var toDate = new Date(tourFormValue.departure_date);
    toDate.setDate(toDate.getDate() + parseInt(optionSelected.duration, 10));
    var product = {
        'product_source': '',
        'product_source_ref': '',
        'product_type': 'tour_package',
        'product_id': tour.id,
        'product_code': optionSelected['product_code'],
        'product_name': tour.name + ' > ' + optionSelected.name,
        'product_category_ids': tour.categories.map(function (cat) { return cat.id; }).join(','),
        'quantity': traveler,
        'from_date': tourFormValue.departure_date,
        'to_date': dateToIsoDate(toDate),
        'product_status': 'Unconfirmed',
        adult: adult, child: child, infant: infant, items: items
    };
    return {
        product: product,
        total: totalPrice.toString()
    };
}
exports.computeTour = computeTour;
function computeGeneric(generic, params) {
    params.quantity = parseInt(String(params.quantity || 0), 10);
    var option = generic.options.find(function (optionx) { return optionx.id === params.option_id; });
    if (option == null) {
        return {
            error: 'option_id ' + params.option_id + ' not found'
        };
    }
    var product = {
        product_name: generic.name,
        product_type: 'nontour_product',
        product_status: 'Unconfirmed',
        product_source: '',
        product_id: generic.id + '/' + option.id,
        product_code: option.code,
        product_category_ids: generic.categories.map(function (cat) { return cat.id; }).join(','),
        quantity: params.quantity,
        items: [{
                sub_header: '',
                name: option.name,
                price: option.price,
                price_type: '',
                deposit: option.price,
                quantity: params.quantity
            }]
    };
    return {
        product: product,
        total: big_js_1.default(option.price).times(params.quantity).toFixed(2)
    };
}
exports.computeGeneric = computeGeneric;
function computeGlobaltix(item, form) {
    var quantity = parseInt(String(form.quantity || 0), 10);
    if (item.ticketTypes != null) {
        var ticket = item.ticketTypes.find(function (ticket) { return String(ticket.id) === String(form.id); });
        if (ticket == null) {
            return {
                error: 'ticketType_id ' + item.ticketType_id + ' not found'
            };
        }
        var product = {
            product_name: item.title,
            product_type: 'attraction',
            product_status: 'Unconfirmed',
            product_source: 'globaltix',
            product_id: String(item.id) + '/' + String(form.id),
            product_code: '',
            quantity: quantity,
            items: [{
                    sub_header: '',
                    name: ticket.name,
                    price: String(ticket.price),
                    price_type: '',
                    deposit: String(ticket.price),
                    quantity: quantity
                }]
        };
        return {
            product: product,
            total: big_js_1.default(ticket.price).times(quantity).toFixed(2)
        };
    }
    else if (item.attraction != null) {
        var product = {
            product_name: item.attraction.title,
            product_type: 'attraction',
            product_status: 'Unconfirmed',
            product_source: 'globaltix',
            product_id: String(item.attraction.id) + '/' + String(form.id),
            product_code: '',
            quantity: quantity,
            items: [{
                    sub_header: '',
                    name: item.name,
                    price: String(item.payableAmount),
                    price_type: '',
                    deposit: String(item.payableAmount),
                    quantity: quantity
                }]
        };
        return {
            product: product,
            total: big_js_1.default(item.payableAmount).times(quantity).toFixed(2)
        };
    }
    else {
        return { error: 'Item is in unknown format. Please provide attraction with ticketTypes from attraction/list or ticketType/get' };
    }
}
exports.computeGlobaltix = computeGlobaltix;
function segmentToString(segment) {
    var firstFlight = segment.flights[0];
    var lastFlight = segment.flights[segment.flights.length - 1];
    return segment.marketing_carrier.code
        + segment.flight_number + ' '
        + firstFlight.departure_airport.code + ' (' + firstFlight.departure_datetime.substr(0, 16) + ')'
        + ' ==> '
        + lastFlight.arrival_airport.code + ' (' + lastFlight.arrival_datetime.substr(0, 16) + ')';
}
function computeFlight(flight, form) {
    var selectedFare = flight.fares.find(function (x) { return x.id === form.fare_id; });
    if (selectedFare == null)
        return { error: 'fare ' + form.fare_id + 'not found' };
    var product = {};
    product.product_type = 'flight';
    product.product_source = selectedFare.source;
    product.product_status = 'Unconfirmed';
    product.adult = selectedFare.ptc_breakdown_map['adt'].quantity;
    product.child = selectedFare.ptc_breakdown_map['cnn'] != null ? selectedFare.ptc_breakdown_map['cnn'].quantity : 0;
    product.infant = selectedFare.ptc_breakdown_map['inf'] != null ? selectedFare.ptc_breakdown_map['inf'].quantity : 0;
    product.quantity = product.adult + product.child;
    var od1FirstSegment = flight.od1.segments[0];
    var od1LastSegment = flight.od1.segments[flight.od1.segments.length - 1];
    product.product_name = od1FirstSegment.flights[0].departure_airport.code + ' -> ' + od1LastSegment.flights[od1LastSegment.flights.length - 1].arrival_airport.code;
    if (flight.od2) {
        var od2FirstSegment = flight.od2.segments[0];
        var od2LastSegment = flight.od2.segments[flight.od2.segments.length - 1];
        if (od1LastSegment.flights[od1LastSegment.flights.length - 1].arrival_airport.code === od2FirstSegment.flights[0].departure_airport.code) {
            product.product_name += ' -> ' + od2LastSegment.flights[od2LastSegment.flights.length - 1].arrival_airport.code;
        }
        else {
            product.product_name += ' / ' + od2FirstSegment.flights[0].departure_airport.code + ' -> ' + od2LastSegment.flights[od2LastSegment.flights.length - 1].arrival_airport.code;
        }
        product.from_date = od1FirstSegment.flights[0].departure_datetime.substr(0, 16);
        product.to_date = od2LastSegment.flights[od2LastSegment.flights.length - 1].arrival_datetime.substr(0, 16);
    }
    else {
        product.from_date = od1FirstSegment.flights[0].departure_datetime.substr(0, 16);
        product.to_date = od1LastSegment.flights[od1LastSegment.flights.length - 1].arrival_datetime.substr(0, 16);
    }
    var segmentsFlattened = flight.od1.segments;
    if (flight.od2 != null) {
        segmentsFlattened = segmentsFlattened.concat(flight.od2.segments);
    }
    var subhead = segmentsFlattened.map(function (x) { return segmentToString(x); }).join('\n');
    product.items = [];
    var total = big_js_1.default(0);
    for (var ptc in selectedFare.ptc_breakdown_map) {
        if (selectedFare.ptc_breakdown_map.hasOwnProperty(ptc)) {
            var breakdown = selectedFare.ptc_breakdown_map[ptc];
            // const tax = formatCurrency(breakdown.tax)
            product.items.push({
                header: 'Item',
                sub_header: subhead,
                name: ptc.toUpperCase(),
                price: breakdown.price,
                cost: breakdown.cost,
                deposit: breakdown.price,
                quantity: breakdown.quantity
            });
            var subTotal = big_js_1.default(breakdown.price).times(breakdown.quantity);
            total = total.add(subTotal);
        }
    }
    if (total.eq(selectedFare.price) === false) {
        return { error: "Error in fare. Breakdown: " + total.toFixed(2) + ". Total: " + selectedFare.price };
    }
    return {
        total: total.toFixed(2),
        product: product
    };
}
exports.computeFlight = computeFlight;
function dateToIsoDate(date) {
    var pad = function (x) { return x < 10 ? '0' + x : x; };
    return '' + date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate());
}
function formatCurrency(x) {
    var xNumber = typeof x === 'string' ? parseFloat(x) : x;
    var neg = xNumber < 0 ? '-' : '';
    return neg + '$' + big_js_1.default(x).abs().toFixed(2).toString();
}
// not null not empty (not undefined)
// TravelCloud treats null and empty values identically
function nnne(x) {
    return x != null && x.length > 0;
}
// not null not zero (for checking boolean fields)
function nnnz(x) {
    return x != null && x.length > 0 && parseInt(x, 10) !== 0;
}
// "Deposit Unpaid", "Fully Paid", "Deposit Paid"
function computePriceRules(order, priceRules, bcryptCache, customer) {
    // remove previous computations if any
    order.products = order.products.map(function (product) {
        product.items = product.items.filter(function (item) { return !nnnz(item.is_from_price_rule); });
        return product;
    });
    if (priceRules == null)
        priceRules = [];
    var orderTotal = order.products.reduce(function (acc, product) {
        var itemsTotal = product.items.reduce(function (acc2, item) { return acc2.plus(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0));
        return acc.plus(itemsTotal);
    }, big_js_1.default(0)).toFixed(2);
    var priceRulesCombined = customer == null || customer.categories == null || customer.categories[0] == null || customer.categories[0].customer_in_any_price_rules == null
        ? priceRules
        : customer.categories.reduce(function (acc, category) { return acc.concat(category.customer_in_any_price_rules); }, priceRules.slice(0));
    var priceRulesSorted = priceRulesCombined.sort(function (rule) { return parseInt(rule.adjustment_sequence, 10); });
    var now = new Date();
    // compute price attributes for
    for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
        var product = _a[_i];
        var mutexDict = {};
        var ruleIdDict = {};
        var total = product.items.reduce(function (acc, item) { return acc.add(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0));
        var running = big_js_1.default(total); // Clone. Big doesn't seem to mutate it's object so this might not be required.
        var seqTotal = big_js_1.default(0);
        var lastSeq = void 0;
        var priceAttributes = Object.assign({
            'order/total': orderTotal,
            'product/total': product.items.reduce(function (acc, item) { return acc.plus(big_js_1.default(item.price).times(item.quantity)); }, big_js_1.default(0))
        }, product.product_attributes);
        var _loop_1 = function (rule) {
            if (ruleIdDict[rule.id] != null)
                return "continue";
            // console.log('processing rule ' + rule.id)
            ruleIdDict[rule.id] = true;
            if (lastSeq != rule.adjustment_sequence) {
                running = running.add(seqTotal);
                seqTotal = big_js_1.default(0);
            }
            lastSeq = rule.adjustment_sequence;
            if (nnne(rule.condition_product_source)) {
                if (rule.condition_product_source !== product.product_source) {
                    return "continue";
                }
            }
            if (nnne(rule.condition_product_type)) {
                if (rule.condition_product_type !== product.product_type) {
                    return "continue";
                }
            }
            if (nnne(rule.condition_from_datetime)) {
                var condition_from_date = new Date(rule.condition_from_datetime);
                if (isNaN(condition_from_date.getTime()) || condition_from_date > now) {
                    return "continue";
                }
            }
            if (nnne(rule.condition_to_datetime)) {
                var condition_to_date = new Date(rule.condition_to_datetime);
                if (isNaN(condition_to_date.getTime()) || condition_to_date < now) {
                    return "continue";
                }
            }
            if (rule.condition_customer_in_any_categories.length > 0) {
                if (customer == null)
                    return "continue";
                if (rule.condition_customer_in_any_categories.find(function (category1) {
                    return customer.categories.find(function (category2) {
                        return category1.id === category2.id;
                    })
                        != null;
                })
                    == null)
                    return "continue";
            }
            if (rule.condition_product_in_any_categories.length > 0) {
                if (product.product_category_ids == null) {
                    return "continue";
                }
                var product_category_ids_1 = product.product_category_ids.split(',');
                if (rule.condition_product_in_any_categories.find(function (category1) {
                    return product_category_ids_1.find(function (category2) {
                        return category1.id === category2;
                    })
                        != null;
                })
                    == null) {
                    return "continue";
                }
            }
            if (nnne(rule.condition_checkout_code_hash)) {
                if (!nnne(order.checkout_code)) {
                    return "continue";
                }
                var cacheKey = rule.condition_checkout_code_hash + '/' + order.checkout_code;
                if (bcryptCache[cacheKey] == null)
                    bcryptCache[cacheKey] = bcryptjs_1.default.compareSync(order.checkout_code, rule.condition_checkout_code_hash);
                if (bcryptCache[cacheKey] === false) {
                    return "continue";
                }
            }
            // this sets mutexDict and needs to be the last check
            if (nnne(rule.condition_mutex_code)) {
                if (mutexDict[rule.condition_mutex_code] != null) {
                    return "continue";
                }
                mutexDict[rule.condition_mutex_code] = true;
            }
            // console.log('rule ' + rule.id + ' passed')
            var priceAttribute = big_js_1.default(0);
            if (rule.adjustment_percentage_price_attribute === 'product_running_total') {
                priceAttribute = running;
            }
            else if (priceAttributes[rule.adjustment_percentage_price_attribute] != null) {
                priceAttribute = big_js_1.default(priceAttributes[rule.adjustment_percentage_price_attribute]);
            }
            var adjustmentPercentage = nnne(rule.adjustment_percentage)
                ? big_js_1.default(rule.adjustment_percentage).times(priceAttribute).div(100)
                : big_js_1.default(0);
            if (nnnz(rule.adjustment_percentage_max_enabled) && adjustmentPercentage.gt(rule.adjustment_percentage_max)) {
                adjustmentPercentage = big_js_1.default(rule.adjustment_percentage_max);
            }
            if (nnnz(rule.adjustment_percentage_min_enabled) && adjustmentPercentage.lt(rule.adjustment_percentage_min)) {
                adjustmentPercentage = big_js_1.default(rule.adjustment_percentage_min);
            }
            var adjustmentTotal = adjustmentPercentage.plus(rule.adjustment_fixed);
            seqTotal = seqTotal.plus(adjustmentTotal);
            product.items.push({
                'id': undefined,
                'header': '',
                'sub_header': 'Adjustments',
                'name': rule.adjustment_name,
                'price': adjustmentTotal.toFixed(2),
                'price_type': rule.adjustment_price_type,
                'deposit': adjustmentTotal.toFixed(2),
                'quantity': '1',
                'is_from_price_rule': '1',
            });
        };
        for (var _b = 0, priceRulesSorted_1 = priceRulesSorted; _b < priceRulesSorted_1.length; _b++) {
            var rule = priceRulesSorted_1[_b];
            _loop_1(rule);
        }
    }
    return order;
}
exports.computePriceRules = computePriceRules;
function urldecode(str) {
    return JSON.parse("{\"" + str.replace(/&/g, '","').replace(/\=/g, '":"') + "\"}", function (key, value) {
        if (key === "") {
            return value;
        }
        else {
            return decodeURIComponent(value);
        }
    });
}
function computeLegacyHotel(details, checkout) {
    var source = 'bedsonline';
    var room = details.rooms.find(function (room) { return room.id === checkout.room_id; });
    if (room == null) {
        return {
            error: 'Cannot find room id ' + checkout.room_id
        };
    }
    var param = checkout.params;
    var duration = param.duration;
    var checkInDate = param.checkInDate;
    var checkOutDate = moment_1.default(param.checkInDate, 'YYYY-MM-DD').add(param.duration, 'days').format('YYYY-MM-DD');
    var product = {};
    product.items = [];
    product.product_name = details.name;
    product.product_type = 'hotel_room';
    product.product_status = 'Unconfirmed';
    product.product_source = source;
    product.product_id = room.id;
    product.to_date = checkOutDate;
    product.from_date = checkInDate;
    product.adult = 1;
    product.quantity = '1';
    var voucherRoom = JSON.parse(JSON.stringify(room));
    voucherRoom.prices = [];
    var price = room.prices.find(function (price) { return price.code === checkout.price_code; });
    if (price == null) {
        return {
            error: 'Cannot find price code ' + checkout.price_code
        };
    }
    product.items.push({
        header: 'Item',
        sub_header: room.description,
        name: "Check-in on " + checkInDate + ", Check-out on " + checkOutDate + ", " + duration + " nights",
        price: price.price,
        price_type: price.code,
        deposit: price.price,
        cost: price.cost,
        cost_source: source,
        quantity: '1'
    });
    if (price.cost != null) {
        delete price.cost;
    }
    price.quantity = '1';
    voucherRoom.prices.push(price);
    //capture info for voucher
    product.voucher = {
        contact: details.contact,
        latitude: details.latitude,
        longitude: details.longitude,
        remarks: details.remarks,
        room: voucherRoom
    };
    return { product: product };
}
exports.computeLegacyHotel = computeLegacyHotel;
;
//# sourceMappingURL=order-product-compute.js.map