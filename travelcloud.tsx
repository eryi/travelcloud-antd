import 'isomorphic-unfetch'
import Big from 'big.js'
import moment from 'moment'
import { computeTour, computeLegacyHotel, computeGeneric, computeFlight, Product, computePriceRules, TourFormValue, LegacyHotelFormValue, GlobaltixFormValue, computeGlobaltix } from './order-product-compute'
import React from "react"
import { FlightSearch, FlightDetail, PriceRules, Tour } from './types'

export interface TcResponse<T> {
  loading?: boolean
  error?: string
  result?: T
}

export interface LegacyHotelsParams {
  cityCode: string
  checkInDate: string
  checkOutDate: string
  adults: number | string
  children: number | string
}

export interface FlightsParams {
  source: string
  'od1.origin_airport.code': string
  'od1.destination_airport.code'?: string
  'od1.origin_datetime': string
  'od2.origin_airport.code': string
  'od2.destination_airport.code'?: string
  'od2.origin_datetime'?: string
  cabin?: string
  ptc_adt: number
  ptc_cnn: number
  ptc_inf: number
}

export interface FlightParams {
  source: string
  'od1.id': string
  'od2.id': string
  ptc_adt: number
  ptc_cnn: number
  ptc_inf: number
}

export interface LegacyHotelParams {
  hotelId: string
  checkInDate: string
  checkOutDate: string
  adults: number | string
  children: number | string
}

export interface TourParams {
  'categories.name': string,
  '~boolean': string
}

function postRedirect(post: { action: string, form: { name: string, value: string }[] }) {
  const f = document.createElement('form');
  f.action = post.action;
  f.method = 'POST';

  for (let input of post.form) {
    const i = document.createElement('input');
    i.type = 'hidden';
    i.name = input.name;
    i.value = input.value;
    f.appendChild(i);
  }
  document.body.appendChild(f);
  f.submit();
}

export const setupUserback = (token = '4386|6572|8OWEwPsjAdNcLi51kGOXy3CKGebHwlJ2sEcrPYy9SEPvizoqNF') => {
  if (window.location.hostname.indexOf('.travel.click') === -1) return
  const window2: any = window
  const id = 'userback-sdk'
  const Userback = window2.Userback || {}
  Userback.access_token = token;
  window2.Userback = Userback
  if (document.getElementById(id)) { return }
  var s = document.createElement('script')
  s.id = id
  s.async = true
  s.src = 'https://static.userback.io/widget/v1.js'
  var parent_node = document.head || document.body
  parent_node.appendChild(s)
}

export const setupGtag = (Router, config: { tcUser: string, googleAnalyticsTrackingId?: string, [key: string]: any }) => {
  const window2: any = window
  const travelClickTrackingId = 'UA-132985038-1'
  if (window == null || window2.dataLayer != null) return

  window2.dataLayer = []
  window2.gtag = function () { window2.dataLayer.push(arguments) }

  // prevent logging localhost byt leave the gtag stub there
  if (window.location.hostname === 'localhost') return

  window2.gtag('js', new Date())
  window2.gtag('config', travelClickTrackingId, {
    'dimension1': config.tcUser
  })

  if (config.googleAnalyticsTrackingId != null && config.googleAnalyticsTrackingId.trim() !== "") {
    window2.gtag('config', config.googleAnalyticsTrackingId.trim())
  }

  let tag = document.createElement('script')
  tag.src = "https://www.googletagmanager.com/gtag/js?id=" + (config.googleAnalyticsTrackingId || 'UA-132985038-1')
  let firstScriptTag = document.getElementsByTagName('script')[0]
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)

  let lastTrackedUrl = null;

  Router.events.on('routeChangeComplete', (newUrl) => {
    if (newUrl === lastTrackedUrl || window2.gtag == null) {
      return;
    }

    // Don't double track the same URL
    lastTrackedUrl = newUrl;

    window2.gtag('config', travelClickTrackingId, {
      page_path: newUrl,
      'dimension1': config.tcUser
    })

    if (config.googleAnalyticsTrackingId != null && config.googleAnalyticsTrackingId.trim() !== "") {
      window2.gtag('config', config.googleAnalyticsTrackingId.trim(), {
        page_path: newUrl
      })
    }
  })
}

export async function redirectToPayment(options: {
  orderRef: string,
  domain: string,
  tcUser: string,
  paymentProcessor: string,
  amount: string,
  successPage: string,
  failurePage: string,
}) {

  const client = new TravelCloudClient(options.tcUser)
  const success = options.successPage.replace(/^\/+/g, '')
  const failure = options.failurePage.replace(/^\/+/g, '')
  const host = location.host
  const prefix = location.protocol + '//' + location.host

  if (options.paymentProcessor === 'paypal') {
    const payload = {
      order_ref: options.orderRef,
      cancel: prefix + location.pathname + location.search,
      success: prefix + '/' + success,
      failure: prefix + '/' + success,
      payment_processor: options.paymentProcessor,
      amount: options.amount
    }
    const post = await client.post('rpc/payment_redirect', null, payload)

    postRedirect(post.result)

    return
  }

  const params = {
    ref: options.orderRef,
    tcUser: options.tcUser,
    amount: options.amount,
    origin: host,
    return: location.pathname + location.search,
    success, failure
  }
  window.location.href
    = "https://" + options.domain + "/payment/" + options.paymentProcessor + '?'
    + urlParam(params)
}

function urlParam(params): string {
  if (params == null) return ''
  return Object.keys(params)
    .filter(k => params[k] !== '')
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&')
}

function hasPrefix(key: string, prefixes: string[], target: string): boolean {
  const result = prefixes.find(prefix => key.indexOf(prefix) === 0)
  if (result == null) console.warn("Invalid param " + key + " for " + target)
  return result != null
}

export class TravelCloudClient {
  tcUser: string
  cache: {
    [key: string]: {
      response: any,
      time: number,
      resolves: any[]
    }
  } = {}

  get(endpoint: string, params, noCache = false): Promise<TcResponse<any>> {
    return new Promise(async (resolve, reject) => {
      const query = params == null ? '' : '?' + urlParam(params)
      const currentTime = Date.now()

      var cache
      if (typeof window === 'undefined') {
        cache = this.cache[endpoint + '/' + query]
        if (cache == null) {
          cache = {
            response: null,
            time: 0,
            resolves: []
          }
          this.cache[endpoint + '/' + query] = cache
        }
      } else {
        // store cache on window so that cache survives even when react component crashes
        if ((window as any).travelCloudClientCache == null) (window as any).travelCloudClientCache = {}
        cache = (window as any).travelCloudClientCache[endpoint + '/' + query]
        if (cache == null) {
          cache = {
            response: null,
            time: 0,
            resolves: []
          };
          (window as any).travelCloudClientCache[endpoint + '/' + query] = cache
        }
      }

      // cached response available
      if (noCache === false && cache.time + 30000 > currentTime) {
        resolve(cache.response)
        return
      }

      cache.resolves.push(resolve)

      // another request taking place
      if (cache.time === -1) {
        return
      }

      cache.time = -1

      const res = await fetch(`https://${this.tcUser}.travelcloud.app/${endpoint}${query}`)
      //const res = await fetch(`http://localhost:9000/${endpoint}${query}`)
      const resJson = await res.json()

      cache.response = resJson
      cache.time = currentTime

      for (var i in cache.resolves) {
        cache.resolves[i](resJson)
      }

      cache.resolves = []
    })
  }

  cancelPendingGetRequests(endpoint: string) {
    if (typeof window === 'undefined') {
      for (const cacheKey in this.cache) {
        if (cacheKey.indexOf(endpoint) !== 0) continue
        const entry = this.cache[cacheKey]
        for (const index in entry.resolves) {
          entry.resolves[index]({})
        }
        entry.resolves.length = 0
      }
    } else {
      if ((window as any).travelCloudClientCache == null) return
      for (const cacheKey in (window as any).travelCloudClientCache) {
        if (cacheKey.indexOf(endpoint) !== 0) continue
        const entry = (window as any).travelCloudClientCache[cacheKey]
        for (const index in entry.resolves) {
          entry.resolves[index]({})
        }
        entry.resolves.length = 0
      }
    }
  }

  async post(endpoint: string, params, data) {
    const query = params == null ? '' : '?' + urlParam(params)

    return fetch(`https://${this.tcUser}.travelcloud.app/${endpoint}${query}`, {
      //return fetch(`http://localhost:9000/${endpoint}${query}`, {
      body: JSON.stringify(data),
      cache: 'no-cache',
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      mode: 'cors',
    })
      .then(response => response.json()) // parses response to JSON
  }

  //todo: add customer token
  constructor(tcUser) {
    this.tcUser = encodeURIComponent(tcUser)
  }

  async autoComplete(params: { type: string, search?: string, values?: string | string[] }) {
    if (Array.isArray(params.values)) params.values = params.values.join(',')
    return await this.get('rpc/autocomplete', params)
  }

  async priceRules(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => ['is_public'].indexOf(key) !== -1)
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/price_rules', params)
    return json
  }

  async tours(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => hasPrefix(key, ['~boolean', 'categories.name'], 'tours'))
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/tours', params)
    json.result = json.result.map(tour => {
      tour['_next_departure'] = nextDeparture(tour.options)
      tour['_cheapest'] = cheapest(tour.options)
      tour.options = tour.options.map(option => {
        if (option['_subset_departures'] == null) option['_subset_departures'] = option['departures']
        if (option['departures'] == null) option['departures'] = option['_subset_departures']
        return option
      })
      return tour
    })
    return json
  }

  async generics(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => hasPrefix(key, ['categories.name'], 'generics'))
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/generics', params)
    return json
  }

  async generic(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => ['id'].indexOf(key) !== -1)
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/generic', params)
    return json
  }

  async documents(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => hasPrefix(key, ['categories.name'], 'documents'))
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/documents', params)
    return json
  }

  async document(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => ['id'].indexOf(key) !== -1)
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/document', params)
    return json
  }

  async tour(paramsRaw) {
    const params = Object.keys(paramsRaw)
      .filter(key => ['id'].indexOf(key) !== -1)
      .reduce((res, key) => (res[key] = paramsRaw[key], res), {});
    const json = await this.get('api/tour', params)
    json.result['_next_departure'] = nextDeparture(json.result.options)
    json.result['_cheapest'] = cheapest(json.result.options)
    return json
  }

  async globaltix(endpoint: string, params = {}) {
    const json = await this.get('globaltix/' + endpoint, params)
    return json
  }

  cancelFlights() {
    this.cancelPendingGetRequests('rpc/flight_search_new')
  }

  async flights(params: FlightsParams, sample: boolean = false): Promise<TcResponse<FlightSearch>> {
    const converted = validateFlightsParams(params)
    if (converted == null) return {}

    const path = sample ? 'sample/flight_search.json' : 'rpc/flight_search_new'

    const json = await this.get(path, converted)
    //const json = await this.get('flight.json', converted)
    return json
  }

  async flight(params: FlightParams, sample: boolean = false): Promise<TcResponse<FlightDetail>> {
    const converted = validateFlightParams(params)
    if (converted == null) return {}

    const path = sample ? 'sample/flight_detail.json' : 'rpc/flight_detail'

    const json = await this.get(path, converted)

    return json
  }

  async legacyHotels(
    params: LegacyHotelsParams) {

    const converted = validateLegacyHotelsParams(params)
    if (converted == null) return {}

    const json = await this.get('rpc/legacy_hotel_search', converted)

    return json
  }

  async legacyHotel(
    params: LegacyHotelParams) {

    const converted = validateLegacyHotelParams(params)
    if (converted == null) return {}

    const json = await this.get('rpc/legacy_hotel_detail', converted)
    return json
  }

  async order(params) {
    return this.get('api/order', params, true)
  }

  async categories(params) {
    return this.get('api/categories', params)
  }

  async submitContactForm(structured: {
    referral_site: string,
    subject: string,
    customer_name: string,
    customer_email: string
  }, unstructured: { [key: string]: string }) {
    const { referral_site, subject, customer_name, customer_email } = structured
    const payload = {
      referral_site, subject,
      '~customer_add_unstructured_message': {
        unstructured, customer_name, customer_email
      }
    }
    return await this.post('api/conversation', {}, payload)
  }
}

export const validateFlightsParams = (state) => {
  if (state == null) return null
  if (state['od1.origin_airport.code'] == null) return null
  if (state['od2.origin_airport.code'] == null) return null
  if (state['od1.origin_datetime'] == null) return null
  if (state.ptc_adt == null) return null

  const filtered = {
    source: state.source,
    'od1.origin_airport.code': state['od1.origin_airport.code'],
    'od2.origin_airport.code': state['od2.origin_airport.code'],
    'od1.destination_airport.code': state['od1.destination_airport.code'],
    'od2.destination_airport.code': state['od2.destination_airport.code'],
    'od1.origin_datetime': state['od1.origin_datetime'],
    'od2.origin_datetime': state['od2.origin_datetime'],
    ptc_adt: state.ptc_adt,
    ptc_cnn: state.ptc_cnn,
    ptc_inf: state.ptc_inf,
    cabin: state.cabin || 'Y'
  }

  if (filtered['od2.origin_datetime'] == null) delete filtered['od2.origin_datetime']
  if (filtered['od1.destination_airport.code'] == null) delete filtered['od1.destination_airport.code']
  if (filtered['od2.destination_airport.code'] == null) delete filtered['od2.destination_airport.code']
  return filtered
}

export const validateFlightParams = (state) => {
  if (state == null) return null
  if (state['od1.id'] == null) return null
  if (state.ptc_adt == null) return null

  const filtered = {
    source: state.source,
    'od1.id': state['od1.id'],
    'od2.id': state['od2.id'],
    ptc_adt: state.ptc_adt,
    ptc_cnn: state.ptc_cnn,
    ptc_inf: state.ptc_inf,
  }

  if (filtered['od2.id'] == null) delete filtered['od2.id']
  return filtered
}

export const validateLegacyHotelParams = (state) => {
  if (state == null) return null
  if (state.hotelId == null) return null
  if (state.checkInDate == null) return null
  if (state.checkOutDate == null) return null
  if (state.adults == null) return null

  return {
    hotelId: state.hotelId,
    checkInDate: moment(state.checkInDate).format('YYYY-MM-DD'),
    duration: moment(state.checkOutDate).diff(state.checkInDate, 'days'),
    adults: state.adults,
    children: state.children || 0
  }
}

export const validateLegacyHotelsParams = (state) => {
  if (state == null) return null
  if (state.cityCode == null) return null
  if (state.checkInDate == null) return null
  if (state.checkOutDate == null) return null
  if (state.adults == null) return null

  return {
    cityCode: state.cityCode,
    checkInDate: moment(state.checkInDate).format('YYYY-MM-DD'),
    duration: moment(state.checkOutDate).diff(state.checkInDate, 'days'),
    adults: state.adults,
    children: state.children || 0
  }
}

// mutates each option to include _next_departure
export function nextDeparture(options) {
  if (options.length === 0) return null;
  for (var index in options) {
    const option = options[index]
    const departuresOrSubset = option.departures || option._subset_departures
    if (option['on_demand_advance_booking'] === '0') {
      option['_next_departure'] = departuresOrSubset.reduce(
        (acc2, departure) =>
          parseInt(departure.slots_total) > parseInt(departure.slots_taken)
            ? (acc2 == null || acc2 > departure.date ? departure.date : acc2)
            : acc2, null)

      option['_last_departure'] = departuresOrSubset.reduce(
        (acc2, departure) =>
          parseInt(departure.slots_total) > parseInt(departure.slots_taken)
            ? (acc2 == null || acc2 < departure.date ? departure.date : acc2)
            : acc2, null)
    } else {
      const todayDate = (new Date()).getDate()
      const minDate = new Date()
      const maxDate = new Date()
      minDate.setDate(todayDate + parseInt(option['min_lead_time']))
      maxDate.setDate(todayDate + parseInt(option['on_demand_advance_booking']))
      option['_next_departure'] = dateToIsoDate(minDate)
      option['_last_departure'] = dateToIsoDate(maxDate)
    }
  }

  return options.reduce((acc, option) =>
    acc === false || acc > option['_next_departure'] ? option['_next_departure'] : acc, false)
}

function cheapest(options) {
  return options.reduce((acc, price) => acc == null || acc > price.TWN ? price.TWN : acc, null);
}

export function dateToIsoDate(date: Date): string {
  const pad = (x: number) => x < 10 ? '0' + x : x
  return "" + date.getFullYear() + '-' + pad(date.getMonth() + 1) + '-' + pad(date.getDate())
}

export function extractValueFromFormState(form): any {
  const acc = {}
  for (var key in form) {
    if (form.hasOwnProperty(key)) {
      const val = form[key].value
      // typeof Array is also object
      if (Array.isArray(val)) {
        // assume array of objects
        // not sure how to distinguish between array of objects and array of primitives
        acc[key] = val.map(item => extractValueFromFormState(item))
      }
      else if (typeof val === 'object') {
        acc[key] = extractValueFromFormState(val)
      }
      else {
        acc[key] = val
      }
    }
  }
  return acc
}

// mutate existing to avoid re-rendering form in pure components
// only works for values and array of objects
function mergeFormState(existing, updates) {
  if (updates == null) return existing

  for (let key in updates) {
    const val = updates[key]
    if (Array.isArray(val)) {
      for (let index in val) {
        existing[key][index] = mergeFormState(existing[key][index], val[index])
      }
    } else {
      existing[key] = val
    }
  }

  return existing
}

export const updateFormState = (rc: React.Component, key: string) =>
  (changes) => {
    rc.setState({ [key]: mergeFormState(rc.state[key], changes) })
  }

export function getStateFromLocalStorage() {
  var order = {}
  var customer: any = {}
  var token = null
  var localStorageAvailable = false

  try {
    localStorage.setItem(Cart.localStorageTestKey, Cart.localStorageTestKey)
    localStorage.removeItem(Cart.localStorageTestKey)
    order = JSON.parse(localStorage.getItem(Cart.localStorageOrderKey))
    customer = JSON.parse(localStorage.getItem(Cart.localStorageCustomerKey))
    token = localStorage.getItem(Cart.localStorageTokenKey)
    localStorageAvailable = true

  } catch (e) { }

  const result = {
    order: { result: order }, customer: { result: customer }, token,
    localStorageAvailable,
  }

  return result
}

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Probably should reimplement as context provider when it is fixed
// https://github.com/zeit/next.js/issues/4194
export class Cart {
  static localStorageTestKey = '__LOCAL_STORAGE_TEST__'
  static localStorageOrderKey = 'travelcloud-order'
  static localStorageCustomerKey = 'travelcloud-customer'
  static localStorageTokenKey = 'travelcloud-token'
  public demo: boolean = false

  private customer
  private order
  private token
  private priceRules: TcResponse<PriceRules> = {}
  private bcryptCache = {}
  private priceRulesLoaded = false

  private client: TravelCloudClient
  private onOrderChange: (order: any) => any
  private onCustomerChange: (customer: any) => any
  private onPriceRulesChange: (priceRules: TcResponse<PriceRules>) => any

  // for order/customer
  // null - server-side, not initialized
  // {} - nothing to show/empty
  constructor(
    options:
      {
        client: TravelCloudClient,
        onOrderChange: (order: any) => any,
        onCustomerChange: (customer: any) => any,
        onPriceRulesChange?: (priceRules: TcResponse<PriceRules>) => any
      }) {
    this.client = options.client
    this.onOrderChange = options.onOrderChange
    this.onCustomerChange = options.onCustomerChange
    this.onPriceRulesChange = options.onPriceRulesChange
    const restored = getStateFromLocalStorage()
    this.token = restored.token
    this.updateCustomer(restored['customer'])
    this.updateOrder(restored['order'])
  }

  // for testing
  saveOrderSnapshot() {
    const str = JSON.stringify(this.order)
    localStorage.setItem('orderSnapshotStr', str)
  }

  restoreOrderSnapshot() {
    const str = localStorage.getItem('orderSnapshotStr')
    this.updateOrder(JSON.parse(str))
  }

  async getPassword(email: string) {
    email = email.trim()
    const json = await this.client.post('api/customer', { email }, { '~send_login_password': { method: 'email' } })
    return json
  }

  async login(email: string, login_password: string) {
    email = email.trim()
    login_password = login_password.trim()
    const json = await this.client.post('api/customer', { email }, { '~get_login_token': { login_password } })

    if (json.result != null) {
      //json.result['_discounts'] = json.result.categories.reduce((acc, cat) => Object.assign(acc, parseCategoryDescription(cat.description)), {})
      this.updateCustomer(json)
      this.token = json.result['_get_login_token']
      try {
        localStorage.setItem(Cart.localStorageTokenKey, json.result['_get_login_token'])
        localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(json.result))
      } catch (e) { }
    } else {
      this.updateCustomer({})
      try {
        localStorage.removeItem(Cart.localStorageTokenKey)
        localStorage.removeItem(Cart.localStorageCustomerKey)
      } catch (e) { }
    }
    return json
  }

  async payOrderWithCustomerCredit(ref: string) {
    const customer = this.customer
    const login_token = this.token
    if (login_token == null || customer == null || customer.result == null) return

    const json = await this.client.post
      ('api/customer'
        , { email: customer.result.email }
        , {
          '~authenticate_with_login_token': { login_token }
          , '~pay_order': { ref }
        })

    if (json.result != null) {
      this.updateCustomer(json)
      localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(json.result))
    }

    return json
  }

  logout() {
    this.updateCustomer({})
    this.updateOrder({})
    this.token = null
    localStorage.removeItem(Cart.localStorageTokenKey)
  }

  async refreshCustomer() {
    const customer = this.customer
    const login_token = this.token
    if (login_token == null || customer == null || customer.result == null) return

    const json = await this.client.post('api/customer', { email: customer.result.email }, { '~authenticate_with_login_token': { login_token } })

    if (json.result != null) {
      this.updateCustomer(json)
      try {
        localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(json.result))
      } catch (e) { }
    }
  }

  reset() {
    this.updateOrder({})
    return this
  }

  addTour(tour, tourFormValue: TourFormValue, mock = false): Product | null {
    const computed: any = computeTour(tour, tourFormValue)
    if (computed.product == null) return null
    computed.product.form = tourFormValue
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  addGlobaltix(item, globaltixFormValue: GlobaltixFormValue, mock = false): Product | null {
    const computed: any = computeGlobaltix(item, globaltixFormValue)
    if (computed.product == null) return null
    computed.product.form = globaltixFormValue
    return this.addProduct(computed.product, mock)
  }

  addLegacyHotel(details, legacyHotelFormValue: LegacyHotelFormValue, mock = false) {
    const computed = computeLegacyHotel(details, legacyHotelFormValue)
    if (computed.product == null) return null
    computed.product.form = legacyHotelFormValue
    console.log(legacyHotelFormValue)
    return this.addProduct(computed.product, mock)
  }

  addFlight(flight: FlightDetail, fareId: string, mock = false) {
    const selectedFare = flight.fares.find((x) => x.id === fareId)
    if (selectedFare == null) return null

    const ptc_adt = selectedFare.ptc_breakdown_map['adt'].quantity
    const ptc_cnn = selectedFare.ptc_breakdown_map['cnn'] != null ? selectedFare.ptc_breakdown_map['cnn'].quantity : 0
    const ptc_inf = selectedFare.ptc_breakdown_map['inf'] != null ? selectedFare.ptc_breakdown_map['inf'].quantity : 0

    const form
      = flight.od2 == null
        ? {
          od1_id: flight.od1.id,
          fare_id: fareId,
          ptc_adt, ptc_cnn, ptc_inf
        }
        : {
          od1_id: flight.od1.id,
          od2_id: flight.od2.id,
          fare_id: fareId,
          ptc_adt, ptc_cnn, ptc_inf
        }

    const computed: any = computeFlight(flight, form)

    computed.product.form = form
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  addGeneric(generic, params: { generic_id: string, option_id: string, quantity: number }, mock = false) {
    const computed = computeGeneric(generic, params)
    computed.product.form = params
    if (computed.total == null) return null
    return this.addProduct(computed.product, mock)
  }

  async checkout(checkoutFormValue) {
    const order = Object.assign({}, this.order.result || {}, checkoutFormValue)

    // server format is not the same
    const oc = JSON.parse(JSON.stringify(order))
    oc['~set_email'] = oc['email']

    delete oc['email']
    delete oc['payment_required']
    delete oc['deposit_required']

    const customer = this.customer
    const login_token = this.token

    if (login_token != null && customer != null && customer.result != null) {
      oc['~customer_authenticate_with_login_token'] = {
        email: customer.result.email,
        login_token: login_token
      }
    }

    const addRemoveProducts = oc['products'].map(product => {
      if (product.product_type === 'tour_package') {
        return {
          tour: product.form
        }
      } else if (product.product_type === 'flight') {
        return {
          flight: product.form
        }
      } else if (product.product_type === 'nontour_product') {
        return {
          generic: product.form
        }
      } else if (product.product_type === 'hotel_room') {
        return {
          legacy_hotel: product.form
        }
      } else if (product.product_type === 'hotel_room') {
        return {
          legacy_hotel: product.form
        }
      } else if (product.product_source === 'globaltix') {
        return {
          globaltix: product.form
        }
      }
      throw new Error("Unknown product type " + product.product_type)
    })

    delete oc['products']
    oc['~add_remove_products'] = addRemoveProducts

    const json = await this.client.post(`api/order`, null, oc)

    if (json.result != null) {
      this.updateOrder({})
      try {
        localStorage.removeItem(Cart.localStorageOrderKey)
      } catch (e) {
        console.warn("Cannot save order details to localStorage")
      }
    }

    return json
  }

  empty() {
    this.updateOrder({})
    localStorage.removeItem(Cart.localStorageOrderKey)
    return true
  }

  addProduct(product: Product, mock: boolean = false): Product | null {
    const order = JSON.parse(JSON.stringify(this.order.result || {}))

    order['products'] = order['products'] || []

    if (mock) {
      order['products'].push(product)

      if (this.priceRulesLoaded === false) {
        this.priceRulesLoaded = true
        this.refreshPriceRules()
      }

      computePriceRules(order, this.priceRules.result, this.bcryptCache, this.customer.result)
    } else {
      // if we find that an existing product already exists, we overwrite it
      // merging products is too complex due to pricing and availability
      const existingProductIndex = order['products'].findIndex(existingProduct =>
        product.product_id === existingProduct.product_id
        && product.product_type === existingProduct.product_type
        && product.product_code === existingProduct.product_code
        && product.product_source === existingProduct.product_source
        && product.product_name === existingProduct.product_name)

      if (existingProductIndex == -1) {
        if (product.quantity > 0) order['products'].push(product)
        else return null
      } else {
        if (product.quantity <= 0) {
          this.removeProductByIndex(existingProductIndex)
          return null
        }
        else order['products'][existingProductIndex] = product
      }

      this.updateOrder({ result: order })
    }

    // product will be mutated by computePriceRules
    return product
  }

  removeProductByIndex(index) {
    const order = Object.assign({}, this.order.result || {})
    order['products'] = order['products'] || []
    if (order['products'][index] != null) order.products.splice(index, 1)
    this.updateOrder({ result: order })
  }

  // utility for invoice where we group products by name
  removeProductByName(name) {
    const order = Object.assign({}, this.order.result || {})
    const products = order['products'] || []
    order['products'] = products.filter(product => product.product_name !== name)
    this.updateOrder({ result: order })
  }

  private refreshPriceRules() {
    this.priceRules = { loading: true }
    if (this.onPriceRulesChange != null) this.onPriceRulesChange(this.priceRules)
    this.client.priceRules({ is_public: '1' })
      .then(priceRulesResults => {
        this.priceRules = priceRulesResults
        this.updateOrder(this.order)
        if (this.onPriceRulesChange != null) this.onPriceRulesChange(this.priceRules)
      })
      .catch(err => {
        console.warn("Failed to fetch price rules", err)
      })
  }

  private updateOrder(order) {
    if (this.priceRulesLoaded === false) {
      this.priceRulesLoaded = true
      this.refreshPriceRules()
    }

    if (order.result != null && order.result.products != null) {
      computePriceRules(order.result, this.priceRules.result, this.bcryptCache, this.customer.result)

      order.result['payment_required'] = order.result['products'].reduce((acc, product) =>
        product.items.reduce((acc2, item) => acc2.add(Big(item.price).times(item.quantity)), acc), Big(0))

      order.result['deposit_required'] = order.result['products'].reduce((acc, product) =>
        product.items.reduce((acc2, item) => acc2.add(Big(item.deposit).times(item.quantity)), acc), Big(0))
    }

    this.order = order
    this.onOrderChange(order)

    try {
      if (order.result)
        localStorage.setItem(Cart.localStorageOrderKey, JSON.stringify(order.result))
      else
        localStorage.removeItem(Cart.localStorageOrderKey)
    } catch (e) {
      console.warn("Cannot save order details to localStorage")
    }
  }

  private async updateCustomer(customer) {
    this.customer = customer
    this.onCustomerChange(customer)

    try {
      if (customer.result) {
        if (this.order != null && this.order.result != null && this.order.result.products != null) {
          this.updateOrder(this.order)
        }

        localStorage.setItem(Cart.localStorageCustomerKey, JSON.stringify(customer.result))
      } else {
        localStorage.removeItem(Cart.localStorageCustomerKey)
      }
    } catch (e) {
      console.warn("Cannot save order details to localStorage")
      console.error(e)
    }
  }
}

/************
* Utilities *
*************/

export function formatCurrency(x) {
  if (x == null) return ''
  const neg = parseFloat(x) < 0 ? '-' : ''
  return neg + '$' + Big(x).abs().toFixed(2).toString()
}

// not sure how to enable Object.entries in babel
export function objectEntries(obj) {
  const groups = []
  for (var i in obj) {
    groups.push([i, obj[i]])
  }
  return groups
}

// will only work for array of forms, need some modifications to work for array of inputs
export function formArrayWrapper({ RenderForm, name, form }: { RenderForm, name: string, form }) {
  form.getFieldDecorator(name)
  var value = form.getFieldValue(name)
  if (value == null) value = []
  if (!Array.isArray(value)) {
    console.warn(`Expecting form state of ${name} to be array`, value)
    value = []
  }
  const onChange = (index) => (change) => {
    value[index] = Object.assign(value[index], change)
    form.setFieldsValue({ [name]: value })
  }

  const deleteSelf = (index) => () => {
    value.splice(index, 1)
    form.setFieldsValue({ [name]: value })
  }

  return value.map((item, index) => <RenderForm key={index} index={index} numberOfSiblings={value.length} value={item} deleteSelf={deleteSelf(index)} onChange={onChange(index)} />)
}

export function mapTourOptionDepartures<T>(option: Tour["options"][0], func: (departure: Tour["options"][0]["departures"][0], departureDate: string) => T): T[] {
  if (option['on_demand_advance_booking'] === '0') {
    return option.departures.map((departure) => func(departure, departure.date))
  } else {
    const todayDate = (new Date()).getDate()
    const runningDate = new Date()
    runningDate.setDate(todayDate + parseInt(option['min_lead_time']))

    const endDate = new Date()
    endDate.setDate(todayDate + parseInt(option['on_demand_advance_booking']))

    const result = []
    while (runningDate < endDate) {
      result.push(func(null, dateToIsoDate(runningDate)))
      runningDate.setDate(runningDate.getDate() + 1)
    }

    return result
  }
}
